using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models;
using HRIS.Models.General;
using HRIS.Models.PM3;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
namespace HRIS.Controllers
{

    public class RCicoController : Controller
    {
        private readonly ILogger<RCicoController> _logger;
        private readonly PM3Context _context;

        public RCicoController(ILogger<RCicoController> logger, PM3Context context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SubmitPersonal([FromBody] cls_ i)
        {
            var status = false;
            try
            {
                var npk = "37752";
                var param1 = new SqlParameter("@NPK", i.txtNpk ?? "");
                var param2 = new SqlParameter("@startdate", i.date_start ?? "");
                var param3 = new SqlParameter("@enddate", i.date_end ?? "");
                var param4 = new SqlParameter("@created_by", npk ?? "");
                var result = _context.Database.ExecuteSqlRawAsync(
                    "EXEC sp_CICO_RecalInsert @NPK,@startdate,@enddate,@created_by",
                    param1,
                    param2,
                    param3,
                    param4
                    );

                status = true;
                return Json(new { status = status, msg = "Calculation Berhasil Di Submit, Mohon Ditunggu Proses Sedang Berjalan" });

            }
            catch (System.Exception ex)
            {
                return Json(new { status = status });
            }
        }
        [HttpPost]
        public ActionResult SubmitMultiple([FromBody] cls_ i)
        {
            var status = false;
            try
            {
                var npk = "37752";
                List<string> Ids = new List<string>();
                var multiArray = i.txtNpk.Split(new Char[] { ' ', '\n', ',', '\t', '\r' });
                foreach (string author in multiArray)
                {
                    if (author.Trim() != "")
                    {
                        Ids.Add(author);
                    }
                }
                string combinedString = string.Join(",", Ids);
                var param1 = new SqlParameter("@NPK", combinedString ?? "");
                var param2 = new SqlParameter("@startdate", i.date_start ?? "");
                var param3 = new SqlParameter("@enddate", i.date_end ?? "");
                var param4 = new SqlParameter("@created_by", npk ?? "");
                var result = _context.Database.ExecuteSqlRawAsync(
                    "EXEC sp_CICO_RecalInsert @NPK,@startdate,@enddate,@created_by",
                    param1,
                    param2,
                    param3,
                    param4
                    );
                status = true;
                return Json(new { status = status, msg = "Calculation Berhasil Di Submit, Mohon Ditunggu Proses Sedang Berjalan" });

            }
            catch (System.Exception ex)
            {
                return Json(new { status = status });
            }
        }


    }
    public class cls_
    {
        public string txtNpk { get; set; }
        public string date_start { get; set; }
        public string date_end { get; set; }
    }
}