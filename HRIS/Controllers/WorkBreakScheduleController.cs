using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models;
using HRIS.Models.General;
using HRIS.Models.PM3;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace HRIS.Controllers
{

    public class WorkBreakScheduleController : Controller
    {
        private readonly ILogger<WorkBreakScheduleController> _logger;
        private readonly PM3Context _context;

        public WorkBreakScheduleController(ILogger<WorkBreakScheduleController> logger, PM3Context context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult loadGV(string wbsname)
        {
            var status = false;
            try
            {
                var param1 = new SqlParameter("@namaWbs", wbsname ?? "");

                var dataAtt = _context.sp_WBS_getdata.FromSqlRaw("Exec sp_WBS_getdata @namaWbs"
                                , param1).ToList().OrderBy(s => s.WBS_ID);
                status = true;
                return Json(new { status = status, dataAtt = dataAtt });

            }
            catch (System.Exception ex)
            {
                return Json(new { status = status });
            }
        }
        [HttpPost]
        public ActionResult create([FromBody] clsParamwb cls_)
        {
            var status = false;
            try
            {
                var param1 = new SqlParameter("@WBS_ID", cls_.id ?? "");
                var param2 = new SqlParameter("@WBS_Name", cls_.WBS_Name ?? "");
                var param3 = new SqlParameter("@info", cls_.info ?? "");
                var param4 = new SqlParameter("@clock_start", cls_.clock_start ?? "");
                var param5 = new SqlParameter("@clock_end", cls_.clock_end ?? "");
                var param6 = new SqlParameter("@action", cls_.action);
                var param7 = new SqlParameter("@id_user", "1");

                var result2 = _context.Database.ExecuteSqlRawAsync(
                "EXEC sp_WBS_crud @WBS_ID,@WBS_Name,@info,@clock_start,@clock_end,@action,@id_user",
                param1, param2, param3, param4, param5, param6, param7

                );
                return Json(new { status = status, msg = "Berhasil Save Data" });

            }
            catch (System.Exception ex)
            {
                return Json(new { status = status, msg = "" });
            }
        }

        [HttpPost]
        public ActionResult delete(string id)
        {
            var status = false;
            try
            {
                var param1 = new SqlParameter("@WBS_ID", id ?? "");
                var param2 = new SqlParameter("@WBS_Name", "");
                var param3 = new SqlParameter("@info", "");
                var param4 = new SqlParameter("@clock_start", "");
                var param5 = new SqlParameter("@clock_end", "");
                var param6 = new SqlParameter("@action", "delete");
                var param7 = new SqlParameter("@id_user", "1");

                var result2 = _context.Database.ExecuteSqlRawAsync(
                "EXEC sp_WBS_crud @WBS_ID,@WBS_Name,@info,@clock_start,@clock_end,@action,@id_user",
                param1, param2, param3, param4, param5, param6, param7

                );
                return Json(new { status = status, msg = "Berhasil Delete Data" });


            }
            catch (System.Exception ex)
            {
                return Json(new { status = status, msg = "" });
            }
        }




    }
    public class clsParamwb
    {
        public string WBS_ID { get; }
        public string WBS_Name { get; set; }
        public string info { get; set; }
        public string clock_start { get; set; }
        public string clock_end { get; set; }
        public string action { get; set; }
        public string id { get; set; }
    }
}