﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Runtime.ConstrainedExecution;
using System.Threading.Tasks;
using ClosedXML.Excel;
using DocumentFormat.OpenXml.InkML;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Vml;
using HRIS.Controllers.Magang;
using HRIS.Controllers.TMS;
using HRIS.Interfaces;
using HRIS.Interfaces.TMS;
using HRIS.Models;
using HRIS.Models.General;
using HRIS.Models.PM3;
using HRIS.Repositories;
using HRIS.Repositories.TMS;
using HRIS.Services;
using Irony.Parsing;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
namespace HRIS.Controllers.OTManagement
{
    public class OTController : Controller
    {
        private readonly ILogger<OTController> _logger;
        private readonly WebBased _webBased;
        private readonly TrLogErrorRepository _trLogErrorRepository;
        private readonly MenuService _menuService;
        private readonly IOTManagementRepository _otManagementRepository;
        public OTController(ILogger<OTController> logger, IHttpContextAccessor httpContextAccessor, TrLogErrorRepository trLogErrorRepository, MenuService menuService, IOTManagementRepository otManagementRepository)
        {
            _logger = logger;
            _webBased = new WebBased(httpContextAccessor);
            _trLogErrorRepository = trLogErrorRepository;
            _menuService = menuService;
            _otManagementRepository = otManagementRepository;
        }
        public IActionResult MProduction()
        {
            if (HttpContext.Session.GetString("username") == null)
            {
                return RedirectToAction("Default", "Home");
            }
            var id_user = _webBased.UserId;
            ViewBag.leftmenu = this.LoadMenu(id_user);
            ViewBag.npk = _webBased.Npk;
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> loadGetMprod([FromBody] clsOT f)
        {
            var status = false;
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            try
            {
                var search = Request.Query["search[value]"].ToString();
                var start = Convert.ToInt32(Request.Query["start"]);
                var lenght = Convert.ToInt32(Request.Query["length"]);

                int pageSize = lenght != null ? Convert.ToInt32(lenght) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int totalRecords = 0;

                var data = await _otManagementRepository.GetDataMprod(f);
                totalRecords = data.Count();

                DatatableToJson datatableToJson = new DatatableToJson();
                datatableToJson.recordsFiltered = totalRecords;
                datatableToJson.recordsTotal = totalRecords;
                datatableToJson.data = data;

                return Json(datatableToJson);

            }
            catch (System.Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { status = status });
            }
        }
        [HttpPost]
        public async Task<IActionResult> CreateMprod([FromBody] clsin s)
        {
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            var status = false;
            try
            {
                clsOT t = new clsOT();
                t.npkLogin = _webBased.Npk;
                t.LocationId = _webBased.LocationId;
                await _otManagementRepository.InsertMprod(t);
                status = true;
                return Json(new { status = status, msg = "Data Berhasil Diupdate" });
            }
            catch (Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { data = "", status = status, msg = "Data Tidak Dapat Di Proses" });
            }
        }
        [HttpPost]
        public async Task<IActionResult> uploadMprod([FromForm] IFormFile f)
        {
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            var status = false;
            try
            {
                if (f != null && f.Length > 0 && !string.IsNullOrEmpty(f.FileName))
                {
                    string fileName = f.FileName;
                    ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
                    using (var stream = new MemoryStream())
                    {
                        f.CopyTo(stream);
                        using (var package = new ExcelPackage(stream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();
                            var noOfCol = workSheet.Dimension.End.Column;
                            var noOfRow = workSheet.Dimension.End.Row;
                            List<uWorkSch> dtvalid = new List<uWorkSch>();
                            List<uWorkSch> dts = new List<uWorkSch>();
                            int no = 1;
                            int no_ = 1;
                            var msg = "";
                            var isError = false;

                            for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                            {
                                string shop = Convert.ToString(workSheet.Cells[rowIterator, 1].Value);
                                string line = Convert.ToString(workSheet.Cells[rowIterator, 2].Value);
                                Overtime_tbl_m_production tbl = new Overtime_tbl_m_production();
                                if (!string.IsNullOrEmpty(shop) && !string.IsNullOrEmpty(line))
                                {
                                    var data_shop_ = await _otManagementRepository.getShop();
                                    var data_shop= data_shop_.Where(a => a.shop.ToLower() == shop.ToLower()).FirstOrDefault();
                                    Overtime_tbl_r_shop tbl_R_Shop = new Overtime_tbl_r_shop();
                                    if (data_shop == null)
                                    {
                                        msg = "Shop belum terdaftar di database,Silahkan Menghubungi HR Untuk Ditambahkan";
                                        isError = true;
                                        break;
                                    }
                                    Overtime_tbl_r_line tbl_R_Line = new Overtime_tbl_r_line();
                                    var data_line_ = await _otManagementRepository.getLine();
                                    var data_line = data_line_.Where(a => a.line.ToLower() == line.ToLower()).FirstOrDefault();
                                    if (data_line == null)
                                    {
                                        msg = "Line belum terdaftar di database,Silahkan Menghubungi HR Untuk Ditambahkan";
                                        isError = true;
                                        break;
                                    }


                                }


                            }
                            if (isError == false)
                            {
                                for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                                {
                                    string shop = Convert.ToString(workSheet.Cells[rowIterator, 1].Value);
                                    string line = Convert.ToString(workSheet.Cells[rowIterator, 2].Value);
                                    string pis = Convert.ToString(workSheet.Cells[rowIterator, 3].Value);
                                    int durasi = workSheet.Cells[rowIterator, 4].GetCellValue<Int32>();
                                    string tglValid = Convert.ToString(workSheet.Cells[rowIterator, 5].Value);
                                    string tglEnd = Convert.ToString(workSheet.Cells[rowIterator, 6].Value);
                                    string dot_hot = Convert.ToString(workSheet.Cells[rowIterator, 7].Value);
                                    Overtime_tbl_m_production tbl = new Overtime_tbl_m_production();
                                    if (!string.IsNullOrEmpty(shop) && !string.IsNullOrEmpty(line))
                                    {
                                        Overtime_tbl_r_shop tbl_R_Shop = new Overtime_tbl_r_shop();
                                        var data_shop_ = await _otManagementRepository.getShop();
                                        var data_shop = data_shop_.Where(a => a.shop.ToLower() == shop.ToLower()).FirstOrDefault();
                                        if (data_shop != null)
                                        {
                                            tbl_R_Shop = data_shop;
                                        }
                                        Overtime_tbl_r_line tbl_R_Line = new Overtime_tbl_r_line();
                                        var data_line_ = await _otManagementRepository.getLine();
                                        var data_line = data_line_.Where(a => a.line.ToLower() == line.ToLower()).FirstOrDefault();
                                        if (data_line != null)
                                        {
                                            tbl_R_Line = data_line;
                                        }
                                        if (tbl_R_Shop != null && tbl_R_Line != null)
                                        {
                                            var dot_hot_ = "";
                                            var pis_ = "";
                                            if (tbl_R_Shop.is_pis != null && tbl_R_Shop.is_pis == true)
                                            {
                                                dot_hot_ = dot_hot;
                                                pis_ = pis;
                                            }
                                            clsOT t= new clsOT();
                                            t.pid_shop= tbl_R_Shop.pid_shop.ToString();
                                            t.shop=shop;
                                            t.pid_line=tbl_R_Line.pid_line.ToString();
                                            t.line = line;
                                            t.pis= pis;
                                            t.durasi = durasi.ToString();
                                            t.npkLogin= _webBased.Npk;
                                            t.LocationId= _webBased.LocationId;
                                            t.tglEfektif = tglValid;
                                            t.tglEnd= tglEnd;
                                            t.dot_hot= dot_hot;
                                            t.durasiPIS = pis_;

                                            await _otManagementRepository.InsertMprod(t);
                                        }
                                    }

                                }
                                status = true;
                                return Json(new { status = status, msg = "Proses upload master production telah berhasil" });
                            }
                            else
                            {
                                status = false;
                                return Json(new { status = status, msg = "Data Tidak Dapat Di Proses" });
                            }


                        }
                    }


                }
                else
                {
                    return Json(new { data = "", status = status, msg = "Data Tidak Dapat Di Proses" });
                }
            }
            catch (Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { data = "", status = status, msg = "Data Tidak Dapat Di Proses" });
            }
        }
        private string LoadMenu(string id_user)
        {
            return _menuService.RecursiveMenu(id_user);
        }

    }
}
