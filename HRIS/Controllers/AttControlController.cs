using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models;
using HRIS.Models.General;
using HRIS.Models.PM3;
using HRIS.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace HRIS.Controllers
{

    public class AttControlController : Controller
    {
        private readonly ILogger<AttControlController> _logger;
        private readonly PM3Context _context;
        private readonly WebBased _webBased;
        private readonly MenuService _menuService;

        public AttControlController(ILogger<AttControlController> logger, PM3Context context, IHttpContextAccessor httpContextAccessor, MenuService menuService)
        {
            _logger = logger;
            _context = context;
            _webBased = new WebBased(httpContextAccessor);
            _menuService = menuService;
        }

        public IActionResult Index()
        {
            var id_user = _webBased.UserId;
            ViewBag.leftmenu = this.LoadMenu(id_user);
            return View();
        }
        [HttpPost]
        public ActionResult loadGV(string Npk, string startdate, string endDate)
        {
            var status = false;
            try
            {
                var LevelIdxNo = "2";
                var DivisionId = "3";
                var DeptId = "0";
                var NpkLogin = "63827";
                var id_user = "0";
                var depthandle = "";
                var lochandle = "";
                status = true;
                var param1 = new SqlParameter("@LevelIdxNo", LevelIdxNo ?? "");
                var param2 = new SqlParameter("@DivisionId", DivisionId ?? "");
                var param3 = new SqlParameter("@DeptId", DeptId ?? "");
                var param4 = new SqlParameter("@NpkLogin", NpkLogin ?? "");
                var param5 = new SqlParameter("@Npk", Npk ?? "");
                var param6 = new SqlParameter("@id_user", id_user ?? "");
                var param7 = new SqlParameter("@depthandle", depthandle ?? "");
                var param8 = new SqlParameter("@lochandle", lochandle ?? "");
                var param9 = new SqlParameter("@startdate", startdate ?? "");
                var param10 = new SqlParameter("@endDate", endDate ?? "");
                var dataAtt = _context.sp_Attendance_control.FromSqlRaw("Exec sp_Attendance_control @LevelIdxNo,@DivisionId,@DeptId,@NpkLogin,@Npk,@id_user,@depthandle,@lochandle,@startdate,@endDate"
                                , param1, param2, param3, param4, param5, param6, param7, param8, param9, param10).ToList();
                status = true;
                return Json(new { status = status, dataAtt = dataAtt });

            }
            catch (System.Exception ex)
            {
                return Json(new { status = status });
            }
        }
        private string LoadMenu(string id_user)
        {
            return _menuService.RecursiveMenu(id_user);
        }

    }
}