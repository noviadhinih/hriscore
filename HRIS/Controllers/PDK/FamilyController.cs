using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using HRIS.Interfaces.PDK;
using HRIS.Models;
using HRIS.Models.General;
using HRIS.Models.PM3;
using HRIS.Services;
using HRIS.Repositories;

namespace HRIS.Controllers.PDK
{
    [Route("PDK/[controller]")]
    public class FamilyController : Controller
    {
        private readonly ILogger<FamilyController> _logger;
        private readonly IFamilyRepository _familyRepository;
        private readonly WebBased _webBased;
        private readonly TrLogErrorRepository _trLogErrorRepository;
        private readonly MenuService _menuService;

        public FamilyController(ILogger<FamilyController> logger, IFamilyRepository familyRepository, IHttpContextAccessor httpContextAccessor, TrLogErrorRepository trLogErrorRepository, MenuService menuService)
        {
            _logger = logger;
            _familyRepository = familyRepository;
            _webBased = new WebBased(httpContextAccessor);
            _trLogErrorRepository = trLogErrorRepository;
            _menuService = menuService;
        }

        [HttpGet("Index")]
        public async Task<IActionResult> Index()
        {
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            try
            {
                var id_user = _webBased.UserId;
                string npk = _webBased.Npk;
                var familyData = await _familyRepository.GetFamilyDataAsync(npk);
                ViewBag.leftmenu = this.LoadMenu(id_user);
                return View(familyData);
            }
            catch (Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return StatusCode(500, ex.Message);
            }
        }

        private string LoadMenu(string id_user)
        {
            return _menuService.RecursiveMenu(id_user);
        }

        [HttpGet("Create")]
        public IActionResult Create()
        {
            return View();
        }
    }
}
