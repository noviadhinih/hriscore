using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Interfaces;
using HRIS.Interfaces.TMS;
using HRIS.Models;
using HRIS.Models.General;
using HRIS.Models.PM3;
using HRIS.Repositories;
using HRIS.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;


namespace HRIS.Controllers.TMS
{

    public class RTMSController : Controller
    {
        private readonly ILogger<RTMSController> _logger;
        private readonly WebBased _webBased;
        private readonly ITMSRepository _tMSRepository;
        private readonly TrLogErrorRepository _trLogErrorRepository;
        private readonly MenuService _menuService;


        public RTMSController(ILogger<RTMSController> logger, IHttpContextAccessor httpContextAccessor, TrLogErrorRepository trLogErrorRepository, MenuService menuService, ITMSRepository tMSRepository)
        {
            _logger = logger;
            _webBased = new WebBased(httpContextAccessor);
            _trLogErrorRepository = trLogErrorRepository;
            _menuService = menuService;
            _tMSRepository = tMSRepository;
        }

        public IActionResult Supem()
        {
            if (HttpContext.Session.GetString("username") == null)
            {
                return RedirectToAction("Default", "Home");
            }
            var id_user = _webBased.UserId;
            ViewBag.leftmenu = this.LoadMenu(id_user);
            return View();
        }
        public IActionResult SKTA()
        {
            if (HttpContext.Session.GetString("username") == null)
            {
                return RedirectToAction("Default", "Home");
            }
            var id_user = _webBased.UserId;
            ViewBag.leftmenu = this.LoadMenu(id_user);
            return View();
        }
        public IActionResult CUTI()
        {
            if (HttpContext.Session.GetString("username") == null)
            {
                return RedirectToAction("Default", "Home");
            }
            var id_user = _webBased.UserId;
            ViewBag.leftmenu = this.LoadMenu(id_user);
            return View();
        }
        public IActionResult SPL()
        {
            if (HttpContext.Session.GetString("username") == null)
            {
                return RedirectToAction("Default", "Home");
            }
            var id_user = _webBased.UserId;
            ViewBag.leftmenu = this.LoadMenu(id_user);
            return View();
        }
        #region SUPEM
        [HttpPost]
        public async Task<ActionResult> loadSupem([FromBody] clss_ cls_)
        {
            var status = false;
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            try
            {
                var search = Request.Query["search[value]"].ToString();
                var start = Convert.ToInt32(Request.Query["start"]);
                var lenght = Convert.ToInt32(Request.Query["length"]);

                int pageSize = lenght != null ? Convert.ToInt32(lenght) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int totalRecords = 0;

                cls_.deptHandle = _webBased.DeptHandle;
                cls_.locHandle = _webBased.LocationHandle;

                var data = await _tMSRepository.RejectGetDataSupem(cls_.deptHandle, cls_.locHandle, cls_.npk_, cls_.tgl_request_start, cls_.tgl_request_end, cls_.status, cls_.tahun, cls_.npk_input);
                totalRecords = data.Count();

                DatatableToJson datatableToJson = new DatatableToJson();
                datatableToJson.recordsFiltered = totalRecords;
                datatableToJson.recordsTotal = totalRecords;
                datatableToJson.data = data;

                return Json(datatableToJson);

            }
            catch (System.Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { status = status });
            }
        }
        [HttpPost]
        public async Task<IActionResult> RejectSupem([FromBody] List<clsApv> clsapv_, string reason)
        {
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            var status = false;
            try
            {
                List<string> Ids = new List<string>();
                foreach (var item in clsapv_)
                {
                    Ids.Add(item.keyID);
                }
                var id = Ids.Distinct().ToList();
                if (id.Count > 0)
                {
                    string combinedString = string.Join(",", id);

                    var return_ = await _tMSRepository.RejectSupem(combinedString, reason, _webBased.Npk);
                    status = return_.status;
                    if (status == true)
                    {
                        return Json(new { status = status, msg = return_.remarks });
                    }
                    else
                    {
                        await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, return_.remarks);
                        return Json(new { status = status, msg = "Data Tidak Dapat Di Proses" });
                    }

                }
                else
                {
                    return Json(new { status = status, msg = "Tidak Ada Data Yang Dipilih" });
                }
            }
            catch (Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { status = status, msg = "Data Tidak Dapat Di Proses" });
            }
        }
        #endregion
        #region SKTA
        [HttpPost]
        public async Task<ActionResult> loadSKTA([FromBody] clss_ cls_)
        {
            var status = false;
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            try
            {
                var search = Request.Query["search[value]"].ToString();
                var start = Convert.ToInt32(Request.Query["start"]);
                var lenght = Convert.ToInt32(Request.Query["length"]);

                int pageSize = lenght != null ? Convert.ToInt32(lenght) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int totalRecords = 0;

                cls_.deptHandle = _webBased.DeptHandle;
                cls_.locHandle = _webBased.LocationHandle;

                var data = await _tMSRepository.RejectGetDataSKTA(cls_.deptHandle, cls_.locHandle, cls_.npk_, cls_.tgl_request_start, cls_.tgl_request_end, cls_.status, cls_.tahun, cls_.npk_input);
                totalRecords = data.Count();

                DatatableToJson datatableToJson = new DatatableToJson();
                datatableToJson.recordsFiltered = totalRecords;
                datatableToJson.recordsTotal = totalRecords;
                datatableToJson.data = data;

                return Json(datatableToJson);

            }
            catch (System.Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { status = status });
            }
        }
        [HttpPost]
        public async Task<IActionResult> RejectSKTA([FromBody] List<clsApv> clsapv_, string reason)
        {
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            var status = false;
            try
            {
                List<string> Ids = new List<string>();
                foreach (var item in clsapv_)
                {
                    Ids.Add(item.keyID);
                }
                var id = Ids.Distinct().ToList();
                if (id.Count > 0)
                {
                    string combinedString = string.Join(",", id);

                    var return_ = await _tMSRepository.RejectSKTA(combinedString, reason, _webBased.Npk);
                    status = return_.status;
                    if (status == true)
                    {
                        return Json(new { status = status, msg = return_.remarks });
                    }
                    else
                    {
                        await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, return_.remarks);
                        return Json(new { status = status, msg = "Data Tidak Dapat Di Proses" });
                    }

                }
                else
                {
                    return Json(new { status = status, msg = "Tidak Ada Data Yang Dipilih" });
                }
            }
            catch (Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { status = status, msg = "Data Tidak Dapat Di Proses" });
            }
        }
        #endregion
        #region CUTI
        [HttpPost]
        public async Task<ActionResult> loadCuti([FromBody] clss_ cls_)
        {
            var status = false;
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            try
            {
                var search = Request.Query["search[value]"].ToString();
                var start = Convert.ToInt32(Request.Query["start"]);
                var lenght = Convert.ToInt32(Request.Query["length"]);

                int pageSize = lenght != null ? Convert.ToInt32(lenght) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int totalRecords = 0;

                cls_.deptHandle = _webBased.DeptHandle;
                cls_.locHandle = _webBased.LocationHandle;

                var data = await _tMSRepository.RejectGetDataCuti(cls_.deptHandle, cls_.locHandle, cls_.npk_, cls_.tgl_request_start, cls_.tgl_request_end, cls_.status, cls_.tahun, cls_.npk_input);
                totalRecords = data.Count();

                DatatableToJson datatableToJson = new DatatableToJson();
                datatableToJson.recordsFiltered = totalRecords;
                datatableToJson.recordsTotal = totalRecords;
                datatableToJson.data = data;

                return Json(datatableToJson);

            }
            catch (System.Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { status = status });
            }
        }
        [HttpPost]
        public async Task<IActionResult> RejectCuti([FromBody] List<clsApv> clsapv_, string reason)
        {
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            var status = false;
            try
            {
                List<string> Ids = new List<string>();
                foreach (var item in clsapv_)
                {
                    Ids.Add(item.keyID);
                }
                var id = Ids.Distinct().ToList();
                if (id.Count > 0)
                {
                    string combinedString = string.Join(",", id);

                    var return_ = await _tMSRepository.RejectCuti(combinedString, reason, _webBased.UserId);
                    status = return_.status;
                    if (status == true)
                    {
                        return Json(new { status = status, msg = return_.remarks });
                    }
                    else
                    {
                        await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, return_.remarks);
                        return Json(new { status = status, msg = "Data Tidak Dapat Di Proses" });
                    }

                }
                else
                {
                    return Json(new { status = status, msg = "Tidak Ada Data Yang Dipilih" });
                }
            }
            catch (Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { status = status, msg = "Data Tidak Dapat Di Proses" });
            }
        }
        #endregion
        #region SPL
        [HttpPost]
        public async Task<ActionResult> loadSPL([FromBody] clss_ cls_)
        {
            var status = false;
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            try
            {
                var search = Request.Query["search[value]"].ToString();
                var start = Convert.ToInt32(Request.Query["start"]);
                var lenght = Convert.ToInt32(Request.Query["length"]);

                int pageSize = lenght != null ? Convert.ToInt32(lenght) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int totalRecords = 0;

                cls_.deptHandle = _webBased.DeptHandle;
                cls_.locHandle = _webBased.LocationHandle;

                var data = await _tMSRepository.RejectGetDataSPL(cls_.deptHandle, cls_.locHandle, cls_.npk_, cls_.tgl_request_start, cls_.tgl_request_end, cls_.status, cls_.tahun, cls_.npk_input);
                totalRecords = data.Count();

                DatatableToJson datatableToJson = new DatatableToJson();
                datatableToJson.recordsFiltered = totalRecords;
                datatableToJson.recordsTotal = totalRecords;
                datatableToJson.data = data;

                return Json(datatableToJson);

            }
            catch (System.Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { status = status });
            }
        }
        [HttpPost]
        public async Task<IActionResult> RejectSPL([FromBody] List<clsApv> clsapv_, string reason)
        {
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            var status = false;
            try
            {
                List<string> Ids = new List<string>();
                foreach (var item in clsapv_)
                {
                    Ids.Add(item.keyID);
                }
                var id = Ids.Distinct().ToList();
                if (id.Count > 0)
                {
                    string combinedString = string.Join(",", id);

                    var return_ = await _tMSRepository.RejectSPL(combinedString, reason, _webBased.UserId);
                    status = return_.status;
                    if (status == true)
                    {
                        return Json(new { status = status, msg = return_.remarks });
                    }
                    else
                    {
                        await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, return_.remarks);
                        return Json(new { status = status, msg = "Data Tidak Dapat Di Proses" });
                    }

                }
                else
                {
                    return Json(new { status = status, msg = "Tidak Ada Data Yang Dipilih" });
                }
            }
            catch (Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { status = status, msg = "Data Tidak Dapat Di Proses" });
            }
        }

        #endregion


        private string LoadMenu(string id_user)
        {
            return _menuService.RecursiveMenu(id_user);
        }
    }
    public class clss_
    {
        public string npk { get; }
        public string tgl_request_start { get; set; }
        public string tgl_request_end { get; set; }
        public string status { get; set; }
        public string tahun { get; set; }
        public string npk_input { get; set; }
        public string deptHandle { get; set; }
        public string locHandle { get; set; }
        public string npk_ { get; set; }

    }
    public class clsApv
    {
        public string keyID { get; set; }
    }
}