using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using HRIS.Interfaces;
using HRIS.Interfaces.TMS;
using HRIS.Models;
using HRIS.Models.General;
using HRIS.Models.PM3;
using HRIS.Repositories;
using HRIS.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace HRIS.Controllers.TMS
{

    public class ApvTMSController : Controller
    {
        private readonly ILogger<AddTMSController> _logger;
        private readonly WebBased _webBased;
        private readonly TrLogErrorRepository _trLogErrorRepository;
        private readonly MenuService _menuService;
        private readonly ITMSRepository _tMSRepository;

        public ApvTMSController(ILogger<AddTMSController> logger, IHttpContextAccessor httpContextAccessor, TrLogErrorRepository trLogErrorRepository, MenuService menuService, ITMSRepository tMSRepository)
        {
            _logger = logger;
            _webBased = new WebBased(httpContextAccessor);
            _trLogErrorRepository = trLogErrorRepository;
            _menuService = menuService;
            _tMSRepository = tMSRepository;
        }
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("username") == null)
            {
                return RedirectToAction("Default", "Home");
            }
            var id_user = _webBased.UserId;
            ViewBag.leftmenu = this.LoadMenu(id_user);
            ViewBag.npk = _webBased.Npk;
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> loadApproval([FromBody] SKTA filterSKTA)
        {
            var status = false;
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            try
            {
                var search = Request.Query["search[value]"].ToString();
                var start = Convert.ToInt32(Request.Query["start"]);
                var lenght = Convert.ToInt32(Request.Query["length"]);

                int pageSize = lenght != null ? Convert.ToInt32(lenght) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int totalRecords = 0;


                DatatableToJson datatableToJson = new DatatableToJson();
                if (filterSKTA.jenisTms == "SKTA")
                {
                    var dataSKTA = await _tMSRepository.GetSKTAApproval(filterSKTA, _webBased.UserId, _webBased.Npk);
                    totalRecords = dataSKTA.Count();


                    datatableToJson.recordsFiltered = totalRecords;
                    datatableToJson.recordsTotal = totalRecords;
                    datatableToJson.data = dataSKTA;
                }
                else if (filterSKTA.jenisTms == "SUPEM")
                {
                    var dataSKTA = await _tMSRepository.GetSUPEMApproval(filterSKTA, _webBased.UserId, _webBased.Npk);
                    totalRecords = dataSKTA.Count();


                    datatableToJson.recordsFiltered = totalRecords;
                    datatableToJson.recordsTotal = totalRecords;
                    datatableToJson.data = dataSKTA;
                }
                else if (filterSKTA.jenisTms == "CUTI")
                {
                    var dataSKTA = await _tMSRepository.GetCUTIApproval(filterSKTA, _webBased.UserId, _webBased.Npk);
                    totalRecords = dataSKTA.Count();


                    datatableToJson.recordsFiltered = totalRecords;
                    datatableToJson.recordsTotal = totalRecords;
                    datatableToJson.data = dataSKTA;
                }
                else if (filterSKTA.jenisTms == "SPL")
                {
                    var dataSKTA = await _tMSRepository.GetSPLApproval(filterSKTA, _webBased.UserId, _webBased.Npk);
                    totalRecords = dataSKTA.Count();


                    datatableToJson.recordsFiltered = totalRecords;
                    datatableToJson.recordsTotal = totalRecords;
                    datatableToJson.data = dataSKTA;
                }



                return Json(datatableToJson);

            }
            catch (System.Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { status = status });
            }
        }
        [HttpPost]
        public async Task<IActionResult> ApprovalTMS([FromBody] List<clsApv> clsapv_, string jenisTms)
        {
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            var status = false;
            var msg = "";
            try
            {
                List<string> Ids = new List<string>();
                foreach (var item in clsapv_)
                {
                    Ids.Add(item.keyID);
                }
                var id = Ids.Distinct().ToList();
                if (id.Count > 0)
                {
                    string combinedString = string.Join(",", id);
                    if (jenisTms == "SKTA")
                    {
                        var return_ = await _tMSRepository.ApproveSKTA(combinedString, "A",_webBased.Npk, "");
                        status = return_.status;
                        msg = return_.remarks;
                    }
                    else if (jenisTms == "SUPEM")
                    {
                        var return_ = await _tMSRepository.ApproveSUPEM(combinedString, "A",_webBased.Npk, "");
                        status = return_.status;
                        msg = return_.remarks;
                    }
                    else if (jenisTms == "CUTI")
                    {
                        var return_ = await _tMSRepository.ApproveCUTI(combinedString, "A",_webBased.Npk, "");
                        status = return_.status;
                        msg = return_.remarks;
                    }
                      else if (jenisTms == "SPL")
                    {
                        var return_ = await _tMSRepository.ApproveSPL(combinedString, "A",_webBased.Npk, "");
                        status = return_.status;
                        msg = return_.remarks;
                    }



                    if (status == true)
                    {
                        return Json(new { status = status, msg =msg });
                    }
                    else
                    {
                        await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, msg);
                        return Json(new { status = status, msg = "Data Tidak Dapat Di Proses" });
                    }

                }
                else
                {
                    return Json(new { status = status, msg = "Tidak Ada Data Yang Dipilih" });
                }
            }
            catch (Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { status = status, msg = "Data Tidak Dapat Di Proses" });
            }
        }
        private string LoadMenu(string id_user)
        {
            return _menuService.RecursiveMenu(id_user);
        }
    }
}