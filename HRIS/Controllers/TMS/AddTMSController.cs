using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using HRIS.Interfaces;
using HRIS.Interfaces.TMS;
using HRIS.Models;
using HRIS.Models.General;
using HRIS.Models.PM3;
using HRIS.Repositories;
using HRIS.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace HRIS.Controllers.TMS
{
    public class AddTMSController : Controller
    {
        private readonly ILogger<AddTMSController> _logger;
        private readonly WebBased _webBased;
        private readonly TrLogErrorRepository _trLogErrorRepository;
        private readonly MenuService _menuService;
        private readonly ITMSRepository _tMSRepository;

        public AddTMSController(ILogger<AddTMSController> logger, IHttpContextAccessor httpContextAccessor, TrLogErrorRepository trLogErrorRepository, MenuService menuService, ITMSRepository tMSRepository)
        {
            _logger = logger;
            _webBased = new WebBased(httpContextAccessor);
            _trLogErrorRepository = trLogErrorRepository;
            _menuService = menuService;
            _tMSRepository = tMSRepository;
        }

        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("username") == null)
            {
                return RedirectToAction("Default", "Home");
            }
            var id_user = _webBased.UserId;
            ViewBag.leftmenu = this.LoadMenu(id_user);
            ViewBag.npk = _webBased.Npk;
            return View();
        }
        #region SUPEM
        [HttpPost]
        public async Task<ActionResult> loadSUPEM([FromBody] clsfilter cls_)
        {
            var status = false;
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            try
            {
                var search = Request.Query["search[value]"].ToString();
                var start = Convert.ToInt32(Request.Query["start"]);
                var lenght = Convert.ToInt32(Request.Query["length"]);

                int pageSize = lenght != null ? Convert.ToInt32(lenght) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int totalRecords = 0;

                cls_.deptHandle = _webBased.DeptHandle;
                cls_.locHandle = _webBased.LocationHandle;

                var data = await _tMSRepository.GetDataSupem(cls_.deptHandle, cls_.locHandle, cls_.pr, cls_.yr, cls_.npk, cls_.nama, cls_.emp_status, cls_.status, cls_.divisi,
                                                              cls_.dept, cls_.golongan, cls_.npk_input, cls_.date_input, cls_.costcenter, cls_.shift, cls_.type_spl, _webBased.UserId,
                                                              _webBased.Npk, cls_.tgl_mulai, cls_.tgl_selesai);
                totalRecords = data.Count();

                DatatableToJson datatableToJson = new DatatableToJson();
                datatableToJson.recordsFiltered = totalRecords;
                datatableToJson.recordsTotal = totalRecords;
                datatableToJson.data = data;

                return Json(datatableToJson);

            }
            catch (System.Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { status = status });
            }
        }
        [HttpPost]
        public async Task<IActionResult> CreateSupem([FromBody] clsin s)
        {
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            var status = false;
            try
            {
                string format = "yyyy-MM-dd HH:mm";
                SUPEM Supem = new SUPEM();
                Supem.apv_atasan = s.apv_atasan;
                Supem.apv_spv = s.apv_spv;
                Supem.date_end = ConvertToNullableDateTime(s.date_end);
                Supem.date_start = ConvertToNullableDateTime(s.date_start);
                Supem.info = s.info;
                if (s.is_status_jam == "false")
                {
                    Supem.is_status_jam = false;
                }
                else if (s.is_status_jam == "true")
                {
                    Supem.is_status_jam = true;
                }
                Supem.max_days = s.max_days;
                Supem.npk = s.npk;
                Supem.npk_atasan = s.npk_atasan;
                Supem.permission_date = ConvertToNullableDateTime(s.permission_date);
                Supem.permit_id = s.permit_id;


                Supem.ID_lokasi = _webBased.LocationId;
                Supem.ID_user = _webBased.UserId;
                Supem.stats = "P";
                Supem.npk_create = _webBased.Npk;
                var jam_masuk = "";
                var jam_keluar = "";
                var tanggal = Supem.permission_date.Value.ToString("yyyy-MM-dd");
                if (Supem.is_status_jam && Supem.permit_id != "24" && Supem.permit_id != "25" && Supem.permit_id != "26" && Supem.permit_id != "27")
                {
                    jam_masuk = Supem.date_start.Value.ToString("HH:mm");
                    jam_keluar = Supem.date_end.Value.ToString("HH:mm");

                    DateTime startDate = Convert.ToDateTime(tanggal + " " + jam_masuk);
                    DateTime endDate = Convert.ToDateTime(tanggal + " " + jam_keluar);
                    Supem.date_start = startDate;
                    Supem.date_end = endDate;
                    Supem.jam_mulai = tanggal + " " + jam_masuk;
                    Supem.jam_akhir = tanggal + " " + jam_keluar;
                }
                else
                {
                    Supem.jam_mulai = Supem.date_start.Value.ToString("yyyy-MM-dd HH:mm");
                    Supem.jam_akhir = Supem.date_end.Value.ToString("yyyy-MM-dd HH:mm");
                }
                Supem.tanggal = tanggal;
                var dataPermissionType_ = await _tMSRepository.GetPermissionTypeByID(Supem.permit_id.ToString());
                var dataPermissionType = dataPermissionType_.FirstOrDefault();
                var st = (DateTime)Supem.date_start;
                var end = (DateTime)Supem.date_end;
                if (Supem.permit_id == "24" || Supem.permit_id == "25" || Supem.permit_id == "26" || Supem.permit_id == "27")
                {
                    st = (DateTime)Supem.permission_date;
                    end = (DateTime)Supem.permission_date;
                }
                TimeSpan totalDays = (Supem.date_end.Value - Supem.date_start.Value);
                int max_days = int.Parse(Supem.max_days);
                bool IsChange = true;
                if ((Supem.date_end < Supem.date_start) && Supem.permit_id != "24" && Supem.permit_id != "25" && Supem.permit_id != "26" && Supem.permit_id != "27")
                    return Json(new { data = "", message = "Tanggal selesai harus harus lebih lebih besar dari tanggal mulai", success = false });

                if (totalDays.TotalDays > max_days && Supem.permit_id != "24")
                    return Json(new { data = "", message = "Jangka waktu supem melebihi maksimum hari yang ditentukan", success = false });
                int libur = 0;
                var dataEWS = await _tMSRepository.GetWorkingSchedule(Supem.npk, st, end, "Free");
                if (dataEWS.Count > 0)
                    libur = dataEWS.Count;
                var dataTotalPermission_ = await _tMSRepository.GetByID(Supem.npk, (DateTime)Supem.date_start, (DateTime)Supem.date_end, "R");
                var dataTotalPermission = dataTotalPermission_.FirstOrDefault().TotalData;
                var cicoStats_ = await _tMSRepository.GetStats(dataPermissionType.cico_code);
                var cicoStats = cicoStats_.FirstOrDefault();
                if ((cicoStats != null && cicoStats.priority != 1) || (cicoStats != null && cicoStats.priority == 1 && dataTotalPermission <= 0) || cicoStats == null)
                {
                    var cicoPriority_ = await _tMSRepository.GetCicoPriority(Supem.npk, st.ToString(), end.ToString());
                    var cicoPriority = cicoPriority_.FirstOrDefault();
                    if (cicoPriority.TotalData == 0)
                    {
                        string npk = Supem.npk;
                        Supem.stats = "P";
                        Supem.last_update = DateTime.Now;
                        Supem.ID_user = _webBased.UserId;
                        if (IsChange)
                        {
                            int totalDay = (Supem.date_end - Supem.date_start).Value.Days;
                            if (dataEWS.Count == 1 && totalDay <= 1 && dataPermissionType.status_libur == 0)
                            {
                                return Json(new { data = "", message = "Anda tidak bisa memasukkan surat pemberitahuan di tanggal ini", success = false });
                            }
                            var isExist = await _tMSRepository.CheckLeave(Supem.npk, st.ToString("yyyy-MM-dd"), end.ToString("yyyy-MM-dd"));
                            if (isExist.Count > 0)
                            {
                                return Json(new { data = "", message = "Anda tidak bisa membuat surat pemberitahuan di tanggal ini, terdapat cuti diantara tanggal tersebut", success = false });
                            }
                            else
                            {
                                var return_ = await _tMSRepository.InsertSupem(Supem);
                                return Json(new { data = "", message = return_.remarks, success = return_.status });

                            }
                        }
                        else
                        {
                            return Json(new { data = "", status = status, msg = "Data Tidak Dapat Di Proses" });

                        }
                    }
                    else
                    {
                        return Json(new { data = "", status = status, msg = "Data Tidak Dapat Di Proses" });
                    }
                }
                else
                    return Json(new { data = "", message = "Permission dengan prioritas ini tidak dapat digabung dengan izin lainnya", success = false });

            }
            catch (Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { data = "", status = status, msg = "Data Tidak Dapat Di Proses" });
            }
        }
        public static DateTime? ConvertToNullableDateTime(string dateString)
        {
            if (DateTime.TryParse(dateString, out DateTime parsedDateTime))
            {
                return parsedDateTime;
            }
            else
            {
                return null;
            }
        }
        [HttpPost]
        public async Task<IActionResult> ExportSUPEM([FromBody] List<clsApv> cls_)
        {
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            var status = false;
            List<string> Ids = new List<string>();
            try
            {
                // Buat DataTable dari list cls_
                DataTable dt = new DataTable("DataSUPEM");
                dt.Columns.AddRange(new DataColumn[14] {
                    new DataColumn("NPK"),
                    new DataColumn("Nama Karyawan"),
                    new DataColumn("Divisi"),
                    new DataColumn("Dept"),
                    new DataColumn("Tgl. SUPEM"),
                    new DataColumn("Tipe SUPEM"),
                    new DataColumn("Info"),
                    new DataColumn("Status"),
                    new DataColumn("NPK Input"),
                    new DataColumn("Tgl. Input"),
                    new DataColumn("NPK Atasan"),
                    new DataColumn("Tgl. Approve Atasan"),
                    new DataColumn("NPK HRD"),
                    new DataColumn("Tgl. Approve HRD")
                });

                foreach (var item in cls_)
                {
                    Ids.Add(item.keyID);
                }
                var leaveID_ = Ids.ToList().Distinct();
                string combinedString = string.Join(",", Ids);
                var data = await _tMSRepository.GetdataRekapExcelSupem(combinedString);
                foreach (var item in data)
                {
                    dt.Rows.Add(item.npk, item.emp_name, item.div_name, item.dept_name, item.permission_dateText, item.permit_code_name,
                                item.info, item.statsNm, item.npk_input, item.date_input, item.npk_apv, item.date_atasan, item.npk_hrd, item.date_hrd);
                }

                // Export DataTable ke dalam file Excel
                byte[] fileContents = ExportToExcel(dt, "DataSUpem");

                // Return file Excel sebagai response
                return File(fileContents, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "DataSupem.xlsx");
            }
            catch (System.Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { data = "", status = status, msg = "Data Tidak Dapat Di Proses" });
            }
        }


        private byte[] ExportToExcel(DataTable dt, string sheetName)
        {
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add(dt, sheetName);
                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    return stream.ToArray();
                }
            }
        }
        #endregion

        [HttpPost]
        public async Task<ActionResult> GetEmployee(string add_npk)
        {
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            var status = false;
            try
            {
                var employee = await _tMSRepository.GetValidateSKTAByNPK(add_npk, _webBased.UserId, _webBased.DeptId.ToString(), _webBased.LocationId);
                var d = employee.FirstOrDefault();
                if (d != null)
                {
                    status = true;
                    return Json(new { data = d, status = status, msg = "" });
                }
                else
                {
                    return Json(new { data = "", status = status, msg = "Data Tidak Dapat Di Proses" });
                }

            }
            catch (Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { data = "", status = status, msg = "Data Tidak Dapat Di Proses" });
            }
        }
        [HttpPost]
        public async Task<ActionResult> GetAtasanDH(string add_npk)
        {
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            var status = false;
            try
            {
                var employee = await _tMSRepository.GetAtasanDH(add_npk);
                if (employee.Count > 0)
                {
                    status = true;
                    return Json(new { emp = employee, status = status, msg = "" });
                }
                else
                {
                    return Json(new { data = "", status = status, msg = "Data Tidak Dapat Di Proses" });
                }

            }
            catch (Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { data = "", status = status, msg = "Data Tidak Dapat Di Proses" });
            }
        }
        [HttpPost]
        public async Task<ActionResult> GetAtasanBasedOrganization(string add_npk)
        {
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            var status = false;
            try
            {
                var employee = await _tMSRepository.GetAtasanBasedOrganization(add_npk);
                if (employee.Count > 0)
                {
                    status = true;
                    return Json(new { emp = employee, status = status, msg = "" });
                }
                else
                {
                    return Json(new { data = "", status = status, msg = "Data Tidak Dapat Di Proses" });
                }

            }
            catch (Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { data = "", status = status, msg = "Data Tidak Dapat Di Proses" });
            }
        }
        [HttpPost]
        public async Task<ActionResult> GetPermissionType(long permit_id, string supem_date)
        {
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            var status = false;
            try
            {
                var dataPermissionType_ = await _tMSRepository.GetPermissionTypeByID(permit_id.ToString());
                var responseSupem = dataPermissionType_.FirstOrDefault();

                return Json(responseSupem);

            }
            catch (Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { data = "", status = status, msg = "Data Tidak Dapat Di Proses" });
            }
        }

        [HttpPost]
        public async Task<ActionResult> GetCICO(string npk, string supem_date)
        {
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            var status = false;
            try
            {
                var dataPermissionType_ = await _tMSRepository.GetCICO(npk, supem_date);
                var responseSupem = dataPermissionType_.FirstOrDefault();
                if (responseSupem != null)
                {
                    status = true;
                    return Json(new { data = responseSupem, status = status, msg = "" });
                }
                else
                {
                    return Json(new { data = "", status = status, msg = "Data Tidak Dapat Di Proses" });
                }

            }
            catch (Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { data = "", status = status, msg = "Data Tidak Dapat Di Proses" });
            }
        }
        [HttpPost]
        public async Task<ActionResult> GetListSupemType(string npk)
        {
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            var status = false;
            try
            {
                var employee = await _tMSRepository.GetListSupemType(npk);
                status = true;
                return Json(new { emp = employee, status = status, msg = "" });

            }
            catch (Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { data = "", status = status, msg = "Data Tidak Dapat Di Proses" });
            }
        }
        [HttpGet]
        public virtual ActionResult ExportToExcel(string fileGuid, string fileName)
        {
            if (TempData[fileGuid] != null)
            {
                byte[] data = TempData[fileGuid] as byte[];
                return File(data, "application/vnd.ms-excel", fileName);
            }
            else
            {
                return new EmptyResult();
            }
        }

        private string LoadMenu(string id_user)
        {
            return _menuService.RecursiveMenu(id_user);
        }


    }
    public class clsfilter
    {
        public string deptHandle { get; set; }
        public string locHandle { get; set; }
        public string pr { get; set; }
        public string yr { get; set; }
        public string npk { get; set; }
        public string nama { get; set; }
        public string emp_status { get; set; }
        public string status { get; set; }
        public string divisi { get; set; }
        public string dept { get; set; }
        public string golongan { get; set; }
        public string npk_input { get; set; }
        public string date_input { get; set; }
        public string costcenter { get; set; }
        public string shift { get; set; }
        public string type_spl { get; set; }
        public string UserId { get; set; }
        public string npkLogin { get; set; }
        public string tgl_mulai { get; set; }
        public string tgl_selesai { get; set; }

    }
    public class clsin
    {
        public string apv_atasan { get; set; }
        public string apv_spv { get; set; }
        public string date_end { get; set; }
        public string date_start { get; set; }
        public string info { get; set; }
        public string is_status_jam { get; set; }
        public string max_days { get; set; }
        public string npk { get; set; }
        public string npk_atasan { get; set; }
        public string permission_date { get; set; }
        public string permit_id { get; set; }
    }
    public class SUPEM
    {
        public long? permission_id { get; set; }
        public string permit_id { get; set; }
        public string ID_user { get; set; }
        public string ID_lokasi { get; set; }
        public DateTime? permission_date { get; set; }
        public DateTime? date_start { get; set; }
        public DateTime? date_end { get; set; }
        public string date_end_ { get; set; }
        public DateTime? last_update { get; set; }
        public string info { get; set; }
        public string npk { get; set; }
        public string emp_name { get; set; }
        public string emp_status { get; set; }
        public string stats { get; set; }
        public string statsNm { get; set; }
        public string reason { get; set; }
        public string npk_atasan { get; set; }
        public string name_atasan { get; set; }
        public string permission_dateText { get; set; }
        public string last_updateText { get; set; }
        public string tanggal_mulai { get; set; }
        public string tanggal_berakhir { get; set; }
        public string jam_masuk { get; set; }
        public string jam_keluar { get; set; }

        // Permit_Type
        public string permit_code_name { get; set; }
        public string permit_code { get; set; }
        public string permit_name { get; set; }
        public string cico_code { get; set; }
        public string status_gender { get; set; }
        public string ID_user_permit { get; set; }
        public string max_days { get; set; }
        public short? visible { get; set; }
        public short? pass_date { get; set; }
        public short? need_abs { get; set; }
        public short? status_jam { get; set; }
        public short? status_lembur { get; set; }
        public short? status_libur { get; set; }
        public short? exclude_SAP { get; set; }
        public DateTime? last_update_permit { get; set; }

        public string ID_division { get; set; }
        public string ID_dept { get; set; }
        public string ID_golongan { get; set; }
        public string div_name { get; set; }
        public string dept_name { get; set; }
        public string golongan_name { get; set; }
        public string npk_create { get; set; }

        public int? TotalData { get; set; }

        public string lastUpdateText { get; set; }
        public string overtimeDateText { get; set; }
        public string overtimeDateMonth { get; set; }
        public string overtimeDateYear { get; set; }
        public string date_input { get; set; }
        public string date_atasan { get; set; }
        public string date_hrd { get; set; }
        public bool? checkCICO { get; set; }
        public int? SeqNo { get; set; }
        public string cico_id2 { get; set; }
        public string status_atasan { get; set; }
        public string status_hrd { get; set; }
        public string npk_input { get; set; }
        public string npk_apv { get; set; }
        public string apv_spv { get; set; }
        public string apv_atasan { get; set; }
        public string npk_hrd { get; set; }
        public string costcenter { get; set; }
        public string leave_id { get; set; }
        public long? id_shift { get; set; }
        public bool is_status_jam { get; set; }
        public string pr { get; set; }
        public string yr { get; set; }
        public string jam_mulai { get; set; }
        public string jam_akhir { get; set; }
        public string tanggal { get; set; }
    }
}