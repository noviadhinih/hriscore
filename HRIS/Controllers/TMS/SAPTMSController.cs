using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models;
using HRIS.Models.General;
using HRIS.Models.PM3;
using HRIS.Repositories;
using HRIS.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace HRIS.Controllers.TMS
{

    public class SAPTMSController : Controller
    {
        private readonly ILogger<SAPTMSController> _logger;

        private readonly WebBased _webBased;
        private readonly PM3Context _context;
        private readonly TrLogErrorRepository _trLogErrorRepository;
        private readonly MenuService _menuService;

        public SAPTMSController(ILogger<SAPTMSController> logger, IHttpContextAccessor httpContextAccessor, TrLogErrorRepository trLogErrorRepository, MenuService menuService)
        {
            _logger = logger;
            _webBased = new WebBased(httpContextAccessor);
            _trLogErrorRepository = trLogErrorRepository;
            _menuService = menuService;
        }

        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("username") == null)
            {
                return RedirectToAction("Default", "Home");
            }
            var id_user = _webBased.UserId;
            ViewBag.leftmenu = this.LoadMenu(id_user);
            return View();
        }
        public IActionResult Detail(string j,string pr,string y,string s)
        {
            if (HttpContext.Session.GetString("username") == null)
            {
                return RedirectToAction("Default", "Home");
            }
            var id_user = _webBased.UserId;
            ViewBag.leftmenu = this.LoadMenu(id_user);
            ViewBag.JenisTMS = j;
            ViewBag.bln = pr;
            ViewBag.thn = y;
            ViewBag.status = s;
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> loadGV([FromBody] clst cls_)
        {
            var status = false;
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            try
            {
                if (cls_.dlljenisTms == "CICO")
                {
                    var deptHandle = _webBased.DeptHandle;
                    var locHandle = _webBased.LocationHandle;
                    var param1 = new SqlParameter("@depthandle", deptHandle ?? "");
                    var param2 = new SqlParameter("@lokasihandle", locHandle ?? "");
                    var param3 = new SqlParameter("@divisionID", cls_.dllDivision ?? "");
                    var param4 = new SqlParameter("@status", cls_.ddlStatus ?? "");
                    var param5 = new SqlParameter("@bln", cls_.ddlBulan ?? "");
                    var param6 = new SqlParameter("@thn", cls_.dllTahun ?? "");
                    var dataAtt = _context.sp_XMLCICO_getDataCount.FromSqlRaw("Exec sp_XMLCICO_getDataCount @depthandle,@lokasihandle,@divisionID,@status,@bln,@thn"
                                    , param1, param2, param3, param4, param5, param6).ToList().OrderBy(s => s.status_xml);
                    status = true;
                    return Json(new { status = status, dataAtt = dataAtt });
                }
                else if (cls_.dlljenisTms == "SUPEM")
                {
                    var deptHandle = _webBased.DeptHandle;
                    var locHandle = _webBased.LocationHandle;
                    var param1 = new SqlParameter("@depthandle", deptHandle ?? "");
                    var param2 = new SqlParameter("@lokasihandle", locHandle ?? "");
                    var param3 = new SqlParameter("@divisionID", cls_.dllDivision ?? "");
                    var param4 = new SqlParameter("@status", cls_.ddlStatus ?? "");
                    var param5 = new SqlParameter("@bln", cls_.ddlBulan ?? "");
                    var param6 = new SqlParameter("@thn", cls_.dllTahun ?? "");
                    var dataAtt = _context.sp_XMLCICO_getDataCount.FromSqlRaw("Exec sp_XMLSUPEM_getDataCount @depthandle,@lokasihandle,@divisionID,@status,@bln,@thn"
                                    , param1, param2, param3, param4, param5, param6).ToList().OrderBy(s => s.status_xml);
                    status = true;
                    return Json(new { status = status, dataAtt = dataAtt });
                }
                else if (cls_.dlljenisTms == "SPL")
                {
                    var deptHandle = _webBased.DeptHandle;
                    var locHandle = _webBased.LocationHandle;
                    var param1 = new SqlParameter("@depthandle", deptHandle ?? "");
                    var param2 = new SqlParameter("@lokasihandle", locHandle ?? "");
                    var param3 = new SqlParameter("@divisionID", cls_.dllDivision ?? "");
                    var param4 = new SqlParameter("@status", cls_.ddlStatus ?? "");
                    var param5 = new SqlParameter("@bln", cls_.ddlBulan ?? "");
                    var param6 = new SqlParameter("@thn", cls_.dllTahun ?? "");
                    var dataAtt = _context.sp_XMLCICO_getDataCount.FromSqlRaw("Exec sp_XMLOVERTIME_getDataCount @depthandle,@lokasihandle,@divisionID,@status,@bln,@thn"
                                    , param1, param2, param3, param4, param5, param6).ToList().OrderBy(s => s.status_xml);
                    status = true;
                    return Json(new { status = status, dataAtt = dataAtt });
                }
                else
                    {
                        return Json(new { status = status });
                    }
            }
            catch (System.Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { status = status });
            }
        }
        [HttpPost]
        public async Task<ActionResult> SendXML([FromBody] clst cls_)
        {
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            var status = false;
            try
            {
                var param1 = new SqlParameter("@cretedBy", _webBased.Npk ?? "");
                var param2 = new SqlParameter("@id_lokasi", _webBased.LocationId ?? "");
                var param3 = new SqlParameter("@bln", cls_.ddlBulan ?? "");
                var param4 = new SqlParameter("@thn", cls_.dllTahun ?? "");
                var param5 = new SqlParameter("@xml_type", cls_.dlljenisTms ?? "");

                var result2 = _context.Database.ExecuteSqlRawAsync(
                "EXEC sp_tx_req_xml_ALL @cretedBy,@id_lokasi,@bln,@thn,@xml_type",
                    param1,param2, param3, param4, param5

                );
                status = true;
                return Json(new { status = status, msg ="Data "+ cls_.dlljenisTms + " berhasil dikirimkan,Mohon Ditunggu prosesnya sedang Berjalan" });
            }
            catch (System.Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { status = status, msg = "Data Tidak Dapat Di Proses" });
            }
        }
         [HttpPost]
        public async Task<ActionResult> SendUnProses([FromBody] clst cls_)
        {
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            var status = false;
            try
            {
                var param1 = new SqlParameter("@id_lokasi", _webBased.LocationId ?? "");
                var param2 = new SqlParameter("@bln", cls_.ddlBulan ?? "");
                var param3 = new SqlParameter("@thn", cls_.dllTahun ?? "");
                var param4 = new SqlParameter("@xml_type", cls_.dlljenisTms ?? "");

                var result2 = _context.Database.ExecuteSqlRawAsync(
                "EXEC sp_TMS_Unproses @id_lokasi,@bln,@thn,@xml_type",
                    param1,param2, param3, param4

                );
                status = true;
                return Json(new { status = status, msg ="Data "+ cls_.dlljenisTms + " berhasil dikirimkan,Mohon Ditunggu prosesnya sedang Berjalan" });
            }
            catch (System.Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { status = status, msg = "Data Tidak Dapat Di Proses" });
            }
        }
        [HttpPost]
        public async Task<ActionResult> loadDetailCICO([FromBody] clst cls_)
        {
            var status = false;
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            try
            {
                var search = Request.Query["search[value]"].ToString();
                var start = Convert.ToInt32(Request.Query["start"]);
                var lenght = Convert.ToInt32(Request.Query["length"]);

                int pageSize = lenght != null ? Convert.ToInt32(lenght) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int totalRecords = 0;
               
                    var deptHandle = _webBased.DeptHandle;
                    var locHandle = _webBased.LocationHandle;
                   
                    var param1 = new SqlParameter("@depthandle", deptHandle ?? "");
                    var param2 = new SqlParameter("@lokasihandle", locHandle ?? "");
                    var param3 = new SqlParameter("@npk", cls_.npk ?? "");
                    var param4 = new SqlParameter("@start", cls_.ddlBulan ?? "");
                    var param5 = new SqlParameter("@end", cls_.dllTahun ?? "");
                    var param6 = new SqlParameter("@status", cls_.ddlStatus ?? "");
                    var param7 = new SqlParameter("@divisionID", cls_.dllDivision ?? "");
                 

                    var data = _context.sp_XMLCICO_getData.FromSqlRaw("Exec sp_XMLCICO_getData @depthandle,@lokasihandle,@npk,@start,@end,@status,@divisionID"
                                    , param1, param2, param3, param4, param5, param6, param7).ToList();
                    totalRecords = data.Count();

                    DatatableToJson datatableToJson = new DatatableToJson();
                    datatableToJson.recordsFiltered = totalRecords;
                    datatableToJson.recordsTotal = totalRecords;
                    datatableToJson.data = data;

                    return Json(datatableToJson);
                
            }
            catch (System.Exception ex)
            {
                 await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { status = status });
            }
        }
        [HttpPost]
        public async Task<ActionResult> loadDetailSupem([FromBody] clst cls_)
        {
            var status = false;
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            try
            {
                var search = Request.Query["search[value]"].ToString();
                var start = Convert.ToInt32(Request.Query["start"]);
                var lenght = Convert.ToInt32(Request.Query["length"]);

                int pageSize = lenght != null ? Convert.ToInt32(lenght) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int totalRecords = 0;
                
                    var deptHandle = _webBased.DeptHandle;
                    var locHandle = _webBased.LocationHandle;
                   
                    var param1 = new SqlParameter("@depthandle", deptHandle ?? "");
                    var param2 = new SqlParameter("@lokasihandle", locHandle ?? "");
                    var param3 = new SqlParameter("@npk", cls_.npk ?? "");
                    var param4 = new SqlParameter("@status", cls_.ddlStatus ?? "");
                    var param5 = new SqlParameter("@divisionID", cls_.dllDivision ?? "");
                    var param6 = new SqlParameter("@permission_date_start", cls_.ddlBulan?? "");
                    var param7 = new SqlParameter("@permission_date_end", cls_.dllTahun?? "");
                 

                    var data = _context.sp_XMLSUPEM_getData.FromSqlRaw("Exec sp_XMLSUPEM_getData @depthandle,@lokasihandle,@npk,@status,@divisionID,@permission_date_start,@permission_date_end"
                                    , param1, param2, param3, param4, param5, param6, param7).ToList();
                    totalRecords = data.Count();

                    DatatableToJson datatableToJson = new DatatableToJson();
                    datatableToJson.recordsFiltered = totalRecords;
                    datatableToJson.recordsTotal = totalRecords;
                    datatableToJson.data = data;

                    return Json(datatableToJson);
                
            }
            catch (System.Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { status = status });
            }
        }
        [HttpPost]
        public async Task<ActionResult> loadDetailSpl([FromBody] clst cls_)
        {
            var status = false;
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            try
            {
                var search = Request.Query["search[value]"].ToString();
                var start = Convert.ToInt32(Request.Query["start"]);
                var lenght = Convert.ToInt32(Request.Query["length"]);

                int pageSize = lenght != null ? Convert.ToInt32(lenght) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int totalRecords = 0;

                    var deptHandle = _webBased.DeptHandle;
                    var locHandle = _webBased.LocationHandle;
                   
                    var param1 = new SqlParameter("@depthandle", deptHandle ?? "");
                    var param2 = new SqlParameter("@lokasihandle", locHandle ?? "");
                    var param3 = new SqlParameter("@npk", cls_.npk ?? "");
                    var param4 = new SqlParameter("@status", cls_.ddlStatus ?? "");
                    var param5 = new SqlParameter("@divisionID", cls_.dllDivision ?? "");
                    var param6 = new SqlParameter("@overtime_date_start", cls_.ddlBulan?? "");
                    var param7 = new SqlParameter("@overtime_date_end",  cls_.dllTahun??"");
                 

                    var data = _context.sp_XMLSPL_getData.FromSqlRaw("Exec sp_XMLSPL_getData @depthandle,@lokasihandle,@npk,@status,@divisionID,@overtime_date_start,@overtime_date_end"
                                    , param1, param2, param3, param4, param5, param6, param7).ToList();
                    totalRecords = data.Count();

                    DatatableToJson datatableToJson = new DatatableToJson();
                    datatableToJson.recordsFiltered = totalRecords;
                    datatableToJson.recordsTotal = totalRecords;
                    datatableToJson.data = data;

                    return Json(datatableToJson);
                
            }
            catch (System.Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { status = status });
            }
        }

        private string LoadMenu(string id_user)
        {
            return _menuService.RecursiveMenu(id_user);
        }

    }
    public class clst
    {
        public string dlljenisTms { get; set; }
        public string dllDivision { get; set; }
        public string ddlStatus { get; set; }
        public string ddlBulan { get; set; }
        public string dllTahun { get; set; }
        public string npk { get; set; }
    }
}