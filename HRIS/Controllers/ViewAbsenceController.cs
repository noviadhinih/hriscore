using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models;
using HRIS.Models.General;
using HRIS.Models.PM3;
using HRIS.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace HRIS.Controllers
{
    public class ViewAbsenceController : Controller
    {
        private readonly WebBased _webBased;
        private readonly ILogger<ViewAbsenceController> _logger;
        private readonly PM3Context _context;
        private readonly MenuService _menuService;

        public ViewAbsenceController(ILogger<ViewAbsenceController> logger, IHttpContextAccessor httpContextAccessor, PM3Context context, MenuService menuService)
        {
            _logger = logger;
            _webBased = new WebBased(httpContextAccessor);
            _context = context;
            _menuService = menuService;
        }


        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("username") == null)
            {
                return RedirectToAction("Default", "Home");
            }
            var id_user = _webBased.UserId;
            ViewBag.leftmenu = this.LoadMenu(id_user);
            var npk = _webBased.Npk;
            ViewBag.npk = npk;
            ViewBag.nama = _webBased.Name;
            return View();
        }
        [HttpPost]
        public ActionResult getDataLihat(string npk, int? bln, int? thn)
        {
            var status = false;
            try
            {
                var param1 = new SqlParameter("@npk", npk);
                var param2 = new SqlParameter("@month", bln);
                var param3 = new SqlParameter("@year", thn);
                var dataAtt = _context.Sp_Attendance_GetViewAbsenceForEmployeeNew.FromSqlRaw("Exec Sp_Attendance_GetViewAbsenceForEmployeeNew @npk,@month,@year"
                    , param1, param2, param3).ToList();

                var dataOT = _context.sp_Overtime_getdataDetailAbesensi2.FromSqlRaw("Exec sp_Overtime_getdataDetailAbesensi2 @npk,@month,@year"
                    , param1, param2, param3).ToList();
                status = true;
                return Json(new { status = status, dataAtt = dataAtt, dataOT = dataOT });
            }
            catch (System.Exception ex)
            {
                return Json(new { status = status });

            }
        }
        private string LoadMenu(string id_user)
        {
            return _menuService.RecursiveMenu(id_user);
        }

    }
}