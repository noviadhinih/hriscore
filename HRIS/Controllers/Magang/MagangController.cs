﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Runtime.ConstrainedExecution;
using System.Threading.Tasks;
using ClosedXML.Excel;
using DocumentFormat.OpenXml.Spreadsheet;
using HRIS.Controllers.TMS;
using HRIS.Interfaces;
using HRIS.Interfaces.TMS;
using HRIS.Models;
using HRIS.Models.General;
using HRIS.Models.PM3;
using HRIS.Repositories;
using HRIS.Repositories.TMS;
using HRIS.Services;
using Irony.Parsing;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;

namespace HRIS.Controllers.Magang
{
    public class MagangController : Controller
    {
        private readonly ILogger<MagangController> _logger;
        private readonly WebBased _webBased;
        private readonly TrLogErrorRepository _trLogErrorRepository;
        private readonly MenuService _menuService;
        private readonly IMagangRepository _magangRepository;


        public MagangController(ILogger<MagangController> logger, IHttpContextAccessor httpContextAccessor, TrLogErrorRepository trLogErrorRepository, MenuService menuService, IMagangRepository magangRepository)
        {
            _logger = logger;
            _webBased = new WebBased(httpContextAccessor);
            _trLogErrorRepository = trLogErrorRepository;
            _menuService = menuService;
            _magangRepository = magangRepository;
        }
        #region OT
        public IActionResult addOT()
        {
            if (HttpContext.Session.GetString("username") == null)
            {
                return RedirectToAction("Default", "Home");
            }
            var id_user = _webBased.UserId;
            ViewBag.leftmenu = this.LoadMenu(id_user);
            ViewBag.npk = _webBased.Npk;
            return View();
        }
        #endregion
        #region attendace
        public IActionResult attendace()
        {
            if (HttpContext.Session.GetString("username") == null)
            {
                return RedirectToAction("Default", "Home");
            }
            var id_user = _webBased.UserId;
            ViewBag.leftmenu = this.LoadMenu(id_user);
            ViewBag.npk = _webBased.Npk;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> GetAttendance([FromBody] clsfil f)
        {
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            var status = false;
            try
            {
                string iStrReturn = string.Empty;
                var data = await _magangRepository.GetDataAttendance(f);
                var dt = data.OrderBy(s => s.tglKerja).ToList();
                iStrReturn += "<table class=\"table fht-table table-freeze-multi table-bordered table-striped table-hover js-basic-example table-freeze-multi-clone\">";
                iStrReturn += "<thead>";
                iStrReturn += "<tr>";
                iStrReturn += "<th class=\"column\" align=\"center\" rowspan=\"3\" style=\"width:36px;\">No</th>";
                iStrReturn += "<th class=\"column\" align=\"center\" rowspan=\"3\" style=\"width:60px;\">NPK</th>";
                iStrReturn += "<th class=\"column\" align=\"center\" rowspan=\"3\" style=\"width:180px;\">Nama</th>";
                iStrReturn += "<th class=\"column\" align=\"center\" rowspan=\"3\" style=\"width:160px;\">Division</th>";
                iStrReturn += "<th class=\"column\" align=\"center\" rowspan=\"3\" style=\"width:160px;\">Department</th>";
                iStrReturn += "<th class=\"column\" align=\"center\" rowspan=\"3\" style=\"width:80px;\">Shift</th>";
                var tglList = dt.GroupBy(s => new {s.tglKerja,s.tglDesc,s.dayName_}).Select(t => new {tglKerja=t.Key.tglKerja,tglDesc=t.Key.tglDesc, dayName_ =t.Key.dayName_ }).OrderBy(s=>s.tglKerja).ToList();
                for (var i = 0; i < tglList.Count(); i++)
                {
                    var item = tglList[i];
                    var tglKerja = item.tglKerja;
                    var tglDesc = item.tglDesc;
                    iStrReturn += $"<th align=\"center\" colspan=\"4\" style=\"width:320px;text-align:center;\">{tglDesc}</th>";
                    iStrReturn += "<th class=\"column2 colgate\" align=\"center\" rowspan=\"3\" style=\"width:130px;\">KET</th>";
                }
                iStrReturn += "</tr>";
                iStrReturn += "<tr>";
                for (var i = 0; i < tglList.Count(); i++)
                {
                    var item = tglList[i];
                    var dayName_ = item.dayName_;
                    iStrReturn += $"<th align=\"center\" colspan=\"4\" style=\"text-align:center;\">{dayName_}</th>";
                   
                }
                iStrReturn += "</tr>";
                iStrReturn += "<tr>";
                for (var i = 0; i < tglList.Count(); i++)
                {
                    iStrReturn += "<th class=\"column2 colfree\" align=\"center\" style=\"width:80px;text-align:center;\">In</th>";
                    iStrReturn += "<th class=\"column2 colfree\" align=\"center\" style=\"width:80px;text-align:center;\">Out</th>";
                    iStrReturn += "<th class=\"column2 colfree\" align=\"center\" style=\"width:80px;text-align:center;\">OT (In)</th>";
                    iStrReturn += "<th class=\"column2 colfree\" align=\"center\" style=\"width:80px;text-align:center;\">OT (Out)</th>";
                }
                iStrReturn += "</tr>";

                iStrReturn += "</thead>";
                iStrReturn += "<tbody>";
                var no = 1;
                var employees = dt.GroupBy(s => new
                {
                    s.npk,
                    s.empNm,
                    s.divNm,
                    s.deptNm,
                    s.shift

                }).Select(s => new
                {
                    npk = s.Key.npk,
                    empNm = s.Key.empNm,
                    divNm = s.Key.divNm,
                    deptNm = s.Key.deptNm,
                    shift = s.Key.shift
                }).ToList();
                var emps = employees;
                foreach (var emp in emps)
                {
                    iStrReturn += "<tr>";
                    iStrReturn += $"<td style=\"width: 36px;\">{no}.</td>";
                    iStrReturn += $"<td style=\"width: 60px;\"><a href=\"../../Pages/Attendance/ViewAbsence.aspx?npk=2586&amp;mon=11&amp;year=2024\">{emp.npk}</a></td>";
                    iStrReturn += $"<td style=\"width: 180px;\">{emp.empNm}</td>";
                    iStrReturn += $"<td style=\"width: 160px;\">{emp.divNm}</td>";
                    iStrReturn += $"<td style=\"width: 160px;\">{emp.deptNm}</td>";
                    iStrReturn += $"<td style=\"width: 160px;\">{emp.shift}</td>";
                    var npk = emp.npk;
                    var lcico = dt.Where(s => s.npk == npk).GroupBy(s => new
                    {
                        s.npk,
                        s.shift,
                        s.CICO_ID,
                        s.clock_in,
                        s.clock_out,
                        s.CI_date,
                        s.CO_date,
                        s.overtime_in,
                        s.overtime_out,
                        s.stats,
                        s.tglKerja
                    }).Select(s => new
                    {
                        npk = s.Key.npk,
                        shift = s.Key.shift,
                        CICO_ID = s.Key.CICO_ID,
                        clock_in = s.Key.clock_in,
                        clock_out = s.Key.clock_out,
                        CI_date = s.Key.CI_date,
                        CO_date = s.Key.CO_date,
                        overtime_in = s.Key.overtime_in,
                        overtime_out = s.Key.overtime_out,
                        stats = s.Key.stats,
                        tglKerja = s.Key.tglKerja
                    }).OrderBy(s => s.clock_in).ToList();
                    foreach (var cic in lcico)
                    {
                        var In = "-";
                        var Out = "-";
                        if (cic.CI_date != null) 
                        {
                            In = (cic.CI_date != null) ? (cic.CI_date.Value.ToString("HH:mm") == "00:00" ? "-" : cic.CI_date.Value.ToString("HH:mm")) : "-";
                        }
                        if (cic.CO_date != null)
                        {
                            Out = (cic.CO_date != null) ? (cic.CO_date.Value.ToString("HH:mm") == "00:00" ? "-" : cic.CO_date.Value.ToString("HH:mm")) : "-";
                        }
                        iStrReturn += $"<td style=\"width: 80px;\">{In}</td>";
                        iStrReturn += $"<td style=\"width: 80px;\">{Out}</td>";

                        string listIn = "";
                        string listOut = "";
                        var overtimes = data.Where(s => s.npk == npk && s.CICO_ID == cic.CICO_ID && s.tglKerja==cic.tglKerja).GroupBy(s => new
                        {
                            s.overtime_in,
                            s.overtime_out
                        }).Select(s => new
                        {
                            date_start = s.Key.overtime_in,
                            date_end = s.Key.overtime_out
                        }).ToList();
                        if (overtimes.Count() > 0)
                        {
                             listIn = "<ul style='padding-left:15px'>";
                             listOut = "<ul style='padding-left:15px'>";
                            foreach (var o in overtimes)
                            {
                                if (o.date_start != null)
                                {
                                    listIn += "<li>" + ((o.date_start.Value.ToString("HH:mm") == "00:00") ? "-" : o.date_start.Value.ToString("HH:mm")) + "</li>";
                                }
                                else
                                {
                                    listIn += "<li>-</li>";
                                }
                                if (o.date_end != null)
                                {
                                    listOut += "<li>" + ((o.date_end.Value.ToString("HH:mm") == "00:00") ? "-" : o.date_end.Value.ToString("HH:mm")) + "</li>";

                                }
                                else
                                {
                                    listOut += "<li>-</li>";
                                }
                            }

                            listIn += "</ul>";
                            listOut += "</ul>";
                        }
                        else
                        {
                            listIn = "-";
                            listOut = "-";
                        }
                        iStrReturn += $"<td style=\"width: 80px;\">{listIn}</td>";
                        iStrReturn += $"<td style=\"width: 80px;\">{listOut}</td>";
                        iStrReturn += $"<td style=\"width: 80px;\">{cic.stats}</td>";

                    }


                    iStrReturn += "</tr>";
                    no++;
                }
                
                iStrReturn += "</tbody>";
                iStrReturn += "</table>";

                return Json(new { status = status, msg = "Success" ,dt= iStrReturn });
            }
            catch (Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { data = "", status = status, msg = "Data Tidak Dapat Di Proses" });
            }
        }
        #endregion
        #region Hiring
        public IActionResult Hiring()
        {
            if (HttpContext.Session.GetString("username") == null)
            {
                return RedirectToAction("Default", "Home");
            }
            var id_user = _webBased.UserId;
            ViewBag.leftmenu = this.LoadMenu(id_user);
            ViewBag.npk = _webBased.Npk;
            return View();
        }
        public IActionResult UHiring()
        {
            if (HttpContext.Session.GetString("username") == null)
            {
                return RedirectToAction("Default", "Home");
            }
            var id_user = _webBased.UserId;
            ViewBag.leftmenu = this.LoadMenu(id_user);
            ViewBag.npk = _webBased.Npk;
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> loadgetHiring([FromBody] clsfil f)
        {
            var status = false;
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            try
            {
                var search = Request.Query["search[value]"].ToString();
                var start = Convert.ToInt32(Request.Query["start"]);
                var lenght = Convert.ToInt32(Request.Query["length"]);

                int pageSize = lenght != null ? Convert.ToInt32(lenght) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int totalRecords = 0;

                f.depthandle = _webBased.DeptHandle;
                f.lokasihandle = _webBased.LocationHandle;

                var data = await _magangRepository.GetDataHiring(f);
                totalRecords = data.Count();

                DatatableToJson datatableToJson = new DatatableToJson();
                datatableToJson.recordsFiltered = totalRecords;
                datatableToJson.recordsTotal = totalRecords;
                datatableToJson.data = data;

                return Json(datatableToJson);

            }
            catch (System.Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { status = status });
            }
        }
        [HttpPost]
        public async Task<IActionResult> UploadHiring([FromBody] List<uHiring> clsapv_)
        {
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            var status = false;
            try
            {
                var check = 0;
                foreach (var item in clsapv_)
                {
                    item.npkLogin = _webBased.Npk;
                    var valid = await _magangRepository.UploadHiring(item);
                    if(valid.status==true)
                    {
                        check++;
                    }
                }
                status = true;
                return Json(new { status = status, msg = "Berhasil Submit Hiring Pemagangan" });
            }
            catch (Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { data = "", status = status, msg = "Data Tidak Dapat Di Proses" });
            }
        }
        [HttpPost]
        public async Task<IActionResult> ValidateHiring([FromForm] IFormFile f)
        {
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            var status = false;
            try
            {
                if (f != null && f.Length > 0 && !string.IsNullOrEmpty(f.FileName))
                {
                    string fileName = f.FileName;
                    ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
                    using (var stream = new MemoryStream())
                    {
                        f.CopyTo(stream);
                        using (var package = new ExcelPackage(stream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();
                            var noOfCol = workSheet.Dimension.End.Column;
                            var noOfRow = workSheet.Dimension.End.Row;
                            List<uHiring> dtvalid = new List<uHiring>();
                            List<uHiring> dts = new List<uHiring>();
                            int no = 1;
                            int no_ = 1;

                            for (int rowIterator = 4; rowIterator <= noOfRow; rowIterator++)
                            {
                                var npk = (workSheet.Cells[rowIterator, 1].Value ?? "").ToString();
                                var nama = (workSheet.Cells[rowIterator, 2].Value ?? "").ToString();
                                var dir = (workSheet.Cells[rowIterator, 3].Value ?? "").ToString();
                                var div = (workSheet.Cells[rowIterator, 4].Value ?? "").ToString();
                                var dept = (workSheet.Cells[rowIterator, 5].Value ?? "").ToString();
                                var sec = (workSheet.Cells[rowIterator, 6].Value ?? "").ToString();
                                var sub = (workSheet.Cells[rowIterator, 7].Value ?? "").ToString();
                                var lok = (workSheet.Cells[rowIterator, 8].Value ?? "").ToString();
                                var start = (workSheet.Cells[rowIterator, 9].Value ?? "").ToString();
                                var end = (workSheet.Cells[rowIterator, 10].Value ?? "").ToString();
                                var gender = (workSheet.Cells[rowIterator, 11].Value ?? "").ToString();
                                var email = (workSheet.Cells[rowIterator, 12].Value ?? "").ToString();
                                clsfil clsfil = new clsfil();
                                clsfil.npk = npk;
                                clsfil.name = nama;
                                clsfil.dir = dir;
                                clsfil.id_div = div;
                                clsfil.id_dept = dept;
                                clsfil.section = sec;
                                clsfil.subsec = sub;
                                clsfil.id_lok = lok;
                                clsfil.start_ = start;
                                clsfil.end_ = end;
                                clsfil.email = email;
                                clsfil.gender = gender;
                                if (!string.IsNullOrEmpty(npk) && !string.IsNullOrEmpty(nama) &&
                                  !string.IsNullOrEmpty(dept) &&
                                  !string.IsNullOrEmpty(lok) && !string.IsNullOrEmpty(start) &&
                                  !string.IsNullOrEmpty(end) && !string.IsNullOrEmpty(gender) &&
                                  !string.IsNullOrEmpty(email)
                                 )
                                {
                                    
                                   var valid = await _magangRepository.ValidasiUHiring(clsfil);
                                   var cek = valid.FirstOrDefault();
                                   if (cek != null)
                                   {
                                        if (cek.msg == "-")
                                        {
                                            uHiring i = new uHiring();
                                            i.No = no.ToString();
                                            i.NPK= npk;
                                            i.NAMA = nama;
                                            i.DIRECTORATE = dir;
                                            i.DIVISI = div;
                                            i.DEPARTEMENT= dept;
                                            i.SECTION = sec;
                                            i.SUB = sub;
                                            i.LOKASI = lok;
                                            i.START = start;
                                            i.END = end;
                                            i.GENDER = gender;
                                            i.EMAIL = email;
                                            i.id_dir = cek.directorateID;
                                            i.id_div = cek.ID_division;
                                            i.id_dept = cek.ID_dept;
                                            i.id_sec = cek.section_id;
                                            i.id_sub = cek.sub_section_id;
                                            i.id_lok = cek.id_lokasi;
                                            dtvalid.Add(i);

                                            no++;
                                        }
                                        else
                                        {
                                            uHiring i = new uHiring();
                                            i.No = no_.ToString();
                                            i.NPK = npk;
                                            i.NAMA = nama;
                                            i.DIRECTORATE = dir;
                                            i.DIVISI = div;
                                            i.DEPARTEMENT = dept;
                                            i.SECTION = sec;
                                            i.SUB = sub;
                                            i.LOKASI = lok;
                                            i.START = start;
                                            i.END = end;
                                            i.GENDER = gender;
                                            i.EMAIL = email;
                                            i.NOTE = cek.msg;
                                            dts.Add(i);

                                            no_++;
                                        }
                                   }
                                   else
                                   {
                                        uHiring i = new uHiring();
                                        i.No = no_.ToString();
                                        i.NPK = npk;
                                        i.NAMA = nama;
                                        i.DIRECTORATE = dir;
                                        i.DIVISI = div;
                                        i.DEPARTEMENT = dept;
                                        i.SECTION = sec;
                                        i.SUB = sub;
                                        i.LOKASI = lok;
                                        i.START = start;
                                        i.END = end;
                                        i.GENDER = gender;
                                        i.EMAIL = email;
                                        i.NOTE = cek.msg;
                                        dts.Add(i);

                                        no_++;
                                    }

                                }
                                else
                                {
                                    uHiring i = new uHiring();
                                    i.No = no_.ToString();
                                    i.NPK = npk;
                                    i.NAMA = nama;
                                    i.DIRECTORATE = dir;
                                    i.DIVISI = div;
                                    i.DEPARTEMENT = dept;
                                    i.SECTION = sec;
                                    i.SUB = sub;
                                    i.LOKASI = lok;
                                    i.START = start;
                                    i.END = end;
                                    i.GENDER = gender;
                                    i.EMAIL = email;
                                    i.NOTE = "Ada Data yang Masih Kosong";
                                    dts.Add(i);

                                    no_++;
                                }
                            }
                            if (dts.Count() >0)
                            {
                                status = true;
                                return Json(new { data = dts, status = status, msg = "Data Tidak Valid", is_valid = 0 });
                            }
                            else
                            {
                                status = true;
                                return Json(new { data = dtvalid, status = status, msg = "Valid", is_valid = 1 });
                            }
                        }
                    }


                }
                else
                {
                    return Json(new { data = "", status = status, msg = "Data Tidak Dapat Di Proses" });
                }
            }
            catch (Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { data = "", status = status, msg = "Data Tidak Dapat Di Proses" });
            }
        }
        #endregion

        #region WorkSchedule
        public IActionResult WorkSchedule()
        {
            if (HttpContext.Session.GetString("username") == null)
            {
                return RedirectToAction("Default", "Home");
            }
            var id_user = _webBased.UserId;
            ViewBag.leftmenu = this.LoadMenu(id_user);
            ViewBag.npk = _webBased.Npk;
            return View();
        }
        public IActionResult UWorkSchedule()
        {
            if (HttpContext.Session.GetString("username") == null)
            {
                return RedirectToAction("Default", "Home");
            }
            var id_user = _webBased.UserId;
            ViewBag.leftmenu = this.LoadMenu(id_user);
            ViewBag.npk = _webBased.Npk;
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> loadgetWorkSch([FromBody] clsWorkSch f)
        {
            var status = false;
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            try
            {
                var search = Request.Query["search[value]"].ToString();
                var start = Convert.ToInt32(Request.Query["start"]);
                var lenght = Convert.ToInt32(Request.Query["length"]);

                int pageSize = lenght != null ? Convert.ToInt32(lenght) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int totalRecords = 0;

                f.npkLogin = _webBased.Npk;
                f.id_user = _webBased.UserId;

                var data = await _magangRepository.GetDataWorkSch(f);
                totalRecords = data.Count();

                DatatableToJson datatableToJson = new DatatableToJson();
                datatableToJson.recordsFiltered = totalRecords;
                datatableToJson.recordsTotal = totalRecords;
                datatableToJson.data = data;

                return Json(datatableToJson);

            }
            catch (System.Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { status = status });
            }
        }
        [HttpPost]
        public async Task<IActionResult> UploadSch([FromBody] List<uWorkSch> clsapv_)
        {
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            var status = false;
            try
            {
                var check = 0;
                foreach (var item in clsapv_)
                {
                   
                    var valid = await _magangRepository.UploadSch(item);
                    if (valid.status == true)
                    {
                        check++;
                    }
                }
                status = true;
                return Json(new { status = status, msg = "Berhasil Submit Hiring Pemagangan" });
            }
            catch (Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { data = "", status = status, msg = "Data Tidak Dapat Di Proses" });
            }
        }
        [HttpPost]
        public async Task<IActionResult> ValidateWorkSch([FromForm] IFormFile f)
        {
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            var status = false;
            try
            {
                if (f != null && f.Length > 0 && !string.IsNullOrEmpty(f.FileName))
                {
                    string fileName = f.FileName;
                    ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
                    using (var stream = new MemoryStream())
                    {
                        f.CopyTo(stream);
                        using (var package = new ExcelPackage(stream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();
                            var noOfCol = workSheet.Dimension.End.Column;
                            var noOfRow = workSheet.Dimension.End.Row;
                            List<uWorkSch> dtvalid = new List<uWorkSch>();
                            List<uWorkSch> dts = new List<uWorkSch>();
                            int no = 1;
                            int no_ = 1;


                            for (int rowIterator = 4; rowIterator <= noOfRow; rowIterator++)
                            {
                                var npk = (workSheet.Cells[rowIterator, 1].Value ?? "").ToString();
                                var tglKerja = (workSheet.Cells[rowIterator, 2].Value ?? "").ToString();
                                var tglKerjaEnd = (workSheet.Cells[rowIterator, 3].Value ?? "").ToString();
                                var shift = (workSheet.Cells[rowIterator, 4].Value ?? "").ToString();
                                var msg = "";
                                bool ReturnError = false;
                                DateTime parsedDate;

                                if (!string.IsNullOrEmpty(npk) && !string.IsNullOrEmpty(tglKerja) &&
                                  !string.IsNullOrEmpty(tglKerjaEnd) &&
                                  !string.IsNullOrEmpty(shift) 
                                 )
                                {

                                    if (!DateTime.TryParse(tglKerja, out parsedDate))
                                    {
                                        msg = "Tanggal tidak valid";
                                        ReturnError = true;
                                    }

                                }
                                if (ReturnError == true)
                                {
                                    uWorkSch i= new uWorkSch();
                                    i.no = no.ToString();
                                    i.npk = npk;
                                    i.tglKerjaEnd = tglKerjaEnd;
                                    i.shift = shift;
                                    i.tglKerja=tglKerja;
                                    dts.Add(i);
                                    no++;
                                }
                                else
                                {
                                    uWorkSch i = new uWorkSch();
                                    i.no = no_.ToString();
                                    i.npk = npk;
                                    i.tglKerjaEnd = tglKerjaEnd;
                                    i.shift = shift;
                                    i.tglKerja = tglKerja;
                                    dtvalid.Add(i);
                                    no_++;
                                }


                            }
                            if (dts.Count() > 0)
                            {
                                status = true;
                                return Json(new { data = dts, status = status, msg = "Data Tidak Valid", is_valid = 0 });
                            }
                            else
                            {
                                status = true;
                                return Json(new { data = dtvalid, status = status, msg = "Valid", is_valid = 1 });
                            }
                        }
                    }


                }
                else
                {
                    return Json(new { data = "", status = status, msg = "Data Tidak Dapat Di Proses" });
                }
            }
            catch (Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { data = "", status = status, msg = "Data Tidak Dapat Di Proses" });
            }
        }
        #endregion

        private string LoadMenu(string id_user)
        {
            return _menuService.RecursiveMenu(id_user);
        }


    }
    public class uHiring
    {
        public string? No { get; set; }
        public string? NPK { get; set; }
        public string? NAMA { get; set; }
        public string? DIRECTORATE { get; set; }
        public string? DIVISI { get; set; }
        public string? DEPARTEMENT { get; set; }
        public string? SECTION { get; set; }
        public string? SUB { get; set; }
        public string? LOKASI { get; set; }
        public string? START { get; set; }
        public string? END { get; set; }
        public string? GENDER { get; set; }
        public string? EMAIL { get; set; }
        public string? NOTE { get; set; }
        public string? id_dir { get; set; }
        public string? id_div { get; set; }
        public string? id_dept { get; set; }
        public string? id_sec { get; set; }
        public string? id_sub { get; set; }
        public string? id_lok { get; set; }
        public string? npkLogin { get; set; }
    }
    public class uWorkSch
    {
        public string? npk { get; set; }
        public string? tglKerja { get; set; }
        public string? tglKerjaEnd { get; set; }
        public string? shift { get; set; }
        public string? no { get; set; }

    }
}
