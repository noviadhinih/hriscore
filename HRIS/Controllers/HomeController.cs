using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using HRIS.Models;
using HRIS.Models.PM3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.DataProtection;
using HRIS.Models.General;
using HRIS.Repositories;
using HRIS.Services;
using HRIS.Interfaces;
using OtpNet;
using System;
using QRCoder;
using System.Drawing;
using System.IO;



namespace HRIS.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    private readonly IDataProtectionProvider _dataProtectionProvider;
    private readonly WebBased _webBased;
    private readonly PM3Context _context;
    private readonly IUsersRepository _usersRepository;
    private readonly MenuService _menuService;

    public HomeController(ILogger<HomeController> logger, IDataProtectionProvider dataProtectionProvider, IHttpContextAccessor httpContextAccessor, PM3Context context, IUsersRepository usersRepository, MenuService menuService)
    {
        _logger = logger;
        _dataProtectionProvider = dataProtectionProvider;
        _webBased = new WebBased(httpContextAccessor);
        _context = context;
        _usersRepository = usersRepository;
        _menuService = menuService;
    }

    public IActionResult Index()
    {
        if (HttpContext.Session.GetString("username") == null)
        {
            return RedirectToAction("Default");
        }
        var id_user = _webBased.UserId;
        ViewBag.leftmenu = this.LoadMenu(id_user);
        return View();
    }
    public IActionResult Default()
    {

        return View();
    }
    public ActionResult Login()
    {
        HttpContext.Session.SetString("Key", "");
        HttpContext.Session.SetString("IsValidTwoFactorAuthentication", "");

        return View();
    }
    [HttpPost]
    public async Task<ActionResult> LoginPos(LoginModel login)
    {
        var msg = "Username dan Password Tidak Valid";
        var token = "";
        var status = false;
        int? is_mfa = 0;
        int? mfa_registrasi = 0;
        token = Guid.NewGuid().ToString();
        var BarcodeImageUrl = "";
        var SetupCode = "";
        var register = "";
        var urlPath = "";

        try
        {
            if (string.IsNullOrWhiteSpace(login.UserName))
            {
                msg = "Please fill username first.";
            }
            else if (string.IsNullOrWhiteSpace(login.Password))
            {
                msg = "Please fill password first.";
            }
            else
            {
                login.Password = "30uaxb5Tw7t4YWuexAru+Q==";
                var user = (await _usersRepository.GetUserDataAsync(login.UserName, login.Password, token)).FirstOrDefault();
                if (user != null)
                {
                    is_mfa = user.is_mfa;
                    msg = user.msg;
                    mfa_registrasi = user.mfa_registrasi;
                    if (user.Uactive == 1 && user.Eactive == 1)
                    {
                        //user.is_mfa = 1;
                        //user.mfa_registrasi = 0;

                        if (user.is_mfa == 1 && user.mfa_registrasi == 0)
                        {
                            var key = KeyGeneration.GenerateRandomKey(20); // 20 bytes
                            var otp = new Totp(key);
                            string base32Secret = OtpNet.Base32Encoding.ToString(key);
                            string accountName = login.UserName; // Replace with user's account name
                            string issuer = "HRIS"; // Replace with your app's name
                            string uri = $"otpauth://totp/{issuer}:{accountName}?secret={base32Secret}&issuer={issuer}";

                            var qrGenerator = new QRCodeGenerator();
                            var qrCodeData = qrGenerator.CreateQrCode(uri, QRCodeGenerator.ECCLevel.Q);

                            using (Bitmap qrBitmap = new Bitmap(qrCodeData.ModuleMatrix.Count, qrCodeData.ModuleMatrix.Count))
                            using (Graphics graphics = Graphics.FromImage(qrBitmap))
                            {
                                graphics.Clear(Color.White);  // Set background putih
                                var moduleSize = 10;  // Ukuran modul QR Code

                                // Iterasi modul data untuk menggambar setiap modul QR Code
                                for (int y = 0; y < qrCodeData.ModuleMatrix.Count; y++)
                                {
                                    for (int x = 0; x < qrCodeData.ModuleMatrix[y].Count; x++)
                                    {
                                        var module = qrCodeData.ModuleMatrix[y][x];
                                        var color = module ? Color.Black : Color.White;
                                        graphics.FillRectangle(new SolidBrush(color), x * moduleSize, y * moduleSize, moduleSize, moduleSize);
                                    }
                                }

                                // Simpan gambar ke dalam stream
                                using (var ms = new MemoryStream())
                                {
                                    qrBitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                                    string base64Image = Convert.ToBase64String(ms.ToArray());
                                    BarcodeImageUrl = base64Image;
                                    Console.WriteLine($"Base64 QR Code: {base64Image}");
                                }
                            }
                            SetupCode = base32Secret;
                            SetupCode= "JS542PXJQX5YBGGVKTWZSFAHTTQ6D6RK";
                            HttpContext.Session.SetString("OtpSecretKey", SetupCode);
                        
                            var insertUser = await _usersRepository.RegisterMFA(user.ID_user.ToString(), base32Secret);
                            if (insertUser.status == true)
                            {
                                SetSessionValues(user);
                                status = true;
                                register = "YA";
                                is_mfa = 1;
                            }
                            else
                            {
                                status = false;
                            }

                        }
                        else if (user.is_mfa == 1 && user.mfa_registrasi == 1)
                        {
                            SetSessionValues(user);
                            status = true;
                            is_mfa = 1;
                        }
                        else
                        {
                            SetSessionValues(user);
                            status = true;
                            urlPath = Url.Action("Index", "Home");
                            // return RedirectToAction("Privacy");
                        }
                    }
                    else
                    {
                        msg = "Your account is not active.";
                    }
                }
                else
                {
                    msg = "Username and password are invalid";
                }
            }

            return Json(new { status = status, msg = msg, is_mfa = is_mfa, mfa_registrasi = mfa_registrasi, register = register, BarcodeImageUrl = BarcodeImageUrl, SetupCode = SetupCode, urlPath = urlPath });
        }
        catch (Exception ex)
        {
            return Json(new { status = status, msg = msg, is_mfa, mfa_registrasi, register, BarcodeImageUrl, SetupCode });
        }
    }
    [HttpPost]
    public IActionResult Validate(LoginModel login)
    {
        var msg = "";
        var status = false;
        var ckey = HttpContext.Session.GetString("OtpSecretKey").ToString(); 
        //"JS542PXJQX5YBGGVKTWZSFAHTTQ6D6RK";
        byte[] secretKey = Base32Encoding.ToBytes(ckey);

        if (ckey == null)
        {
            return BadRequest("OTP secret key not found.");
        }

        var otp = new Totp(secretKey);
        bool isValid = otp.VerifyTotp(login.CodeDigit, out long timeStepMatched);

        if (isValid)
        {
            msg = "Token Valid";
            status = true;
            HttpContext.Session.SetString("IsValidTwoFactorAuthentication", "1");
            // return RedirectToAction("Index", "Home");
            return Json(new { status = status, msg = "Success", });
        }
        else
        {
            msg = "Token Tidak Valid";
            RemoveSessionValues();
            return Json(new { status = status, msg = msg });
        }
    }
    //[HttpPost]
    //public async Task<ActionResult> LoginPosOld(LoginModel login)
    //{
    //    var msg = "Username dan Password Tidak Valid";
    //    var token = "";
    //    var status = false;
    //    int? is_mfa = 0;
    //    int? mfa_registrasi = 0;
    //    token = Guid.NewGuid().ToString();
    //    var BarcodeImageUrl = "";
    //    var SetupCode = "";
    //    var register = "";
    //    var urlPath = "";

    //    try
    //    {
    //        if (string.IsNullOrWhiteSpace(login.UserName))
    //        {
    //            msg = "Please fill username first.";
    //        }
    //        else if (string.IsNullOrWhiteSpace(login.Password))
    //        {
    //            msg = "Please fill password first.";
    //        }
    //        else
    //        {
    //            login.Password = "30uaxb5Tw7t4YWuexAru+Q==";
    //            var user = (await _usersRepository.GetUserDataAsync(login.UserName, login.Password, token)).FirstOrDefault();
    //            if (user != null)
    //            {
    //                is_mfa = user.is_mfa;
    //                msg = user.msg;
    //                mfa_registrasi = user.mfa_registrasi;
    //                if (user.Uactive == 1 && user.Eactive == 1)
    //                {
    //                    user.is_mfa = 1;
    //                    user.mfa_registrasi = 0;

    //                    if (user.is_mfa == 1 && user.mfa_registrasi == 0)
    //                    {
    //                        string googleAuthKey = "P0W48S534";
    //                       // string googleAuthKey = "4DMJ4Y4b9tpUoLP0W48S534";
    //                        string UserUniqueKey = (login.UserName + googleAuthKey);
    //                        HttpContext.Session.SetString("username", login.UserName);
    //                        TwoFactorAuthenticator TwoFacAuth = new TwoFactorAuthenticator();
    //                        var setupInfo = TwoFacAuth.GenerateSetupCode("HRIS MFA OTP", login.UserName, ConvertSecretToBytes(UserUniqueKey, false), 300);
    //                        BarcodeImageUrl = setupInfo.QrCodeSetupImageUrl;
    //                        SetupCode = setupInfo.ManualEntryKey;
    //                        HttpContext.Session.SetString("UserUniqueKey", SetupCode);
    //                        var insertUser = await _usersRepository.RegisterMFA(user.ID_user.ToString(), SetupCode);
    //                        if(insertUser.status==true)
    //                        {
    //                            SetSessionValues(user);
    //                            status = true;
    //                            register = "YA";
    //                            is_mfa = 1;
    //                        }
    //                        else
    //                        {
    //                            status = false;
    //                        }

    //                    }
    //                    else if (user.is_mfa == 1 && user.mfa_registrasi == 1)
    //                    {
    //                        SetSessionValues(user);
    //                        status = true;
    //                        is_mfa = 1;
    //                    }
    //                    else
    //                    {
    //                        SetSessionValues(user);
    //                        status = true;
    //                        urlPath = Url.Action("Index", "Home");
    //                        // return RedirectToAction("Privacy");
    //                    }
    //                }
    //                else
    //                {
    //                    msg = "Your account is not active.";
    //                }
    //            }
    //            else
    //            {
    //                msg = "Username and password are invalid";
    //            }
    //        }

    //        return Json(new { status = status, msg = msg, is_mfa = is_mfa, mfa_registrasi = mfa_registrasi, register = register, BarcodeImageUrl = BarcodeImageUrl, SetupCode = SetupCode, urlPath = urlPath });
    //    }
    //    catch (Exception ex)
    //    {
    //        return Json(new { status = status, msg = msg, is_mfa, mfa_registrasi, register, BarcodeImageUrl, SetupCode });
    //    }
    //}
    //[HttpPost]
    //public async Task<ActionResult> Login(LoginModel login)
    //{
    //    bool status = false;
    //    var isValidTwoFactorAuthenticationString = HttpContext.Session.GetString("IsValidTwoFactorAuthentication");
    //    bool isValidTwoFactorAuthentication = false;

    //    if (HttpContext.Session.GetString("username") == null || isValidTwoFactorAuthenticationString == null ||
    //        (isValidTwoFactorAuthenticationString != null && bool.TryParse(isValidTwoFactorAuthenticationString, out isValidTwoFactorAuthentication) && !isValidTwoFactorAuthentication))
    //    {
    //        string googleAuthKey = "P0W48S534";
    //        string UserUniqueKey = (login.UserName + googleAuthKey);
    //        var tokenpid = Guid.NewGuid().ToString();
    //        login.Password = "30uaxb5Tw7t4YWuexAru+Q==";

    //        var user = (await _usersRepository.GetUserDataAsync(login.UserName, login.Password, tokenpid)).FirstOrDefault();
    //        if (user != null)
    //        {
    //            if (user.Uactive == 1 && user.Eactive == 1)
    //            {
    //                HttpContext.Session.SetString("username", login.UserName);
    //                TwoFactorAuthenticator TwoFacAuth = new TwoFactorAuthenticator();
    //                var setupInfo = TwoFacAuth.GenerateSetupCode("UdayDodiyaAuthDemo.com", login.UserName, ConvertSecretToBytes(UserUniqueKey, false), 300);
    //                HttpContext.Session.SetString("UserUniqueKey", UserUniqueKey);
    //                ViewBag.BarcodeImageUrl = setupInfo.QrCodeSetupImageUrl;
    //                ViewBag.SetupCode = setupInfo.ManualEntryKey;
    //                SetSessionValues(user);
    //                status = true;
    //            }
    //        }
    //        else
    //        {
    //            return RedirectToAction("Index", "Home");

    //        }
    //    }
    //    else
    //    {
    //       return RedirectToAction("Index", "Home");
    //    }

    //    ViewBag.Status = status;
    //    return View();
    //}
    private void SetSessionValues(Users_GetData2 user)
    {
        HttpContext.Session.SetString("id", user.ID_user.ToString());
        HttpContext.Session.SetString("npk", user.npk.ToString());
        HttpContext.Session.SetString("username", user.username);
        HttpContext.Session.SetString("name", user.name);
        HttpContext.Session.SetString("divisi", user.division_name);
        HttpContext.Session.SetString("level", user.level_name);
        HttpContext.Session.SetString("level_idx_no", user.level_idx_no.ToString());
        HttpContext.Session.SetString("dept_handle", user.dept_handle);
        HttpContext.Session.SetString("location_handle", user.location_handle);
    }
    private void RemoveSessionValues()
    {
        HttpContext.Session.Remove("id");
        HttpContext.Session.Remove("npk");
        HttpContext.Session.Remove("username");
        HttpContext.Session.Remove("name");
        HttpContext.Session.Remove("divisi");
        HttpContext.Session.Remove("level");
        HttpContext.Session.Remove("level_idx_no");
        HttpContext.Session.Remove("dept_handle");
        HttpContext.Session.Remove("location_handle");
    }
   
    //[HttpPost]
    //public ActionResult TwoFactorAuthenticate(LoginModel login)
    //{
    //    var msg = "";
    //    var token = login.CodeDigit;
    //    var status = false;
    //    TwoFactorAuthenticator TwoFacAuth = new TwoFactorAuthenticator();
    //    string UserUniqueKey = HttpContext.Session.GetString("UserUniqueKey").ToString();
    //    var currentTimeUtc = DateTime.UtcNow;
    //   // string userUniqueKey = "YOUR_BASE32_SECRET_KEY"; // Ensure this key is consistent and correctly formatted
    //   // token = "123456";
    //    //bool isValid = TwoFacAuth.ValidateTwoFactorPIN(userUniqueKey, token, false);
    //    bool isValid = TwoFacAuth.ValidateTwoFactorPIN(UserUniqueKey, token, TimeSpan.FromSeconds(30));
    //    if (isValid)
    //    {
    //        msg = "Token Valid";
    //        status = true;
    //        HttpContext.Session.SetString("IsValidTwoFactorAuthentication", "1");

    //        // return RedirectToAction("Index", "Home");
    //        return Json(new { status = status, msg = "Success", });
    //        //var cekToken = _context.Trtokengoogle.Where(s => s.token == token.ToString()).FirstOrDefault();
    //        //if (cekToken == null)
    //        //{
    //        //    Trtokengoogle tbl = new Trtokengoogle();
    //        //    tbl.token = token.ToString();
    //        //    tbl.created_date = DateTime.Now;
    //        //    _context.Trtokengoogle.Add(tbl);
    //        //    _context.SaveChanges();
    //        //    string protectedUserUniqueKey = ProtectData(UserUniqueKey);
    //        //    string UserCode = Convert.ToBase64String(Encoding.UTF8.GetBytes(protectedUserUniqueKey));
    //        //    var generatedTimeUtc = RetrieveTokenGenerationTimeFromSession();
    //        //    var tokenLifetime = TimeSpan.FromMinutes(2); // Adjust this to your token lifetime

    //        //    // Check if the token is expired based on current time and token generation time
    //        //    // if ((currentTimeUtc - generatedTimeUtc) > tokenLifetime)
    //        //    // {
    //        //    //     // Token has expired
    //        //    //     ViewBag.Message = "Token has expired!";
    //        //    //     return View("TokenVerificationResult");
    //        //    // }
    //        //    msg = "Token Valid";
    //        //    status = true;
    //        //    HttpContext.Session.SetString("IsValidTwoFactorAuthentication", "1");

    //        //    // return RedirectToAction("Index", "Home");
    //        //    return Json(new { status = status, msg = "Success", });

    //        //}
    //        //else
    //        //{
    //        //    msg = "Token Sudah Pernah Digunakan";
    //        //    RemoveSessionValues();
    //        //    return Json(new { status = status, msg = msg });
    //        //}
    //    }
    //    else
    //    {
    //        msg = "Token Tidak Valid";
    //        RemoveSessionValues();
    //        return Json(new { status = status, msg = msg });
    //    }


    //}
    private DateTime RetrieveTokenGenerationTimeFromSession()
    {
        // Retrieve and parse token generation time from session
        var tokenGeneratedTime = HttpContext.Session.GetString("TokenGeneratedTime");
        if (DateTime.TryParse(tokenGeneratedTime, out var generatedTimeUtc))
        {
            return generatedTimeUtc;
        }
        // Return a default value if retrieval fails
        return DateTime.MinValue;
    }
    public string ProtectData(string data)
    {
        var protector = _dataProtectionProvider.CreateProtector("YourPurpose");
        return protector.Protect(data);
    }

    public string UnprotectData(string protectedData)
    {
        var protector = _dataProtectionProvider.CreateProtector("YourPurpose");
        return protector.Unprotect(protectedData);
    }


    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
    private string LoadMenu(string id_user)
    {
        return _menuService.RecursiveMenu(id_user);
    }
}
