using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Interfaces;
using HRIS.Models;
using HRIS.Models.General;
using HRIS.Models.PM3;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace HRIS.Controllers
{

    public class RequesPWSController : Controller
    {
        private readonly ILogger<RequesPWSController> _logger;
        private readonly PM3Context _context;
        private readonly ILocationRepository _locationRepository;

        public RequesPWSController(ILogger<RequesPWSController> logger, PM3Context context, ILocationRepository locationRepository)
        {
            _logger = logger;
            _context = context;
            _locationRepository = locationRepository;
        }

        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult loadGV(string plant, string start_date, string end_date, string status_, string tglbuat)
        {
            var status = false;
            try
            {
                var lokasi_handle = "1";
                var param1 = new SqlParameter("@lokasihandle", lokasi_handle ?? "");
                var param2 = new SqlParameter("@lokasi_fliter", plant ?? "");
                var param3 = new SqlParameter("@startDate", start_date ?? "");
                var param4 = new SqlParameter("@endDate", end_date ?? "");
                var param5 = new SqlParameter("@status", status_ ?? "");
                var param6 = new SqlParameter("@tgl_buat", tglbuat ?? "");

                var dataAtt = _context.sp_PWS_getrequestplant.FromSqlRaw("Exec sp_PWS_getrequestplant @lokasihandle,@lokasi_fliter,@startDate,@endDate,@status,@tgl_buat"
                                , param1, param2, param3, param4, param5, param6).ToList().OrderByDescending(s => s.pid);
                status = true;
                return Json(new { status = status, dataAtt = dataAtt });
            }
            catch (System.Exception ex)
            {
                return Json(new { status = status });
            }
        }
        [HttpPost]
        public ActionResult GetDetail(string pid, string status_)
        {
            var status = false;
            try
            {
                var param1 = new SqlParameter("@pid_request_xml_pws", pid ?? "");
                var param2 = new SqlParameter("@status", status_ ?? "");

                var dataAtt = _context.sp_PWS_getDetailRequest.FromSqlRaw("Exec sp_PWS_getDetailRequest @pid_request_xml_pws,@status"
                                , param1, param2).ToList().OrderByDescending(s => s.pid);
                status = true;
                return Json(new { status = status, dataAtt = dataAtt });
            }
            catch (System.Exception ex)
            {
                return Json(new { status = status });
            }
        }
        [HttpPost]
        public ActionResult CreateReqPlant([FromBody] Z_tx_req_xml_pws cls_)
        {
            var status = false;
            try
            {
                var sql = @"
                    INSERT INTO Z_tx_req_xml_pws (plant, startDate, endDate, request, status, created_by, created_date) 
                    VALUES (@plant, @startDate, @endDate, @request, @status, @created_by, @created_date)";

                var parameters = new[]
                {
                    new SqlParameter("@plant", cls_.plant),
                    new SqlParameter("@startDate", cls_.startDate),
                    new SqlParameter("@endDate", cls_.endDate),
                    new SqlParameter("@request", "plant"),
                    new SqlParameter("@status", false),
                    new SqlParameter("@created_by", ""),
                    new SqlParameter("@created_date", DateTime.Now)
                };

                _context.Database.ExecuteSqlRaw(sql, parameters);
                status = true;


                return Json(new { status = status, msg = "Berhasil Request" });
            }
            catch (Exception ex)
            {
                return Json(new { status = status, msg = "" });
            }
        }
        [HttpPost]
        public ActionResult CreateReqNpk([FromBody] clsNpk cls_)
        {
            var status = false;
            try
            {
                var crea = "37752";
                var sql = @"
                    INSERT INTO Z_tx_req_xml_pws (plant, startDate, endDate, request, status, created_by, created_date) 
                    VALUES (@plant, @startDate, @endDate, @request, @status, @created_by, @created_date)";

                var parameters = new[]
                {
                    new SqlParameter("@plant", cls_.plant),
                    new SqlParameter("@startDate", cls_.startDate),
                    new SqlParameter("@endDate", cls_.endDate),
                    new SqlParameter("@request", "npk"),
                    new SqlParameter("@status", false),
                    new SqlParameter("@created_by",crea),
                    new SqlParameter("@created_date", DateTime.Now)
                };

                _context.Database.ExecuteSqlRaw(sql, parameters);
                List<string> Ids = new List<string>();
                var multiArray = cls_.npk.Split(new Char[] { ' ', '\n', ',', '\t', '\r' });
                foreach (string author in multiArray)
                {
                    if (author.Trim() != "")
                    {
                        Ids.Add(author);
                    }
                }
                string combinedString = string.Join(",", Ids);
                var d = _context.Z_tx_req_xml_pws.Where(s => s.created_by == "37752" && s.request == "npk").OrderByDescending(s => s.pid).FirstOrDefault();
                var pid = "0";
                if (d != null)
                {
                    pid = d.pid.ToString();
                }
                var param1 = new SqlParameter("@pid_request_xml_pws", pid ?? "");
                var param2 = new SqlParameter("@ID_lokasi", cls_.plant ?? "");
                var param3 = new SqlParameter("@NPK", combinedString ?? "");
                var result = _context.Database.ExecuteSqlRawAsync(
                "EXEC sp_PWS_Insert_tbl_XML_REQ_NPK @pid_request_xml_pws, @ID_lokasi,@NPK",
                param1,
                param2,
                param3
                );
                status = true;
                return Json(new { status = status, msg = "Berhasil Request" });
            }
            catch (Exception ex)
            {
                return Json(new { status = status, msg = "" });
            }
        }
        [HttpPost]
        public async Task<ActionResult> GetLokasi()
        {
            try
            {
                var locations = await _locationRepository.GetLokasiAsync();
                var lis = locations.Select(s => new { value = s.ID_lokasi.ToString(), text = $"{s.locationNm} ({s.locationCode})" });
                return Json(new { lis });
            }
            catch (Exception ex)
            {
                return Json(new { data = "", message = ex.Message + " - " + (ex.InnerException?.Message ?? ""), success = false });
            }
        }

    }
    public class clsNpk
    {
        public string plant { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string npk { get; set; }
    }
}