using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models;
using HRIS.Models.General;
using HRIS.Models.PM3;
using HRIS.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace HRIS.Controllers
{

    public class MasterShiftController : Controller
    {
        private readonly ILogger<MasterShiftController> _logger;
        private readonly PM3Context _context;
        private readonly WebBased _webBased;
        private readonly MenuService _menuService;

        public MasterShiftController(ILogger<MasterShiftController> logger, PM3Context context, IHttpContextAccessor httpContextAccessor, MenuService menuService)
        {
            _logger = logger;
            _context = context;
            _webBased = new WebBased(httpContextAccessor);
            _menuService = menuService;
        }

        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("username") == null)
            {
                return RedirectToAction("Default");
            }
            var id_user = _webBased.UserId;
            ViewBag.leftmenu = this.LoadMenu(id_user);
            return View();
        }
        private string LoadMenu(string id_user)
        {
            return _menuService.RecursiveMenu(id_user);
        }
        [HttpPost]
        public ActionResult loadGV(string namashift, string kodeShift)
        {
            var status = false;
            try
            {
                status = true;
                var param1 = new SqlParameter("@namashift", namashift ?? "");
                var param2 = new SqlParameter("@kodeShift", kodeShift ?? "");

                var dataAtt = _context.sp_Shift_getMasterData.FromSqlRaw("Exec sp_Shift_getMasterData @namashift,@kodeShift"
                                , param1, param2).ToList();
                status = true;
                return Json(new { status = status, dataAtt = dataAtt });

            }
            catch (System.Exception ex)
            {
                return Json(new { status = status });
            }
        }

    }
}