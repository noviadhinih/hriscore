using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Interfaces;
using HRIS.Models;
using HRIS.Models.General;
using HRIS.Models.PM3;
using HRIS.Repositories;
using HRIS.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace HRIS.Controllers.UML
{

    public class UMLController : Controller
    {
        private readonly ILogger<UMLController> _logger;
        private readonly WebBased _webBased;
        private readonly PM3Context _context;
        private readonly IDeptRepository _deptRepository;
        private readonly TrLogErrorRepository _trLogErrorRepository;
        private readonly MenuService _menuService;
        private readonly IMasterShiftHeaderRepository _masterShiftHeaderRepository;
        private readonly ILocationRepository _locationRepository;

        public UMLController(ILogger<UMLController> logger, IHttpContextAccessor httpContextAccessor, PM3Context context, IDeptRepository deptRepository, TrLogErrorRepository trLogErrorRepository, MenuService menuService, IMasterShiftHeaderRepository masterShiftHeaderRepository, ILocationRepository locationRepository)
        {
            _logger = logger;
            _webBased = new WebBased(httpContextAccessor);
            _context = context;
            _deptRepository = deptRepository;
            _trLogErrorRepository = trLogErrorRepository;
            _menuService = menuService;
            _masterShiftHeaderRepository = masterShiftHeaderRepository;
            _locationRepository = locationRepository;
        }

        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("username") == null)
            {
                return RedirectToAction("Default", "Home");
            }
            var id_user = _webBased.UserId;
            ViewBag.leftmenu = this.LoadMenu(id_user);
            return View();
        }
        public IActionResult Report()
        {
            if (HttpContext.Session.GetString("username") == null)
            {
                return RedirectToAction("Default", "Home");
            }
            var id_user = _webBased.UserId;
            ViewBag.leftmenu = this.LoadMenu(id_user);
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> loadGV([FromBody] clsf cls_)
        {
            var status = false;
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            try
            {
                var search = Request.Query["search[value]"].ToString();
                var start = Convert.ToInt32(Request.Query["start"]);
                var length = Convert.ToInt32(Request.Query["length"]);

                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int totalRecords = 0;

                var deptHandle = _webBased.DeptHandle;
                var locHandle = _webBased.LocationHandle;
                if (cls_.stats == "")
                    cls_.stats = "%";

                var param1 = new SqlParameter("@date_start", cls_.date_start ?? "");
                var param2 = new SqlParameter("@date_end", cls_.date_end ?? "");
                var param3 = new SqlParameter("@mealtime", cls_.mealtime ?? "");
                var param4 = new SqlParameter("@shift", cls_.shift ?? "");
                var param5 = new SqlParameter("@locationID", cls_.locationID ?? "");
                var param6 = new SqlParameter("@npk", cls_.npk ?? "");
                var param7 = new SqlParameter("@name", cls_.name ?? "");
                var param8 = new SqlParameter("@stats", cls_.stats ?? "%");
                var param9 = new SqlParameter("@ID_dept", cls_.ID_dept ?? "");
                var param10 = new SqlParameter("@dws_type", cls_.dws_type ?? "");
                var param11 = new SqlParameter("@deptHandle", deptHandle ?? "");
                var param12 = new SqlParameter("@locHandle", locHandle ?? "");
                var param13 = new SqlParameter("@dayNight", cls_.dayNight ?? "");

                var data = await _context.sp_retrieveMealTime.FromSqlRaw("Exec sp_retrieveMealTime @date_start,@date_end,@mealtime,@shift,@locationID,@npk,@name,@stats,@ID_dept,@dws_type,@deptHandle,@locHandle,@dayNight",
                    param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11, param12, param13).ToListAsync();

                totalRecords = data.Count();

                DatatableToJson datatableToJson = new DatatableToJson
                {
                    recordsFiltered = totalRecords,
                    recordsTotal = totalRecords,
                    data = data
                };

                status = true;
                return Json(datatableToJson);
            }
            catch (Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { status = status });
            }
        }

        [HttpPost]
        public async Task<IActionResult> submitPengajuan([FromBody] List<clsApv> cls_)
        {
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            var status = false;
            try
            {
                List<string> Ids = new List<string>();
                foreach (var item in cls_)
                {
                    Ids.Add(item.keyID);
                }
                var id = Ids.Distinct().ToList();
                if (id.Count > 0)
                {
                    string combinedString = string.Join(",", id);

                    var param1 = new SqlParameter("@IDNo_param", combinedString ?? "0");
                    await _context.Database.ExecuteSqlRawAsync("EXEC sp_MealUpdateProgress @IDNo_param", param1);

                    status = true;
                    return Json(new { status = status, msg = "Berhasil Save Data" });
                }
                else
                {
                    return Json(new { status = status, msg = "Tidak Ada Data Yang Dipilih" });
                }
            }
            catch (Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { status = status, msg = "Data Tidak Dapat Di Proses" });
            }
        }

        [HttpPost]
        public async Task<IActionResult> loadReport([FromBody] clsf cls_)
        {
            var status = false;
            string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            try
            {
                var search = Request.Query["search[value]"].ToString();
                var start = Convert.ToInt32(Request.Query["start"]);
                var length = Convert.ToInt32(Request.Query["length"]);

                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int totalRecords = 0;

                var deptHandle = _webBased.DeptHandle;
                var locHandle = _webBased.LocationHandle;
                if (string.IsNullOrEmpty(cls_.stats))
                    cls_.stats = "%";

                var param1 = new SqlParameter("@date_start", cls_.date_start ?? "");
                var param2 = new SqlParameter("@date_end", cls_.date_end ?? "");
                var param3 = new SqlParameter("@proc_start", cls_.proc_start ?? "");
                var param4 = new SqlParameter("@proc_end", cls_.proc_end ?? "");
                var param5 = new SqlParameter("@mealtime", cls_.mealtime ?? "");
                var param6 = new SqlParameter("@shift", cls_.shift ?? "");
                var param7 = new SqlParameter("@locationID", cls_.locationID ?? "");
                var param8 = new SqlParameter("@npk", cls_.npk ?? "");
                var param9 = new SqlParameter("@name", cls_.name ?? "");
                var param10 = new SqlParameter("@stats", cls_.stats ?? "%");
                var param11 = new SqlParameter("@ID_dept", cls_.ID_dept ?? "");
                var param12 = new SqlParameter("@dws_type", cls_.dws_type ?? "");
                var param13 = new SqlParameter("@deptHandle", deptHandle ?? "");
                var param14 = new SqlParameter("@locHandle", locHandle ?? "");
                var param15 = new SqlParameter("@dayNight", cls_.dayNight ?? "");

                var data = await _context.sp_retrieveMealTimeRpt.FromSqlRaw(
                    "Exec sp_retrieveMealTimeRpt @date_start,@date_end,@proc_start,@proc_end,@mealtime,@shift,@locationID,@npk,@name,@stats,@ID_dept,@dws_type,@deptHandle,@locHandle,@dayNight",
                    param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11, param12, param13, param14, param15
                ).ToListAsync();

                totalRecords = data.Count();

                var datatableToJson = new DatatableToJson
                {
                    recordsFiltered = totalRecords,
                    recordsTotal = totalRecords,
                    data = data
                };

                return Json(datatableToJson);
            }
            catch (Exception ex)
            {
                await _trLogErrorRepository.Insert(controllerName, actionName, _webBased.Npk, ex.Message);
                return Json(new { status = status });
            }
        }

         private string LoadMenu(string id_user)
        {
            return _menuService.RecursiveMenu(id_user);
        }
        [HttpPost]
        public async Task<ActionResult> GetLokasi()
        {
            try
            {
                var locations = await _locationRepository.GetLokasiAsync();
                var lis = locations.Select(s => new { value = s.ID_lokasi.ToString(), text = $"{s.locationNm} ({s.locationCode})" });
                return Json(new { lis });
            }
            catch (Exception ex)
            {
                return Json(new { data = "", message = ex.Message + " - " + (ex.InnerException?.Message ?? ""), success = false });
            }
        }
        [HttpPost]
        public async Task<ActionResult> GetDept()
        {
            try
            {
                var depts = await _deptRepository.GetDeptAsync();
                var lis = depts.Select(s => new { value = s.ID_dept.ToString(), text = s.name });
                return Json(new { lis });
            }
            catch (Exception ex)
            {
                return Json(new { data = "", message = ex.Message + " - " + ex.InnerException?.Message, success = false });
            }
        }
        [HttpPost]
        public async Task<ActionResult> GetShift()
        {
            try
            {
                var shifts = await _masterShiftHeaderRepository.GetActiveShiftsAsync();
                var lis = shifts.Select(s => new { value = s.ID_Master_Shift_Header.ToString(), text = $"{s.Master_Shift_Header_Name} ({s.shift_code})" });
                return Json(new { lis });
            }
            catch (Exception ex)
            {
                return Json(new { data = "", message = ex.Message + " - " + (ex.InnerException?.Message ?? ""), success = false });
            }
        }

    }
    public class clsf
    {
        public string date_start { get; set; }
        public string date_end { get; set; }
        public string proc_start { get; set; }
        public string proc_end { get; set; }
        public string mealtime { get; set; }
        public string shift { get; set; }
        public string locationID { get; set; }
        public string npk { get; set; }
        public string name { get; set; }
        public string stats { get; set; }
        public string ID_dept { get; set; }
        public string dws_type { get; set; }
        public string dayNight { get; set; }
    }
    public class clsApv
    {
        public string keyID { get; set; }
    }
}