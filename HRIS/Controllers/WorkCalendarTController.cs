using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models;
using HRIS.Models.General;
using HRIS.Models.PM3;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using HRIS.Repositories;
using HRIS.Interfaces;

namespace HRIS.Controllers
{
    public class WorkCalendarTController : Controller
    {
        private readonly ILogger<WorkCalendarTController> _logger;
        private readonly PM3Context _context;
        private readonly IViewDwsRepository _viewDwsInterface;
        private readonly IMasterShiftHeaderRepository _masterShiftHeaderRepository;

        public WorkCalendarTController(ILogger<WorkCalendarTController> logger, PM3Context context, IViewDwsRepository viewDwsInterface, IMasterShiftHeaderRepository masterShiftHeaderRepository)
        {
            _logger = logger;
            _context = context;
            _viewDwsInterface = viewDwsInterface;
            _masterShiftHeaderRepository = masterShiftHeaderRepository;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> GetDWS()
        {
            try
            {
                var lis = await _viewDwsInterface.GetDWSAsync();
                var data = lis.Select(s => new { value = s.DWS_ID.ToString(), text = s.DWS_Name });
                return Json(new { lis = data });
            }
            catch (Exception ex)
            {
                return Json(new { data = "", message = ex.Message + " - " + ex.InnerException, success = false });
            }
        }

        [HttpPost]
        public ActionResult loadGV(string namaKalendar, string id_shift)
        {
            var status = false;
            try
            {
                var param1 = new SqlParameter("@namakalen", namaKalendar ?? "");
                var param2 = new SqlParameter("@id_shift", id_shift ?? "");

                var dataAtt = _context.sp_WCalendar_getdata.FromSqlRaw("Exec sp_WCalendar_getdata @namakalen,@id_shift", param1, param2)
                                                          .ToList()
                                                          .OrderByDescending(s => s.WSR_ID);
                status = true;
                return Json(new { status = status, dataAtt = dataAtt });
            }
            catch (System.Exception ex)
            {
                return Json(new { status = status });
            }
        }

        [HttpPost]
        public ActionResult GenerateTemplate(string id_shift, string date_start, string date_end, string ddlbulan)
        {
            var status = false;
            try
            {
                var param1 = new SqlParameter("@ID_Master_Shift_Header", id_shift ?? "");
                var param2 = new SqlParameter("@dateStart", date_start ?? "");
                var param3 = new SqlParameter("@dateEnd", date_end ?? "");
                var param4 = new SqlParameter("@Monthly", ddlbulan ?? "");

                var dataAtt = _context.sp_WCalendar_generate.FromSqlRaw("Exec sp_WCalendar_generate @ID_Master_Shift_Header,@dateStart,@dateEnd,@Monthly", param1, param2, param3, param4)
                                                            .ToList()
                                                            .OrderBy(s => s.detail_ID);
                status = true;
                return Json(new { status = status, detail = dataAtt });
            }
            catch (System.Exception ex)
            {
                return Json(new { status = status });
            }
        }

        [HttpPost]
        public ActionResult getEdit(string WsrId)
        {
            var status = false;
            try
            {
                var bln = 12;
                var thn = 2023;
                Int64 pid = 0;
                Int64.TryParse(WsrId, out pid);

                var dataAtt = _context.WSR_Template.Where(s => s.WSR_ID == pid).FirstOrDefault();
                var detail = _context.WSR_Template_Detail.Where(s => s.WSR_ID == pid && s.calendar_date.Value.Month == bln && s.calendar_date.Value.Year == thn)
                                                         .ToList()
                                                         .OrderBy(s => s.calendar_date);

                status = true;
                return Json(new { status = status, dataAtt = dataAtt, detail = detail });
            }
            catch (System.Exception ex)
            {
                return Json(new { status = status });
            }
        }

        [HttpPost]
        public async Task<ActionResult> GetShift()
        {
            try
            {
                var shifts = await _masterShiftHeaderRepository.GetActiveShiftsAsync();
                var result = shifts.Select(s => new { value = s.ID_Master_Shift_Header.ToString(), text = s.Master_Shift_Header_Name });
                return Json(new { result });
            }
            catch (Exception ex)
            {
                return Json(new { data = "", message = ex.Message + " - " + ex.InnerException?.Message, success = false });
            }
        }
        [HttpPost]
        public async Task<ActionResult> GetDWSDetail(string DWS_ID)
        {
            try
            {
                if (!Int64.TryParse(DWS_ID, out long pid))
                {
                    throw new ArgumentException("Invalid DWS_ID provided.");
                }

                var dwsDetail = await _viewDwsInterface.GetDWSAsync(pid);

                if (dwsDetail != null)
                {
                    return Json(new { lis = dwsDetail });
                }
                else
                {
                    return Json(new { data = "", message = "DWS detail not found.", success = false });
                }
            }
            catch (Exception ex)
            {
                return Json(new { data = "", message = ex.Message + " - " + ex.InnerException, success = false });
            }
        }

        [HttpPost]
        public ActionResult createWSR([FromBody] clsParam cls_)
        {
            var status = false;
            try
            {
                cls_.action = "add";
                var id_user = 0;

                var param1 = new SqlParameter("@action", cls_.action ?? "");
                var param2 = new SqlParameter("@WSR_ID", cls_.WSR_ID ?? "");
                var param3 = new SqlParameter("@WSR_Name", cls_.WSR_Name ?? "");
                var param4 = new SqlParameter("@ID_Master_Shift_Header", cls_.ID_Master_Shift_Header ?? "");
                var param5 = new SqlParameter("@start_date", cls_.start_date ?? "");
                var param6 = new SqlParameter("@end_date", cls_.end_date ?? "");
                var param7 = new SqlParameter("@is_active", cls_.active_ ?? "");
                var param8 = new SqlParameter("@id_user", id_user);

                var result = _context.sp_WCalendar_crud.FromSqlRaw("EXEC sp_WCalendar_crud @action,@WSR_ID,@WSR_Name,@ID_Master_Shift_Header,@start_date,@end_date,@is_active,@id_user",
                    param1, param2, param3, param4, param5, param6, param7, param8)
                                                     .ToList()
                                                     .FirstOrDefault();

                if (result != null)
                {
                    var d = result.WSR_ID;

                    foreach (var item in cls_.detail)
                    {
                        var param9 = new SqlParameter("@WSR_ID", d ?? "");
                        var param10 = new SqlParameter("@calendar_date", item.calendar_date ?? "");
                        var param11 = new SqlParameter("@DWS_ID", item.DWS_ID ?? "");
                        var param12 = new SqlParameter("@DWS_Name", item.DWS_Name ?? "");
                        var param13 = new SqlParameter("@DWS_Type", item.DWS_Type ?? "");
                        var param14 = new SqlParameter("@clock_in", item.clock_in ?? "");
                        var param15 = new SqlParameter("@clock_out", item.clock_out ?? "");
                        var param16 = new SqlParameter("@notes", item.notes);

                        var result2 = _context.Database.ExecuteSqlRawAsync(
                            "EXEC sp_Wcalendar_InsertDetail @WSR_ID,@calendar_date,@DWS_ID,@DWS_Name,@DWS_Type,@clock_in,@clock_out,@notes",
                            param9, param10, param11, param12, param13, param14, param15, param16
                        );
                    }

                    status = true;
                    return Json(new { status = status, msg = "Berhasil Save Data" });
                }
                else
                {
                    return Json(new { status = status, msg = "Tidak Berhasil Save Data" });
                }
            }
            catch (System.Exception ex)
            {
                return Json(new { status = status, msg = "" });
            }
        }

        [HttpPost]
        public ActionResult deleteWSR([FromBody] clsParam cls_)
        {
            var status = false;
            try
            {
                cls_.action = "delete";
                var id_user = 0;

                var param1 = new SqlParameter("@action", cls_.action ?? "");
                var param2 = new SqlParameter("@WSR_ID", cls_.WSR_ID);
                var param3 = new SqlParameter("@WSR_Name", "");
                var param4 = new SqlParameter("@ID_Master_Shift_Header", "");
                var param5 = new SqlParameter("@start_date", "");
                var param6 = new SqlParameter("@end_date", "");
                var param7 = new SqlParameter("@is_active", "");
                var param8 = new SqlParameter("@id_user", id_user);

                var result = _context.Database.ExecuteSqlRaw(
                    "EXEC sp_WCalendar_crud @action,@WSR_ID,@WSR_Name,@ID_Master_Shift_Header,@start_date,@end_date,@is_active,@id_user",
                    param1, param2, param3, param4, param5, param6, param7, param8
                );

                status = true;

                return Json(new { status = status, msg = "Berhasil Save Data" });
            }
            catch (System.Exception)
            {
                return Json(new { status = status, msg = "" });
            }
        }
    }

    public class clsParam
    {
        public string? WSR_Name { get; set; }
        public string? ID_Master_Shift_Header { get; set; }
        public string? start_date { get; set; }
        public string? end_date { get; set; }
        public string? action { get; set; }
        public string? active_ { get; set; }
        public string? WSR_ID { get; set; }
        public List<clsDetail> detail { get; set; }
    }
    public class clsDetail
    {
        public string? detail_ID { get; set; }
        public string? calendar_date { get; set; }
        public string? DWS_ID { get; set; }
        public string? DWS_Name { get; set; }
        public string? DWS_Type { get; set; }
        public string? clock_in { get; set; }
        public string? clock_out { get; set; }
        public string? notes { get; set; }
    }
}