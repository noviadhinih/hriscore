using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using HRIS.Interfaces;
using HRIS.Interfaces.TMS;
using HRIS.Models;
using HRIS.Models.General;
using HRIS.Models.PM3;
using HRIS.Repositories;
using HRIS.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace HRIS.Controllers.Setting
{
   
    public class MenuController : Controller
    {
        private readonly ILogger<MenuController> _logger;
        private readonly WebBased _webBased;
        private readonly TrLogErrorRepository _trLogErrorRepository;
        private readonly MenuService _menuService;
       
        public MenuController(ILogger<MenuController> logger, IHttpContextAccessor httpContextAccessor, TrLogErrorRepository trLogErrorRepository, MenuService menuService)
        {
            _logger = logger;
            _webBased = new WebBased(httpContextAccessor);
            _trLogErrorRepository = trLogErrorRepository;
            _menuService = menuService;
        }

        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("username") == null)
            {
                return RedirectToAction("Default", "Home");
            }
            var id_user = _webBased.UserId;
            ViewBag.leftmenu = this.LoadMenu(id_user);
            ViewBag.npk = _webBased.Npk;
            return View();
        }
        private string LoadMenu(string id_user)
        {
            return _menuService.RecursiveMenu(id_user);
        }
    }
}