using System.Collections.Generic;
using System.Threading.Tasks;
using HRIS.Interfaces;
using HRIS.Models.PM3;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Repositories
{
    public class ViewDwsRepository : IViewDwsRepository
    {
        private readonly PM3Context _context;

        public ViewDwsRepository(PM3Context context)
        {
            _context = context;
        }

        public async Task<List<view_DWS>> GetDWSAsync()
        {
            return await _context.view_DWS.ToListAsync();
        }

        public async Task<view_DWS> GetDWSAsync(long dwsId)
        {
            return await _context.view_DWS.FirstOrDefaultAsync(v => v.DWS_ID == dwsId);
        }
    }
}
