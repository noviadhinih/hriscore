using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Interfaces;
using HRIS.Models.PM3;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Repositories
{
    public class MasterShiftHeaderRepository : IMasterShiftHeaderRepository
    {
        private readonly PM3Context _context;

        public MasterShiftHeaderRepository(PM3Context context)
        {
            _context = context;
        }

        public async Task<List<Master_Shift_Header>> GetActiveShiftsAsync()
        {
            return await _context.Master_Shift_Header.Where(s => s.active == 1).ToListAsync();
        }
    }
}
