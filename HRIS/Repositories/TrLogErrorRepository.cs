using System;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using HRIS.Models.General;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Repositories
{
    public class TrLogErrorRepository
    {
        private readonly PM3Context _context;

        public TrLogErrorRepository(PM3Context context)
        {
            _context = context;
        }

        public async Task<return_value> Insert(string controller, string method, string npk, string logError)
        {
            try
            {
                TrLogError tbl = new TrLogError();
                tbl.controller = controller;
                tbl.method = method;
                tbl.created_by = npk;
                tbl.created_date = DateTime.Now;
                tbl.logError=logError;
                _context.TrLogError.Add(tbl);
                _context.SaveChanges();
                
                _context.TrLogError.Add(tbl);
                await _context.SaveChangesAsync();
                
                return new return_value { status = true, remarks = "Berhasil menambah data" };
            }
            catch (Exception ex)
            {
                return new return_value { status = false, remarks = ex.Message };
            }
        }
    }
}
