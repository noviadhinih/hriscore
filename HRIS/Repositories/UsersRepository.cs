using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Interfaces;
using HRIS.Models;
using HRIS.Models.General;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        private readonly PM3Context _context;

        public UsersRepository(PM3Context context)
        {
            _context = context;
        }

        public async Task<List<Users_GetData2>> GetUserDataAsync(string username, string password, string token)
        {
            var param1 = new SqlParameter("@Username", username);
            var param2 = new SqlParameter("@password", password);
            var param3 = new SqlParameter("@token", token);
            
            return await _context.Users_GetData2
                .FromSqlRaw("Exec Users_GetData2 @Username, @password, @token", param1, param2, param3)
                .ToListAsync();
        }
        public async Task<return_value> RegisterMFA(string id_user, string token)
        {
            try
            {
                var param1 = new SqlParameter("@id_user", id_user ?? "0");
                var param2 = new SqlParameter("@token", token ?? "0");
               
                await _context.Database.ExecuteSqlRawAsync("EXEC sp_User_insertTokenMfa @id_user,@token", param1, param2);


                return new return_value { status = true, remarks = "Berhasil Register" };
            }
            catch (Exception ex)
            {
                return new return_value { status = false, remarks = ex.Message };
            }
        }
    }
}
