﻿using System.Collections.Generic;
using System.Linq;
using HRIS.Controllers.Magang;
using HRIS.Interfaces;
using HRIS.Models.General;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
namespace HRIS.Repositories
{
    public class OTManagementRepository : IOTManagementRepository
    {
        private readonly PM3Context _context;
        public OTManagementRepository(PM3Context context)
        {
            _context = context;
        }
        public async Task<List<sp_Overtime_getMsProd>> GetDataMprod(clsOT f)
        {
            try
            {
                var param1 = new SqlParameter("@shop", f.pid_shop ?? "");
               
                var data = await _context.sp_Overtime_getMsProd.FromSqlRaw("Exec sp_Overtime_getMsProd @shop"
                                  , param1).ToListAsync();

                return data;
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<sp_Overtime_getMsProd>();
            }
        }
        public async Task<List<Overtime_tbl_r_shop>> getShop()
        {
            try
            {
                var data = await _context.Overtime_tbl_r_shop.ToListAsync();
                return data;
            }
            catch (System.Exception ex)
            {
                return new List<Overtime_tbl_r_shop>();
            }
        }
        public async Task<List<Overtime_tbl_r_line>> getLine()
        {
            try
            {
                var data = await _context.Overtime_tbl_r_line.ToListAsync();
                return data;
            }
            catch (System.Exception ex)
            {
                return new List<Overtime_tbl_r_line>();
            }
        }
        public async Task<return_value> InsertMprod(clsOT f)
        {
            try
            {
                var param1 = new SqlParameter("@pid_shop", f.pid_shop ?? "");
                var param2 = new SqlParameter("@shop", f.shop ?? "");
                var param3 = new SqlParameter("@pid_line", f.pid_line ?? "");
                var param4 = new SqlParameter("@line", f.line ?? "");
                var param5 = new SqlParameter("@pis", f.pis ?? "");
                var param6 = new SqlParameter("@durasi", f.durasi ?? "");
                var param7 = new SqlParameter("@created_by", f.npkLogin ?? "");
                var param8 = new SqlParameter("@id_lokasi", f.LocationId ?? "");
                var param9 = new SqlParameter("@tgl_valid", f.tglEfektif ?? "");
                var param10 = new SqlParameter("@tgl_end", f.tglEnd ?? "");
                var param11 = new SqlParameter("@dot_hot", f.dot_hot ?? "");
                var param12 = new SqlParameter("@durasiPIS", f.durasiPIS ?? "");

                await _context.Database.ExecuteSqlRawAsync("EXEC sp_Overtime_InsertMprod @pid_shop,@shop,@pid_line,@line,@pis,@durasi,@created_by,@id_lokasi,@tgl_valid,@tgl_end,@dot_hot,@durasiPIS", param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11, param12);


                return new return_value { status = true, remarks = "Insert Master Berhasil" };
            }
            catch (Exception ex)
            {
                return new return_value { status = false, remarks = ex.Message };
            }
        }

    }
    public class clsOT
    {
        public string? pid_shop { get; set; }
        public string? shop { get; set; }
        public string? pid_line { get; set; }
        public string? line { get; set; }
        public string? dot_hot { get; set; }
        public string? day_night { get; set; }
        public string? tgl { get; set; }
        public string? shift { get; set; }
        public string? pis { get; set; }
        public string? durasi { get; set; }
        public string? npkLogin { get; set; }
        public string? LocationId { get; set; }
        public string? tglEfektif { get; set; }
        public string? tglEnd { get; set; }
        public string? durasiPIS { get; set; }
    }

}
