using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Interfaces;
using HRIS.Models.PM3;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Repositories
{
    public class DeptRepository : IDeptRepository
    {
        private readonly PM3Context _context;

        public DeptRepository(PM3Context context)
        {
            _context = context;
        }

        public async Task<List<dept>> GetDeptAsync()
        {
            return await _context.dept.Where(s => s.active == 1).ToListAsync();
        }
    }
}
