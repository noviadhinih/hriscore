using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Interfaces;
using HRIS.Models.PM3;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Repositories
{
    public class LocationRepository : ILocationRepository
    {
        private readonly PM3Context _context;

        public LocationRepository(PM3Context context)
        {
            _context = context;
        }

        public async Task<List<location>> GetLokasiAsync()
        {
            return await _context.location.Where(s => s.SAP_ID != null).ToListAsync();
        }
    }
}
