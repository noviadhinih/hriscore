using System.Collections.Generic;
using System.Linq;
using HRIS.Interfaces;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Repositories
{
    public class MenuRepository : IMenuRepository
    {
        private readonly PM3Context _context;

        public MenuRepository(PM3Context context)
        {
            _context = context;
        }

        public List<sp_getMenu> GetMenu(string id_user)
        {
            try
            {
                var param1 = new SqlParameter("@id_user", id_user);
                var data = _context.sp_getMenu
                    .FromSqlRaw("EXEC sp_getMenu @id_user", param1)
                    .ToList();

                return data;
            }
            catch(System.Exception ex)
            {
                // Handle exception or log it
                return new List<sp_getMenu>();
            }
        }
    }
}