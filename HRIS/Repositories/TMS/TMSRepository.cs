using System.Collections.Generic;
using System.Linq;
using HRIS.Controllers.TMS;
using HRIS.Interfaces;
using HRIS.Interfaces.TMS;
using HRIS.Models.General;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
namespace HRIS.Repositories.TMS
{
    public class TMSRepository : ITMSRepository
    {
        private readonly PM3Context _context;

        public TMSRepository(PM3Context context)
        {
            _context = context;
        }
        #region SUPEM
        public async Task<List<sp_SUPEM_getDataReject>> RejectGetDataSupem(string deptHandle, string locHandle, string npk, string tgl_request_start, string tgl_request_end, string status, string tahun, string npk_input)
        {
            try
            {
                var param1 = new SqlParameter("@depthandle", deptHandle ?? "");
                var param2 = new SqlParameter("@lokasihandle", locHandle ?? "");
                var param3 = new SqlParameter("@npk", npk ?? "");
                var param4 = new SqlParameter("@tgl_request_start", tgl_request_start ?? "");
                var param5 = new SqlParameter("@tgl_request_end", tgl_request_end ?? "");
                var param6 = new SqlParameter("@status", status ?? "");
                var param7 = new SqlParameter("@tahun", tahun ?? "");
                var param8 = new SqlParameter("@npk_input", npk_input ?? "");
                var data = await _context.sp_SUPEM_getDataReject.FromSqlRaw("Exec sp_SUPEM_getDataReject @depthandle,@lokasihandle,@npk,@tgl_request_start,@tgl_request_end,@status,@tahun,@npk_input"
                                  , param1, param2, param3, param4, param5, param6, param7, param8).ToListAsync();

                return data;
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<sp_SUPEM_getDataReject>();
            }
        }
        public async Task<List<sp_SUPEM_getData>> GetDataSupem(string deptHandle, string locHandle, string pr, string yr, string npk, string nama, string emp_status, string status, string divisi,
                                                              string dept, string golongan, string npk_input, string date_input, string costcenter, string shift, string type_spl, string UserId,
                                                              string npkLogin, string tgl_mulai, string tgl_selesai)
        {
            try
            {
                var param1 = new SqlParameter("@depthandle", deptHandle ?? "");
                var param2 = new SqlParameter("@lokasihandle", locHandle ?? "");
                var param3 = new SqlParameter("@pr", pr ?? "");
                var param4 = new SqlParameter("@yr", yr ?? "");
                var param5 = new SqlParameter("@npk", npk ?? "");
                var param6 = new SqlParameter("@nama", nama ?? "");
                var param7 = new SqlParameter("@emp_status", emp_status ?? "");
                var param8 = new SqlParameter("@status", status ?? "");
                var param9 = new SqlParameter("@divisi", divisi ?? "");
                var param10 = new SqlParameter("@dept", dept ?? "");
                var param11 = new SqlParameter("@golongan", golongan ?? "");
                var param12 = new SqlParameter("@npk_input", npk_input ?? "");
                var param13 = new SqlParameter("@date_input", date_input ?? "");
                var param14 = new SqlParameter("@costcenter", costcenter ?? "");
                var param15 = new SqlParameter("@shift", shift ?? "");
                var param16 = new SqlParameter("@type_spl", type_spl ?? "");
                var param17 = new SqlParameter("@UserId", UserId ?? "");
                var param18 = new SqlParameter("@npkLogin", npkLogin ?? "");
                var param19 = new SqlParameter("@tgl_mulai", tgl_mulai ?? "");
                var param20 = new SqlParameter("@tgl_selesai", tgl_selesai ?? "");

                var data = await _context.sp_SUPEM_getData.FromSqlRaw("Exec sp_SUPEM_getData @depthandle,@lokasihandle,@pr,@yr,@npk,@nama,@emp_status,@status,@divisi,@dept,@golongan,@npk_input,@date_input,@costcenter,@shift,@type_spl,@UserId,@npkLogin,@tgl_mulai,@tgl_selesai"
                                  , param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11, param12, param13, param14, param15, param16, param17, param18, param19, param20).ToListAsync();

                return data;
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<sp_SUPEM_getData>();
            }
        }

        public async Task<List<sp_SUPEM_getRekapExcel>> GetdataRekapExcelSupem(string permission_id)
        {
            try
            {
                var param1 = new SqlParameter("@permission_id", permission_id ?? "");

                var data = await _context.sp_SUPEM_getRekapExcel.FromSqlRaw("Exec sp_SUPEM_getRekapExcel @permission_id"
                                  , param1).ToListAsync();

                return data;
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<sp_SUPEM_getRekapExcel>();
            }
        }
        public async Task<List<SUPEM_Type_Get>> GetPermissionTypeByID(string permit_id)
        {
            try
            {
                var param1 = new SqlParameter("@permit_id", permit_id ?? "");

                var data = await _context.SUPEM_Type_Get.FromSqlRaw("Exec SUPEM_Type_Get @permit_id"
                                  , param1).ToListAsync();

                return data;
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<SUPEM_Type_Get>();
            }
        }
        public async Task<List<SUPEM_Type_Get>> GetListSupemType(string npk)
        {
            try
            {
                var param1 = new SqlParameter("@npk", npk ?? "");

                var data = await _context.SUPEM_Type_Get.FromSqlRaw("Exec SUPEM_Type_GetAllv2 @npk"
                                  , param1).ToListAsync();

                return data;
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<SUPEM_Type_Get>();
            }
        }

        public async Task<return_value> RejectSupem(string pid, string reason, string createdBy)
        {
            try
            {
                var param1 = new SqlParameter("@pid", pid ?? "0");
                var param2 = new SqlParameter("@reason", reason ?? "0");
                var param3 = new SqlParameter("@createdBy", createdBy ?? "0");
                await _context.Database.ExecuteSqlRawAsync("EXEC sp_SUPEM_Reject @pid,@reason,@createdBy", param1, param2, param3);


                return new return_value { status = true, remarks = "Data SUPEM yang terpilih berhasil di reject" };
            }
            catch (Exception ex)
            {
                return new return_value { status = false, remarks = ex.Message };
            }
        }
        public async Task<return_value> InsertSupem(SUPEM c)
        {
            try
            {
                var param1 = new SqlParameter("@npk", c.npk ?? "");
                var param2 = new SqlParameter("@tanggal", c.tanggal ?? "");
                var param3 = new SqlParameter("@jam_mulai", c.jam_mulai ?? "");
                var param4 = new SqlParameter("@jam_akhir", c.jam_akhir ?? "");
                var param5 = new SqlParameter("@info", c.info ?? "");
                var param6 = new SqlParameter("@type", c.permit_id ?? "0");
                var param7 = new SqlParameter("@lokasi", "0");
                var param8 = new SqlParameter("@atasan_spv", "");
                var param9 = new SqlParameter("@atasan_dh", c.npk_atasan ?? "");
                var param10 = new SqlParameter("@atasan_atasan", c.apv_atasan ?? "");
                var param11 = new SqlParameter("@npk_create", c.npk_create ?? "");
                var param12 = new SqlParameter("@UserId", c.ID_user ?? "");
                await _context.Database.ExecuteSqlRawAsync("EXEC sp_SUPEM_InsertTr @npk,@tanggal,@jam_mulai,@jam_akhir,@info,@type,@lokasi,@atasan_spv,@atasan_dh,@atasan_atasan,@npk_create,@UserId"
                                , param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11, param12);


                return new return_value { status = true, remarks = "Surat Pemberitahuan berhasil dibuat" };
            }
            catch (Exception ex)
            {
                return new return_value { status = false, remarks = ex.Message };
            }
        }
        public async Task<List<sp_SUPEM_getApproval>> GetSUPEMApproval(SKTA c, string UserId, string npkLogin)
        {
            try
            {
                var param1 = new SqlParameter("@depthandle", "");
                var param2 = new SqlParameter("@lokasihandle", "");
                var param3 = new SqlParameter("@pr", c.pr ?? "");
                var param4 = new SqlParameter("@yr", c.yr ?? "");
                var param5 = new SqlParameter("@npk", c.npk ?? "");
                var param6 = new SqlParameter("@nama", c.emp_name ?? "");
                var param7 = new SqlParameter("@emp_status", c.emp_status ?? "");
                var param8 = new SqlParameter("@status", c.stats ?? "");
                var param9 = new SqlParameter("@divisi", c.ID_division.ToString() ?? "");
                var param10 = new SqlParameter("@dept", c.ID_dept.ToString() ?? "");
                var param11 = new SqlParameter("@golongan", c.ID_golongan.ToString() ?? "");
                var param12 = new SqlParameter("@npk_input", c.npk_input ?? "");
                var param13 = new SqlParameter("@date_input", c.date_input ?? "");
                var param14 = new SqlParameter("@costcenter", c.costcenter ?? "");
                var param15 = new SqlParameter("@shift", c.id_shift.ToString() ?? "");
                var param16 = new SqlParameter("@type_spl", "");
                var param17 = new SqlParameter("@UserId", UserId ?? "");
                var param18 = new SqlParameter("@npkLogin", npkLogin ?? "");
                var param19 = new SqlParameter("@tgl_mulai", c.date_startText ?? "");
                var param20 = new SqlParameter("@tgl_selesai", c.date_endText ?? "");


                var data = await _context.sp_SUPEM_getApproval.FromSqlRaw("Exec sp_SUPEM_getApproval @depthandle,@lokasihandle,@pr,@yr,@npk,@nama,@emp_status,@status,@divisi,@dept,@golongan,@npk_input,@date_input,@costcenter,@shift,@type_spl,@UserId,@npkLogin,@tgl_mulai,@tgl_selesai"
                                  , param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11, param12, param13, param14, param15, param16, param17, param18, param19, param20).ToListAsync();

                return data;
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<sp_SUPEM_getApproval>();
            }
        }
        public async Task<return_value> ApproveSUPEM(string pid, string reason, string createdBy,string rejectComment)
        {
            try
            {
                var param1 = new SqlParameter("@pid", pid ?? "0");
                var param2 = new SqlParameter("@stats", reason ?? "0");
                var param3 = new SqlParameter("@npkBy", createdBy ?? "0");
                var param4 = new SqlParameter("@reason", rejectComment ?? "0");

                await _context.Database.ExecuteSqlRawAsync("EXEC sp_SUPEM_ApproveAll @pid,@stats,@npkBy,@reason", param1, param2, param3,param4);
                return new return_value { status = true, remarks = "Data pengajuan SUPEM telah disetujui" };
            }
            catch (Exception ex)
            {
                return new return_value { status = false, remarks = ex.Message };
            }
        }
        #endregion
        #region SKTA
        public async Task<List<sp_SKTA_getDataReject>> RejectGetDataSKTA(string deptHandle, string locHandle, string npk, string tgl_kerja_start, string tgl_kerja_end, string status, string tahun, string npk_input)
        {
            try
            {
                var param1 = new SqlParameter("@depthandle", deptHandle ?? "");
                var param2 = new SqlParameter("@lokasihandle", locHandle ?? "");
                var param3 = new SqlParameter("@npk", npk ?? "");
                var param4 = new SqlParameter("@tgl_kerja_start", tgl_kerja_start ?? "");
                var param5 = new SqlParameter("@tgl_kerja_end", tgl_kerja_end ?? "");
                var param6 = new SqlParameter("@tgl_apv_start", "");
                var param7 = new SqlParameter("@tgl_apv_end", "");
                var param8 = new SqlParameter("@status", status ?? "");
                var param9 = new SqlParameter("@tahun", tahun ?? "");
                var param10 = new SqlParameter("@npk_input", npk_input ?? "");
                var data = await _context.sp_SKTA_getDataReject.FromSqlRaw("Exec sp_SKTA_getDataReject @depthandle,@lokasihandle,@npk,@tgl_kerja_start,@tgl_kerja_end,@tgl_apv_start,@tgl_apv_end,@status,@tahun,@npk_input"
                                  , param1, param2, param3, param4, param5, param6, param7, param8, param9, param10).ToListAsync();

                return data;
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<sp_SKTA_getDataReject>();
            }
        }
        public async Task<List<sp_SKTA_getApproval>> GetSKTAApproval(SKTA c, string UserId, string npkLogin)
        {
            try
            {
                var param1 = new SqlParameter("@depthandle", "");
                var param2 = new SqlParameter("@lokasihandle", "");
                var param3 = new SqlParameter("@pr", c.pr ?? "");
                var param4 = new SqlParameter("@yr", c.yr ?? "");
                var param5 = new SqlParameter("@npk", c.npk ?? "");
                var param6 = new SqlParameter("@nama", c.emp_name ?? "");
                var param7 = new SqlParameter("@emp_status", c.emp_status ?? "");
                var param8 = new SqlParameter("@status", c.stats ?? "");
                var param9 = new SqlParameter("@divisi", c.ID_division.ToString() ?? "");
                var param10 = new SqlParameter("@dept", c.ID_dept.ToString() ?? "");
                var param11 = new SqlParameter("@golongan", c.ID_golongan.ToString() ?? "");
                var param12 = new SqlParameter("@npk_input", c.npk_input ?? "");
                var param13 = new SqlParameter("@date_input", c.date_input ?? "");
                var param14 = new SqlParameter("@costcenter", c.costcenter ?? "");
                var param15 = new SqlParameter("@shift", c.id_shift.ToString() ?? "");
                var param16 = new SqlParameter("@type_spl", "");
                var param17 = new SqlParameter("@UserId", UserId ?? "");
                var param18 = new SqlParameter("@npkLogin", npkLogin ?? "");
                var param19 = new SqlParameter("@tgl_mulai", c.date_startText ?? "");
                var param20 = new SqlParameter("@tgl_selesai", c.date_endText ?? "");


                var data = await _context.sp_SKTA_getApproval.FromSqlRaw("Exec sp_SKTA_getApproval @depthandle,@lokasihandle,@pr,@yr,@npk,@nama,@emp_status,@status,@divisi,@dept,@golongan,@npk_input,@date_input,@costcenter,@shift,@type_spl,@UserId,@npkLogin,@tgl_mulai,@tgl_selesai"
                                  , param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11, param12, param13, param14, param15, param16, param17, param18, param19, param20).ToListAsync();

                return data;
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<sp_SKTA_getApproval>();
            }
        }
        public async Task<return_value> RejectSKTA(string pid, string reason, string createdBy)
        {
            try
            {
                var param1 = new SqlParameter("@letter_id", pid ?? "0");
                var param2 = new SqlParameter("@reason", reason ?? "0");

                await _context.Database.ExecuteSqlRawAsync("EXEC sp_SKTA_RejectUpdateCico @letter_id,@reason", param1, param2);
                return new return_value { status = true, remarks = "Data SKTA yang terpilih berhasil di reject" };
            }
            catch (Exception ex)
            {
                return new return_value { status = false, remarks = ex.Message };
            }
        }
        public async Task<return_value> ApproveSKTA(string pid, string reason, string createdBy,string rejectComment)
        {
            try
            {
                var param1 = new SqlParameter("@pid", pid ?? "0");
                var param2 = new SqlParameter("@stats", reason ?? "0");
                var param3 = new SqlParameter("@npkBy", createdBy ?? "0");
                var param4 = new SqlParameter("@reason", rejectComment ?? "0");

                await _context.Database.ExecuteSqlRawAsync("EXEC sp_SKTA_ApproveAll @pid,@stats,@npkBy,@reason", param1, param2, param3,param4);
                return new return_value { status = true, remarks = "Data pengajuan SKTA telah disetujui" };
            }
            catch (Exception ex)
            {
                return new return_value { status = false, remarks = ex.Message };
            }
        }
        #endregion
        #region CUTI
        public async Task<List<sp_CUTI_getDataReject>> RejectGetDataCuti(string deptHandle, string locHandle, string npk, string tgl_request_start, string tgl_request_end, string status, string tahun, string npk_input)
        {
            try
            {
                var param1 = new SqlParameter("@depthandle", deptHandle ?? "");
                var param2 = new SqlParameter("@lokasihandle", locHandle ?? "");
                var param3 = new SqlParameter("@npk", npk ?? "");
                var param4 = new SqlParameter("@tgl_request_start", tgl_request_start ?? "");
                var param5 = new SqlParameter("@tgl_request_end", tgl_request_end ?? "");
                var param6 = new SqlParameter("@status", status ?? "");
                var param7 = new SqlParameter("@tahun", tahun ?? "");

                var data = await _context.sp_CUTI_getDataReject.FromSqlRaw("Exec sp_CUTI_getDataReject @depthandle,@lokasihandle,@npk,@tgl_request_start,@tgl_request_end,@status,@tahun"
                                  , param1, param2, param3, param4, param5, param6, param7).ToListAsync();

                return data;
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<sp_CUTI_getDataReject>();
            }
        }
        public async Task<return_value> RejectCuti(string pid, string reason, string createdBy)
        {
            try
            {
                var param1 = new SqlParameter("@pid", pid ?? "0");
                var param2 = new SqlParameter("@reason", reason ?? "0");
                var param3 = new SqlParameter("@createdBy", createdBy ?? "0");

                await _context.Database.ExecuteSqlRawAsync("EXEC sp_CUTI_Reject @pid,@reason,@createdBy", param1, param2, param3);
                return new return_value { status = true, remarks = "Data CUTI yang terpilih berhasil di reject" };
            }
            catch (Exception ex)
            {
                return new return_value { status = false, remarks = ex.Message };
            }
        }
        public async Task<return_value> ApproveCUTI(string pid, string reason, string createdBy,string rejectComment)
        {
            try
            {
                var param1 = new SqlParameter("@pid", pid ?? "0");
                var param2 = new SqlParameter("@stats", reason ?? "0");
                var param3 = new SqlParameter("@npkBy", createdBy ?? "0");
                var param4 = new SqlParameter("@reason", rejectComment ?? "0");

                await _context.Database.ExecuteSqlRawAsync("EXEC sp_CUTI_ApproveAll @pid,@stats,@npkBy,@reason", param1, param2, param3,param4);
                return new return_value { status = true, remarks = "Data pengajuan CUTI telah disetujui" };
            }
            catch (Exception ex)
            {
                return new return_value { status = false, remarks = ex.Message };
            }
        }
        public async Task<List<sp_CUTI_getApproval>> GetCUTIApproval(SKTA c, string UserId, string npkLogin)
        {
            try
            {
                var param1 = new SqlParameter("@depthandle", "");
                var param2 = new SqlParameter("@lokasihandle", "");
                var param3 = new SqlParameter("@pr", c.pr ?? "");
                var param4 = new SqlParameter("@yr", c.yr ?? "");
                var param5 = new SqlParameter("@npk", c.npk ?? "");
                var param6 = new SqlParameter("@nama", c.emp_name ?? "");
                var param7 = new SqlParameter("@emp_status", c.emp_status ?? "");
                var param8 = new SqlParameter("@status", c.stats ?? "");
                var param9 = new SqlParameter("@divisi", c.ID_division.ToString() ?? "");
                var param10 = new SqlParameter("@dept", c.ID_dept.ToString() ?? "");
                var param11 = new SqlParameter("@golongan", c.ID_golongan.ToString() ?? "");
                var param12 = new SqlParameter("@npk_input", c.npk_input ?? "");
                var param13 = new SqlParameter("@date_input", c.date_input ?? "");
                var param14 = new SqlParameter("@costcenter", c.costcenter ?? "");
                var param15 = new SqlParameter("@shift", c.id_shift.ToString() ?? "");
                var param16 = new SqlParameter("@type_spl", "");
                var param17 = new SqlParameter("@UserId", UserId ?? "");
                var param18 = new SqlParameter("@npkLogin", npkLogin ?? "");
                var param19 = new SqlParameter("@tgl_mulai", c.date_startText ?? "");
                var param20 = new SqlParameter("@tgl_selesai", c.date_endText ?? "");


                var data = await _context.sp_CUTI_getApproval.FromSqlRaw("Exec sp_CUTI_getApproval @depthandle,@lokasihandle,@pr,@yr,@npk,@nama,@emp_status,@status,@divisi,@dept,@golongan,@npk_input,@date_input,@costcenter,@shift,@type_spl,@UserId,@npkLogin,@tgl_mulai,@tgl_selesai"
                                  , param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11, param12, param13, param14, param15, param16, param17, param18, param19, param20).ToListAsync();

                return data;
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<sp_CUTI_getApproval>();
            }
        }
        #endregion
        #region SPL
        public async Task<List<sp_Overtime_getDataReject>> RejectGetDataSPL(string deptHandle, string locHandle, string npk, string tgl_request_start, string tgl_request_end, string status, string tahun, string npk_input)
        {
            try
            {
                var param1 = new SqlParameter("@depthandle", deptHandle ?? "");
                var param2 = new SqlParameter("@lokasihandle", locHandle ?? "");
                var param3 = new SqlParameter("@npk", npk ?? "");
                var param4 = new SqlParameter("@overtime_date_start", tgl_request_start ?? "");
                var param5 = new SqlParameter("@overtime_date_end", tgl_request_end ?? "");
                var param6 = new SqlParameter("@Id_tipe", "");
                var param7 = new SqlParameter("@status", status ?? "");
                var param8 = new SqlParameter("@periode", "");
                var param9 = new SqlParameter("@tahun", tahun ?? "");
                var param10 = new SqlParameter("@npk_input", npk_input ?? "");
                var data = await _context.sp_Overtime_getDataReject.FromSqlRaw("Exec sp_Overtime_getDataReject @depthandle,@lokasihandle,@npk,@overtime_date_start,@overtime_date_end,@Id_tipe,@status,@periode,@tahun,@npk_input"
                                  , param1, param2, param3, param4, param5, param6, param7, param8, param9, param10).ToListAsync();

                return data;
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<sp_Overtime_getDataReject>();
            }
        }
        public async Task<return_value> RejectSPL(string pid, string reason, string createdBy)
        {
            try
            {
                var param1 = new SqlParameter("@pid", pid ?? "0");
                var param2 = new SqlParameter("@reason", reason ?? "0");
                var param3 = new SqlParameter("@createdBy", createdBy ?? "0");

                await _context.Database.ExecuteSqlRawAsync("EXEC sp_SPL_Reject @pid,@reason,@createdBy", param1, param2, param3);
                return new return_value { status = true, remarks = "Data SPL yang terpilih berhasil di reject" };
            }
            catch (Exception ex)
            {
                return new return_value { status = false, remarks = ex.Message };
            }
        }
        public async Task<return_value> ApproveSPL(string pid, string reason, string createdBy,string rejectComment)
        {
            try
            {
                var param1 = new SqlParameter("@pid", pid ?? "0");
                var param2 = new SqlParameter("@stats", reason ?? "0");
                var param3 = new SqlParameter("@npkBy", createdBy ?? "0");
                var param4 = new SqlParameter("@rejectComment", rejectComment ?? "0");

                await _context.Database.ExecuteSqlRawAsync("EXEC sp_SPL_ApproveAll @pid,@stats,@npkBy,@rejectComment", param1, param2, param3,param4);
                return new return_value { status = true, remarks = "Data pengajuan SPL telah disetujui" };
            }
            catch (Exception ex)
            {
                return new return_value { status = false, remarks = ex.Message };
            }
        }
        public async Task<List<sp_SPL_getApproval>> GetSPLApproval(SKTA c, string UserId, string npkLogin)
        {
            try
            {
                var param1 = new SqlParameter("@depthandle", "");
                var param2 = new SqlParameter("@lokasihandle", "");
                var param3 = new SqlParameter("@pr", c.pr ?? "");
                var param4 = new SqlParameter("@yr", c.yr ?? "");
                var param5 = new SqlParameter("@npk", c.npk ?? "");
                var param6 = new SqlParameter("@nama", c.emp_name ?? "");
                var param7 = new SqlParameter("@emp_status", c.emp_status ?? "");
                var param8 = new SqlParameter("@status", c.stats ?? "");
                var param9 = new SqlParameter("@divisi", c.ID_division.ToString() ?? "");
                var param10 = new SqlParameter("@dept", c.ID_dept.ToString() ?? "");
                var param11 = new SqlParameter("@golongan", c.ID_golongan.ToString() ?? "");
                var param12 = new SqlParameter("@npk_input", c.npk_input ?? "");
                var param13 = new SqlParameter("@date_input", c.date_input ?? "");
                var param14 = new SqlParameter("@costcenter", c.costcenter ?? "");
                var param15 = new SqlParameter("@shift", c.id_shift.ToString() ?? "");
                var param16 = new SqlParameter("@type_spl", "");
                var param17 = new SqlParameter("@UserId", UserId ?? "");
                var param18 = new SqlParameter("@npkLogin", npkLogin ?? "");
                var param19 = new SqlParameter("@tgl_mulai", c.date_startText ?? "");
                var param20 = new SqlParameter("@tgl_selesai", c.date_endText ?? "");


                var data = await _context.sp_SPL_getApproval.FromSqlRaw("Exec sp_SPL_getApproval @depthandle,@lokasihandle,@pr,@yr,@npk,@nama,@emp_status,@status,@divisi,@dept,@golongan,@npk_input,@date_input,@costcenter,@shift,@type_spl,@UserId,@npkLogin,@tgl_mulai,@tgl_selesai"
                                  , param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11, param12, param13, param14, param15, param16, param17, param18, param19, param20).ToListAsync();

                return data;
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<sp_SPL_getApproval>();
            }
        }
        public async Task<List<Employee_Working_Schedule_Get>> GetWorkingSchedule(string npk, DateTime clock_in, DateTime clock_out, string dws_type)
        {
            try
            {
                var param1 = new SqlParameter("@npk", npk ?? "");
                var param2 = new SqlParameter("@clock_in", clock_in.ToString("yyyy-MM-dd") ?? "");
                var param3 = new SqlParameter("@clock_out", clock_out.ToString("yyyy-MM-dd") ?? "");
                var param4 = new SqlParameter("@dws_type", dws_type ?? "");

                var data = await _context.Employee_Working_Schedule_Get.FromSqlRaw("Exec Employee_Working_Schedule_Get @npk,@clock_in,@clock_out,@dws_type"
                                  , param1, param2, param3, param4).ToListAsync();

                return data;
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<Employee_Working_Schedule_Get>();
            }
        }
        public async Task<List<SUPEM_GetBy>> GetByID(string npk, DateTime date_start, DateTime date_end, string stats)
        {
            try
            {
                var param1 = new SqlParameter("@npk", npk ?? "");
                var param2 = new SqlParameter("@start_date", date_start);
                var param3 = new SqlParameter("@end_date", date_end);
                var param4 = new SqlParameter("@stats", stats ?? "");

                var data = await _context.SUPEM_GetBy.FromSqlRaw("Exec SUPEM_GetBy @npk,@start_date,@end_date,@stats"
                                  , param1, param2, param3, param4).ToListAsync();

                return data;
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<SUPEM_GetBy>();
            }
        }
        public async Task<List<CICO_Stats_Get>> GetStats(string cico_code)
        {
            try
            {
                var param1 = new SqlParameter("@cico_code", cico_code ?? "");

                var data = await _context.CICO_Stats_Get.FromSqlRaw("Exec CICO_Stats_Get @cico_code"
                                  , param1).ToListAsync();

                return data;
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<CICO_Stats_Get>();
            }
        }
        public async Task<List<CICO_GetPriority>> GetCicoPriority(string npk, string start_date, string end_date)
        {
            try
            {
                var param1 = new SqlParameter("@npk", npk ?? "");
                var param2 = new SqlParameter("@start_date", start_date ?? "");
                var param3 = new SqlParameter("@end_date", end_date ?? "");

                var data = await _context.CICO_GetPriority.FromSqlRaw("Exec CICO_GetPriority @npk,@start_date,@end_date"
                                  , param1, param2, param3).ToListAsync();

                return data;
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<CICO_GetPriority>();
            }
        }
        public async Task<List<SUPEM_CheckLeave>> CheckLeave(string npk, string start_date, string end_date)
        {
            try
            {
                var param1 = new SqlParameter("@npk", npk ?? "");
                var param2 = new SqlParameter("@startDate", start_date ?? "");
                var param3 = new SqlParameter("@endDate", end_date ?? "");

                var data = await _context.SUPEM_CheckLeave.FromSqlRaw("Exec CICO_GetPriority @npk,@startDate,@endDate"
                                  , param1, param2, param3).ToListAsync();

                return data;
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<SUPEM_CheckLeave>();
            }
        }
        public async Task<List<Employee_ValidateSKTAByNPK>> GetValidateSKTAByNPK(string npk, string UserId, string DeptId, string LocationId)
        {
            try
            {
                var param1 = new SqlParameter("@npk", npk ?? "");
                var param2 = new SqlParameter("@UserId", UserId ?? "");
                var param3 = new SqlParameter("@DeptId", DeptId ?? "");
                var param4 = new SqlParameter("@LocationId", LocationId ?? "");

                var data = await _context.Employee_ValidateSKTAByNPK.FromSqlRaw("Exec Employee_ValidateSKTAByNPK @npk,@UserId,@DeptId,@LocationId"
                                  , param1, param2, param3, param4).ToListAsync();

                return data;
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<Employee_ValidateSKTAByNPK>();
            }
        }

        public async Task<List<Sp_Employee_GetAllAtasan_ByDeptHead>> GetAtasanDH(string npk)
        {
            try
            {
                var org = _context.viewEmployeeOrganizationAll.Where(s => s.npk == npk).FirstOrDefault();
                if (org != null)
                {
                    var param1 = new SqlParameter("@DeptId", org.ID_dept ?? 0);
                    var data = await _context.Sp_Employee_GetAllAtasan_ByDeptHead.FromSqlRaw("Exec Sp_Employee_GetAllAtasan_ByDeptHead @DeptId"
                                      , param1).ToListAsync();
                    return data;
                }
                else
                {
                    return new List<Sp_Employee_GetAllAtasan_ByDeptHead>();
                }
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<Sp_Employee_GetAllAtasan_ByDeptHead>();
            }
        }
        public async Task<List<Sp_Employee_GetAllAtasan_ByDeptHead>> GetAtasanBasedOrganization(string npk)
        {
            try
            {
                List<Sp_Employee_GetAllAtasan_ByDeptHead> emp = new List<Sp_Employee_GetAllAtasan_ByDeptHead>();
                var org = _context.viewEmployeeOrganizationAll.Where(s => s.npk == npk).FirstOrDefault();
                if (org != null)
                {
                    if (org.level_idx_no == 1)
                    {
                        var param1 = new SqlParameter("@npk", org.npk ?? "");
                        emp = await _context.Sp_Employee_GetAllAtasan_ByDeptHead.FromSqlRaw("Exec Sp_GetAtasanForDirectorateHead @npk"
                                          , param1).ToListAsync();
                    }
                    else if (org.level_idx_no == 2)
                    {
                        var param1 = new SqlParameter("@DirHead", org.directorateID ?? 0);
                        emp = await _context.Sp_Employee_GetAllAtasan_ByDeptHead.FromSqlRaw("Exec Sp_Employee_GetAllAtasan_ByDirHead @DirHead"
                                          , param1).ToListAsync();
                        if (emp.Count() == 0)
                        {
                            var param2 = new SqlParameter("@DivId", org.directorateID ?? 0);
                            emp = await _context.Sp_Employee_GetAllAtasan_ByDeptHead.FromSqlRaw("Exec Sp_Employee_GetAllAtasan_ByDivHead @DivId"
                                              , param2).ToListAsync();
                        }
                    }
                    else if (org.level_idx_no == 3)
                    {
                        var param1 = new SqlParameter("@DivId", org.npk ?? "");
                        emp = await _context.Sp_Employee_GetAllAtasan_ByDeptHead.FromSqlRaw("Exec Sp_Employee_GetAllAtasan_ByDivHead @DivId"
                                              , param1).ToListAsync();
                    }
                    else
                    {
                        var param1 = new SqlParameter("@DeptId", org.ID_dept ?? 0);
                        emp = await _context.Sp_Employee_GetAllAtasan_ByDeptHead.FromSqlRaw("Exec Sp_Employee_GetAllAtasan_ByDeptHead @DeptId"
                                         , param1).ToListAsync();

                    }

                    return emp;
                }
                else
                {
                    return new List<Sp_Employee_GetAllAtasan_ByDeptHead>();
                }
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<Sp_Employee_GetAllAtasan_ByDeptHead>();
            }
        }
        public async Task<List<CICO_Get>> GetCICO(string npk, string working_date)
        {
            try
            {
                var param1 = new SqlParameter("@npk", npk ?? "");
                var param2 = new SqlParameter("@working_date", working_date ?? "");
                var data = await _context.CICO_Get.FromSqlRaw("Exec CICO_Get @npk,@working_date"
                                  , param1, param2).ToListAsync();
                return data;
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<CICO_Get>();
            }
        }


        #endregion


    }


}