using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Interfaces.PDK;
using HRIS.Models.PDK;
using HRIS.Models.PM3;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Repositories.PDK
{
    public class FamilyRepository : IFamilyRepository
    {
        private readonly PM3Context _context;

        public FamilyRepository(PM3Context context)
        {
            _context = context;
        }

        public async Task<FamilyModel> GetFamilyDataAsync(string npk)
        {
            var familyModel = new FamilyModel();
            var familyMembers = new List<FamilyMemberModel>();

            try
            {
                var familyData = await _context.Set<SP_PDK_Family_GetDataEmployee>()
                    .FromSqlRaw("EXEC Sp_PDK_Family_GetDataEmployee @npk={0}", npk)
                    .AsNoTracking()
                    .ToListAsync();

                if (familyData.Any())
                {
                    var firstData = familyData.FirstOrDefault();
                    familyModel.NPK = firstData.npk;
                    familyModel.Nama = firstData.name;
                    familyModel.StatusPernikahan = firstData.status_marriage;
                    familyModel.TanggalPernikahan = firstData.tanggal_nikah;
                    familyModel.StatusKeluarga = firstData.status_family;
                }

                var familyMemberData = await _context.Set<Sp_PDK_Family_GetDataFamily>()
                    .FromSqlRaw("EXEC Sp_PDK_Family_GetDataFamily @npk={0}", npk)
                    .AsNoTracking()
                    .ToListAsync();

                familyMembers = familyMemberData.Select(member => new FamilyMemberModel
                {
                    Npk = member.npk,
                    Hubungan = member.relationship,
                    Nama = member.name,
                    JenisKelamin = member.genderNm,
                    TempatTanggalLahir = member.tempat_lahir,
                    Pekerjaan = member.jobNm,
                    NoIdentitas = member.no_ktp,
                    NoGardaMedika = member.no_garda_medika,
                    NoBPJS = member.no_bpjs,
                    StatusBPJS = member.status_bpjs,
                    ProgressStatusBPJS = member.Progress_BPJS
                }).ToList();

                familyModel.FamilyMembers = familyMembers;
            }
            catch (Exception ex)
            {
                // Handle exceptions or log errors
                Console.WriteLine($"Error fetching family data: {ex.Message}");
                throw;
            }

            return familyModel;
        }
    }
}
