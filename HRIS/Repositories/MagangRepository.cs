﻿using System.Collections.Generic;
using System.Linq;
using HRIS.Controllers.Magang;
using HRIS.Interfaces;
using HRIS.Models.General;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
namespace HRIS.Repositories
{
    public class MagangRepository : IMagangRepository
    {
        private readonly PM3Context _context;
        public MagangRepository(PM3Context context)
        {
            _context = context;
        }
        public async Task<List<sp_Pemagangan_getData>> GetDataHiring(clsfil f)
        {
            try
            {
                var param1 = new SqlParameter("@npk", f.npk ?? "");
                var param2 = new SqlParameter("@name", f.name ?? "");
                var param3 = new SqlParameter("@id_div", f.id_div ?? "");
                var param4 = new SqlParameter("@id_dept", f.id_dept ?? "");
                var param5 = new SqlParameter("@section", f.section ?? "");
                var param6 = new SqlParameter("@id_lok", f.id_lok ?? "");
                var param7 = new SqlParameter("@start", f.start_ ?? "");
                var param8 = new SqlParameter("@end", f.end_ ?? "");
                var param9 = new SqlParameter("@tglUpload", f.tglUpload ?? "");
                var param10 = new SqlParameter("@depthandle", f.depthandle ?? "");
                var param11 = new SqlParameter("@lokasihandle", f.lokasihandle ?? "");
                var data = await _context.sp_Pemagangan_getData.FromSqlRaw("Exec sp_Pemagangan_getData @npk,@name,@id_div,@id_dept,@section,@id_lok,@start,@end,@tglUpload,@depthandle,@lokasihandle"
                                  , param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11).ToListAsync();

                return data;
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<sp_Pemagangan_getData>();
            }
        }
        public async Task<List<sp_Pemagangan_getAttendlist>> GetDataAttendance(clsfil f)
        {
            try
            {
                var param1 = new SqlParameter("@npk", f.npk ?? "");
                var param2 = new SqlParameter("@nama", f.name ?? "");
                var param3 = new SqlParameter("@ID_division", f.id_div ?? "");
                var param4 = new SqlParameter("@id_dept", f.id_dept ?? "");
                var param5 = new SqlParameter("@id_shift", f.id_shift ?? "");
                var param6 = new SqlParameter("@tglmulai", f.tglmulai ?? "");
                var param7 = new SqlParameter("@tglend", f.tglend ?? "");
                var param8 = new SqlParameter("@id_user", f.id_user ?? "");
                var param9 = new SqlParameter("@npkLogin", f.NpkLogin ?? "");
              
                var data = await _context.sp_Pemagangan_getAttendlist.FromSqlRaw("Exec sp_Pemagangan_getAttendlist @npk,@nama,@ID_division,@id_dept,@id_shift,@tglmulai,@tglend,@id_user,@npkLogin"
                                  , param1, param2, param3, param4, param5, param6, param7, param8, param9).ToListAsync();

                return data;
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<sp_Pemagangan_getAttendlist>();
            }
        }
        
        public async Task<List<sp_Pemagangan_ValidasiUploadHiring>> ValidasiUHiring(clsfil f)
        {
            try
            {
                var param1 = new SqlParameter("@npk", f.npk ?? "");
                var param2 = new SqlParameter("@nama", f.name ?? "");
                var param3 = new SqlParameter("@dir", f.dir ?? "");
                var param4 = new SqlParameter("@div", f.id_div ?? "");
                var param5 = new SqlParameter("@dept", f.id_dept ?? "");
                var param6 = new SqlParameter("@sec", f.section ?? "");
                var param7 = new SqlParameter("@subsec", f.subsec ?? "");
                var param8 = new SqlParameter("@lok", f.id_lok ?? "");
                var param9 = new SqlParameter("@start", f.start_ ?? "");
                var param10 = new SqlParameter("@end", f.end_ ?? "");
                var param11 = new SqlParameter("@gender", f.gender ?? "");
                var param12 = new SqlParameter("@email", f.email ?? "");
                var data = await _context.sp_Pemagangan_ValidasiUploadHiring.FromSqlRaw("Exec sp_Pemagangan_ValidasiUploadHiring @npk,@nama,@dir,@div,@dept,@sec,@subsec,@lok,@start,@end,@gender,@email"
                                  , param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11,param12).ToListAsync();

                return data;
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<sp_Pemagangan_ValidasiUploadHiring>();
            }
        }
        public async Task<return_value> UploadHiring(uHiring f)
        {
            try
            {
                var param1 = new SqlParameter("@npk_magang", f.NPK ?? "0");
                var param2 = new SqlParameter("@name", f.NAMA ?? "0");
                var param3 = new SqlParameter("@directorateID", f.id_dir ?? "0");
                var param4 = new SqlParameter("@ID_division", f.id_div ?? "0");
                var param5 = new SqlParameter("@ID_dept", f.id_dept ?? "0");
                var param6 = new SqlParameter("@section_id", f.id_sec ?? "0");
                var param7 = new SqlParameter("@subsection_id", f.id_sub ?? "0");
                var param8 = new SqlParameter("@ID_lokasi", f.id_lok ?? "0");
                var param9 = new SqlParameter("@directorate", f.DIRECTORATE ?? "0");
                var param10 = new SqlParameter("@division", f.DIVISI ?? "0");
                var param11 = new SqlParameter("@department", f.DEPARTEMENT ?? "0");
                var param12 = new SqlParameter("@section", f.SECTION ?? "0");
                var param13 = new SqlParameter("@subSection", f.SUB ?? "0");
                var param14 = new SqlParameter("@lokasiKerja", f.LOKASI ?? "0");
                var param15 = new SqlParameter("@work_date", f.START ?? "0");
                var param16 = new SqlParameter("@end_work_date", f.END ?? "0");
                var param17 = new SqlParameter("@gender", f.GENDER ?? "0");
                var param18 = new SqlParameter("@email", f.EMAIL ?? "0");
                var param19 = new SqlParameter("@created_by", f.npkLogin ?? "0");
                await _context.Database.ExecuteSqlRawAsync("EXEC sp_Pemagangan_InsertMs @npk_magang,@name,@directorateID,@ID_division,@ID_dept,@section_id,@subsection_id,@ID_lokasi,@directorate,@division,@department,@section,@subSection,@lokasiKerja,@work_date,@end_work_date,@gender,@email,@created_by", param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11, param12, param13, param14, param15, param16, param17, param18, param19);


                return new return_value { status = true, remarks = "Upload Pemagangan Berhasil Disubmit" };
            }
            catch (Exception ex)
            {
                return new return_value { status = false, remarks = ex.Message };
            }
        }
        public async Task<return_value> UploadSch(uWorkSch f)
        {
            try
            {
                var param1 = new SqlParameter("@npk", f.npk ?? "0");
                var param2 = new SqlParameter("@tgl", f.tglKerja ?? "0");
                var param3 = new SqlParameter("@dws", f.tglKerjaEnd ?? "0");
                var param4 = new SqlParameter("@type", f.shift ?? "0");
                var param5 = new SqlParameter("@shift", "");
                var param6 = new SqlParameter("@ci",  "");
                var param7 = new SqlParameter("@co",  "");
               
                await _context.Database.ExecuteSqlRawAsync("EXEC sp_pemagangan_InserSchedlu @npk,@tgl,@dws,@type,@shift,@ci,@co", param1, param2, param3, param4, param5, param6, param7);


                return new return_value { status = true, remarks = "Upload Work Schedule Pemagangan Berhasil Disubmit" };
            }
            catch (Exception ex)
            {
                return new return_value { status = false, remarks = ex.Message };
            }
        }
        public async Task<List<sp_Pemagangan_getDataWorkSchdule>> GetDataWorkSch(clsWorkSch f)
        {
            try
            {
                var param1 = new SqlParameter("@npk", f.npk ?? "");
                var param2 = new SqlParameter("@nama", f.nama ?? "");
                var param3 = new SqlParameter("@ID_division", f.ID_division ?? "");
                var param4 = new SqlParameter("@id_dept", f.id_dept ?? "");
                var param5 = new SqlParameter("@id_shift",f.id_shift  ?? "");
                var param6 = new SqlParameter("@tglmulai", f.tglmulai ?? "");
                var param7 = new SqlParameter("@tglend", f.tglend ?? "");
                var param8 = new SqlParameter("@id_user", f.id_user ?? "");
                var param9 = new SqlParameter("@npkLogin", f.npkLogin ?? "");
               
                var data = await _context.sp_Pemagangan_getDataWorkSchdule.FromSqlRaw("Exec sp_Pemagangan_getDataWorkSchdule @npk,@nama,@ID_division,@id_dept,@id_shift,@tglmulai,@tglend,@id_user,@npkLogin"
                                  , param1, param2, param3, param4, param5, param6, param7, param8, param9).ToListAsync();

                return data;
            }
            catch (System.Exception ex)
            {
                // Handle exception or log it
                return new List<sp_Pemagangan_getDataWorkSchdule>();
            }
        }
    }
    public class clsfil
    {
        public string? npk { get; set; }
        public string? name { get; set; }
        public string? id_div { get; set; }
        public string? id_dept { get; set; }
        public string? section { get; set; }
        public string? id_lok { get; set; }
        public string? start_ { get; set; }
        public string? end_ { get; set; }
        public string? tglUpload { get; set; }
        public string? depthandle { get; set; }
        public string? lokasihandle { get; set; }
        public string? dir { get; set; }
        public string? gender { get; set; }
        public string? subsec { get; set; }
        public string? email { get; set; }
        public string? id_shift { get; set; }
        public string? tglmulai { get; set; }
        public string? tglend { get; set; }
        public string? id_user { get; set; }
        public string? NpkLogin { get; set; }
    }
    public class clsWorkSch
    {
        public string? npk { get; set; }
        public string? nama { get; set; }
        public string? ID_division { get; set; }
        public string? id_dept { get; set; }
        public string? id_shift { get; set; }
        public string? tglmulai { get; set; }
        public string? tglend { get; set; }
        public string? id_user { get; set; }
        public string? npkLogin { get; set; }
    }

}
