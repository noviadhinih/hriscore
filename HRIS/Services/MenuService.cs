using System;
using System.Collections.Generic;
using System.Linq;
using HRIS.Interfaces;
using HRIS.Models.PM3;

namespace HRIS.Services
{
    public class MenuService
    {
        private readonly IMenuRepository _menuRepository;

        public MenuService(IMenuRepository menuRepository)
        {
            _menuRepository = menuRepository;
        }
        public string RecursiveMenu(string id_user)
        {
            var str_menuResult = string.Empty;

            try
            {
                // Mengambil semua menu berdasarkan user
                var menuItems = _menuRepository.GetMenu(id_user)
                    .OrderBy(s => s.Menuid)
                    .ThenBy(s => s.ParentMenuID)
                    .ThenBy(s => s.urutan)
                    .ToList();

                // Membuat menu "Personal" sebagai parent utama
                str_menuResult += "<li class='nav-item'>";
                str_menuResult += "<a class=\"nav-link collapsed\" href=\"#\" data-toggle=\"collapse\" data-target=\"#collapsePersonal\" aria-expanded=\"false\" aria-controls=\"collapsePersonal\">";
                str_menuResult += "<i class=\"fas fa-fw fa-folder\"></i>";
                str_menuResult += "<span>Main Menu</span>";
                // str_menuResult += "<i class=\"fas fa-chevron-right float-right collapse-icon\"></i>";
                str_menuResult += "</a>";

                // Bagian collapse untuk Personal
                str_menuResult += "<div id=\"collapsePersonal\" class=\"collapse\" aria-labelledby=\"headingPersonal\" data-parent=\"#accordionSidebar\">";
                str_menuResult += "<div class=\"bg-white py-2 collapse-inner rounded\">";

                // Mendapatkan submenu untuk Kehadiran dan Tunjangan (ParentMenuID == 0 dan nama menu Kehadiran/Tunjangan)
                var personal= menuItems.Where(s=>s.id_dept=="0").ToList();
                var personalSubmenus = personal.Where(m => m.JenisMenu == "Personal" && m.ParentMenuID == 0).ToList();

                // Iterasi untuk Kehadiran dan Tunjangan
                foreach (var menuItem in personalSubmenus)
                {
                    var submenuItems = personal.Where(m => m.ParentMenuID == menuItem.Menuid).ToList();
                    if (submenuItems.Count() == 0)
                    {
                        str_menuResult += $"<a class=\"collapse-item\" href=\"{menuItem.link}\">{menuItem.menu}</a>";
                    }
                    else
                    {
                        // Jika menuItem adalah Kehadiran atau Tunjangan, buat struktur menu dengan BuildMenuNew
                        str_menuResult += $"<a class=\"collapse-item\" href=\"{menuItem.link}\" data-toggle=\"collapse\" data-target=\"#collapse{menuItem.Menuid}\" aria-expanded=\"false\" aria-controls=\"collapse{menuItem.Menuid}\">{menuItem.menu}</a>";


                        str_menuResult += $"<div id=\"collapse{menuItem.Menuid}\" class=\"collapse\" aria-labelledby=\"heading{menuItem.Menuid}\" data-parent=\"#collapseApv\">";
                        str_menuResult += "<div class=\"bg-white py-2 collapse-inner rounded\">";
                        foreach (var subMenuItem in submenuItems)
                        {
                            if (subMenuItem.jmlink == 0)
                            {
                                str_menuResult += $"<a class=\"collapse-item\" href=\"{subMenuItem.link}\">{subMenuItem.menu}</a>";
                            }
                            else
                            {
                                str_menuResult += $"<a class=\"collapse-item\" href=\"{subMenuItem.link}\" data-toggle=\"collapse\" data-target=\"#nested{subMenuItem.id}\" aria-expanded=\"false\" aria-controls=\"#nested{subMenuItem.id}\"> {subMenuItem.menu}";
                                str_menuResult += " <i class=\"fas fa-chevron-right float-right collapse-icon\"></i>";
                                str_menuResult += " </a>";
                                str_menuResult += $"<div id=\"nested{subMenuItem.id}\" class=\"collapse\">";
                                str_menuResult += "<ul class=\"nested-list\">";
                                var ch = personal.Where(m => m.ParentMenuID == subMenuItem.Menuid).ToList();
                                foreach (var c in ch)
                                {
                                    str_menuResult += $"<li><a class=\"nested-item\" href=\"{c.link}\">{c.menu}</a></li>";
                                }
                                str_menuResult += "</ul>";
                                str_menuResult += " </div>";
                            }
                        }

                        str_menuResult += "</div>"; // Tutup .collapse-inner untuk submenu
                        str_menuResult += "</div>"; // Tutup collapse div untuk Kehadiran/Tunjangan
                    }

                }

                str_menuResult += "</div>"; // Tutup .collapse-inner untuk Personal
                str_menuResult += "</div>"; // Tutup #collapsePersonal
                str_menuResult += "</li>"; // Tutup li.nav-item
                #region ADMIN
                var parenAdmin = menuItems.Where(m => m.id_dept != "0").ToList();
                var admin = parenAdmin.GroupBy(s => new { s.id_dept, s.departement }).Select(m => new { id_dept = m.Key.id_dept, departement = m.Key.departement }).ToList();
                foreach (var ad in admin)
                {
                    str_menuResult += "<li class='nav-item'>";
                    str_menuResult += $"<a class=\"nav-link collapsed\" href=\"#\" data-toggle=\"collapse\" data-target=\"#collapseAd{ad.id_dept}\" aria-expanded=\"false\" aria-controls=\"collapseAd{ad.id_dept}\">";
                    str_menuResult += "<i class=\"fas fa-fw fa-folder\"></i>";
                    str_menuResult += $"<span>{ad.departement}</span>";
                    str_menuResult += "</a>";
                    // Bagian collapse untuk Admin
                    str_menuResult += $"<div id=\"collapseAd{ad.id_dept}\" class=\"collapse\" aria-labelledby=\"headingAd{ad.id_dept}\" data-parent=\"#accordionSidebar\">";
                    str_menuResult += "<div class=\"bg-white py-2 collapse-inner rounded\">";
                    var adminSubmenus = parenAdmin.Where(m => m.id_dept == ad.id_dept && m.ParentMenuID == 0).ToList();
                    foreach (var menuItem in adminSubmenus)
                    {
                        var submenuItems = parenAdmin.Where(m => m.ParentMenuID == menuItem.Menuid).ToList();
                        if (submenuItems.Count() == 0)
                        {
                            str_menuResult += $"<a class=\"collapse-item\" href=\"{menuItem.link}\">{menuItem.menu}</a>";
                        }
                        else
                        {
                            // Jika menuItem adalah Kehadiran atau Tunjangan, buat struktur menu dengan BuildMenuNew
                            str_menuResult += $"<a class=\"collapse-item\" href=\"{menuItem.link}\" data-toggle=\"collapse\" data-target=\"#collapse{menuItem.Menuid}\" aria-expanded=\"false\" aria-controls=\"collapse{menuItem.Menuid}\">{menuItem.menu}</a>";


                            str_menuResult += $"<div id=\"collapse{menuItem.Menuid}\" class=\"collapse\" aria-labelledby=\"heading{menuItem.Menuid}\" data-parent=\"#collapseApv\">";
                            str_menuResult += "<div class=\"bg-white py-2 collapse-inner rounded\">";
                            foreach (var subMenuItem in submenuItems)
                            {
                                if (subMenuItem.jmlink == 0)
                                {
                                    str_menuResult += $"<a class=\"collapse-item\" href=\"{subMenuItem.link}\">{subMenuItem.menu}</a>";
                                }
                                else
                                {
                                    str_menuResult += $"<a class=\"collapse-item\" href=\"{subMenuItem.link}\" data-toggle=\"collapse\" data-target=\"#nested{subMenuItem.id}\" aria-expanded=\"false\" aria-controls=\"#nested{subMenuItem.id}\"> {subMenuItem.menu}";
                                    str_menuResult += " <i class=\"fas fa-chevron-right float-right collapse-icon\"></i>";
                                    str_menuResult += " </a>";
                                    str_menuResult += $"<div id=\"nested{subMenuItem.id}\" class=\"collapse\">";
                                    str_menuResult += "<ul class=\"nested-list\">";
                                    var ch = parenAdmin.Where(m => m.ParentMenuID == subMenuItem.Menuid).ToList();
                                    foreach (var c in ch)
                                    {
                                        str_menuResult += $"<li><a class=\"nested-item\" href=\"{c.link}\">{c.menu}</a></li>";
                                    }
                                    str_menuResult += "</ul>";
                                    str_menuResult += " </div>";
                                }
                            }

                            str_menuResult += "</div>"; // Tutup .collapse-inner untuk submenu
                            str_menuResult += "</div>"; // Tutup collapse div untuk Kehadiran/Tunjangan
                        }
                    }
                    str_menuResult += "</div>"; // Tutup .collapse-inner untuk Apv
                    str_menuResult += "</div>"; // Tutup #collapseApv
                    str_menuResult += "</li>"; // Tutup li.nav-item
                }

                #endregion
                //APPROVAL
                #region Approval
                str_menuResult += "<li class='nav-item'>";
                str_menuResult += "<a class=\"nav-link collapsed\" href=\"#\" data-toggle=\"collapse\" data-target=\"#collapseApv\" aria-expanded=\"false\" aria-controls=\"collapsePersonal\">";
                str_menuResult += "<i class=\"fas fa-fw fa-folder\"></i>";
                str_menuResult += "<span>Approval</span>";
                //  str_menuResult += "<i class=\"fas fa-chevron-right float-right collapse-icon\"></i>";
                str_menuResult += "</a>";

                // Bagian collapse untuk Apv
                str_menuResult += "<div id=\"collapseApv\" class=\"collapse\" aria-labelledby=\"headingApv\" data-parent=\"#accordionSidebar\">";
                str_menuResult += "<div class=\"bg-white py-2 collapse-inner rounded\">";

                // Mendapatkan submenu untuk Kehadiran dan Tunjangan (ParentMenuID == 0 dan nama menu Kehadiran/Tunjangan)
                var apvSubmenus = menuItems.Where(m => m.isApv == 1 && m.ParentMenuID == 0).ToList();

                foreach (var menuItem in apvSubmenus)
                {
                    var submenuItems = menuItems.Where(m => m.ParentMenuID == menuItem.Menuid).ToList();
                    if (submenuItems.Count() == 0)
                    {
                        str_menuResult += $"<a class=\"collapse-item\" href=\"{menuItem.link}\">{menuItem.menu}</a>";
                    }
                    else
                    {
                        // Jika menuItem adalah Kehadiran atau Tunjangan, buat struktur menu dengan BuildMenuNew
                        str_menuResult += $"<a class=\"collapse-item\" href=\"{menuItem.link}\" data-toggle=\"collapse\" data-target=\"#collapse{menuItem.Menuid}\" aria-expanded=\"false\" aria-controls=\"collapse{menuItem.Menuid}\">{menuItem.menu}</a>";


                        str_menuResult += $"<div id=\"collapse{menuItem.Menuid}\" class=\"collapse\" aria-labelledby=\"heading{menuItem.Menuid}\" data-parent=\"#collapseApv\">";
                        str_menuResult += "<div class=\"bg-white py-2 collapse-inner rounded\">";
                        foreach (var subMenuItem in submenuItems)
                        {
                            if (subMenuItem.jmlink == 0)
                            {
                                str_menuResult += $"<a class=\"collapse-item\" href=\"{subMenuItem.link}\">{subMenuItem.menu}</a>";
                            }
                            else
                            {
                                str_menuResult += $"<a class=\"collapse-item\" href=\"{subMenuItem.link}\" data-toggle=\"collapse\" data-target=\"#nested{subMenuItem.id}\" aria-expanded=\"false\" aria-controls=\"#nested{subMenuItem.id}\"> {subMenuItem.menu}";
                                str_menuResult += " <i class=\"fas fa-chevron-right float-right collapse-icon\"></i>";
                                str_menuResult += " </a>";
                                str_menuResult += $"<div id=\"nested{subMenuItem.id}\" class=\"collapse\">";
                                str_menuResult += "<ul class=\"nested-list\">";
                                var ch = menuItems.Where(m => m.ParentMenuID == subMenuItem.Menuid).ToList();
                                foreach (var c in ch)
                                {
                                    str_menuResult += $"<li><a class=\"nested-item\" href=\"{c.link}\">{c.menu}</a></li>";
                                }
                                str_menuResult += "</ul>";
                                str_menuResult += " </div>";
                            }
                        }

                        str_menuResult += "</div>"; // Tutup .collapse-inner untuk submenu
                        str_menuResult += "</div>"; // Tutup collapse div untuk Kehadiran/Tunjangan
                    }

                }

                str_menuResult += "</div>"; // Tutup .collapse-inner untuk Apv
                str_menuResult += "</div>"; // Tutup #collapseApv
                str_menuResult += "</li>"; // Tutup li.nav-item

                #endregion


                return str_menuResult;
            }
            catch (Exception ex)
            {
                return "Failed to load menu: " + ex.ToString();
            }
        }




        public string RecursiveMenuOld(string id_user)
        {
            var str_menuResult = string.Empty;

            try
            {
                var menuItems = _menuRepository.GetMenu(id_user)
                    .OrderBy(s => s.Menuid)
                    .ThenBy(s => s.ParentMenuID)
                    .ThenBy(s => s.urutan)
                    .ToList();

                var header = menuItems.Where(m => m.ParentMenuID == 0).ToList();


                foreach (var menuItem in header)
                {
                    str_menuResult += BuildMenuNew(menuItems, menuItem);
                }
                return str_menuResult;
            }
            catch (Exception ex)
            {
                return "Failed to load menu: " + ex.ToString();
            }
        }

        private string BuildMenuNew(List<sp_getMenu> menuItems, sp_getMenu menuItem)
        {
            var str_menuResult = "<li class='nav-item'>";

            if (menuItem.jmlink == 0)
            {
                str_menuResult += $"<a class=\"nav-link\" href=\"{menuItem.link}\" data-target=\"#{menuItem.id}\" aria-expanded=\"true\" aria-controls=\"collapsePages\">";
                str_menuResult += "<i class=\"fas fa-fw fa-folder\"></i>";
                str_menuResult += $"<span>{menuItem.menu}</span>";
                str_menuResult += "</a>";
            }
            else
            {
                var submenuItems = menuItems.Where(m => m.ParentMenuID == menuItem.Menuid).ToList();
                if (submenuItems.Any())
                {
                    str_menuResult += $"<a class=\"nav-link\" href=\"{menuItem.link}\" data-toggle=\"collapse\" data-target=\"#collapsePages{menuItem.id}\" aria-expanded=\"true\" aria-controls=\"{menuItem.id}\">";
                    str_menuResult += "<i class=\"fas fa-fw fa-folder\"></i>";
                    str_menuResult += $"<span>{menuItem.menu}</span>";
                    str_menuResult += "</a>";
                    str_menuResult += $"<div id=\"collapsePages{menuItem.id}\" class=\"collapse\" aria-labelledby=\"headingPages\" data-parent=\"#accordionSidebar\">";
                    str_menuResult += "<div class=\"bg-white py-2 collapse-inner rounded\">";
                    foreach (var subMenuItem in submenuItems)
                    {
                        if (subMenuItem.jmlink == 0)
                        {
                            str_menuResult += $"<a class=\"collapse-item\" href=\"{subMenuItem.link}\">{subMenuItem.menu}</a>";
                        }
                        else
                        {
                            str_menuResult += $"<a class=\"collapse-item\" href=\"{subMenuItem.link}\" data-toggle=\"collapse\" data-target=\"#nested{subMenuItem.id}\" aria-expanded=\"false\" aria-controls=\"#nested{subMenuItem.id}\"> {subMenuItem.menu}";
                            str_menuResult += " <i class=\"fas fa-chevron-right float-right collapse-icon\"></i>";
                            str_menuResult += " </a>";
                            str_menuResult += $"<div id=\"nested{subMenuItem.id}\" class=\"collapse\">";
                            str_menuResult += "<ul class=\"nested-list\">";
                            var ch = menuItems.Where(m => m.ParentMenuID == subMenuItem.Menuid).ToList();
                            foreach (var c in ch)
                            {
                                str_menuResult += $"<li><a class=\"nested-item\" href=\"{c.link}\">{c.menu}</a></li>";
                            }
                            str_menuResult += "</ul>";
                            str_menuResult += " </div>";
                        }
                    }
                    str_menuResult += "</div>";
                    str_menuResult += "</div>";
                }
            }

            str_menuResult += "</li>";
            return str_menuResult;
        }
    }
}
