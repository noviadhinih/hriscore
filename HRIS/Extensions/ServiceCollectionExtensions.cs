using Microsoft.Extensions.DependencyInjection;
using HRIS.Repositories;
using HRIS.Repositories.PDK;
using HRIS.Interfaces;
using HRIS.Interfaces.PDK;
using HRIS.Services;
using HRIS.Interfaces.TMS;
using HRIS.Repositories.TMS;
namespace HRIS.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IFamilyRepository, FamilyRepository>();
            services.AddScoped<IDeptRepository, DeptRepository>();
            services.AddScoped<IUsersRepository, UsersRepository>();
            services.AddScoped<IViewDwsRepository, ViewDwsRepository>();
            services.AddScoped<IMenuRepository, MenuRepository>();
            services.AddScoped<MenuService>();
            services.AddScoped<IMasterShiftHeaderRepository, MasterShiftHeaderRepository>();
            services.AddScoped<ILocationRepository, LocationRepository>();
            services.AddScoped<TrLogErrorRepository>();
            services.AddScoped<ITMSRepository,TMSRepository>();
            services.AddScoped<IMagangRepository,MagangRepository>();
            services.AddScoped<IOTManagementRepository,OTManagementRepository>();
            return services;
        }
    }
}