using Microsoft.AspNetCore.Mvc.Razor;
using System.Collections.Generic;

namespace HRIS.Infrastructure
{
    public class CustomViewLocationExpander : IViewLocationExpander
    {
        public void PopulateValues(ViewLocationExpanderContext context)
        {
            // No need to populate any values here
        }

        public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
        {
            var locations = new List<string>
            {
                "/Views/PDK/{1}/{0}.cshtml",
                "/Views/Shared/{0}.cshtml"
            };
            locations.AddRange(viewLocations);
            return locations;
        }
    }
}
