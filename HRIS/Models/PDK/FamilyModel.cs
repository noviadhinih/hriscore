using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRIS.Models.PDK
{
    public class FamilyModel
    {
       public string NPK { get; set; }
        public string Nama { get; set; }
        public string StatusPernikahan { get; set; }
        public string TanggalPernikahan { get; set; }
        public string StatusKeluarga {get;set;}
        public List<FamilyMemberModel> FamilyMembers { get; set; }

    }
}