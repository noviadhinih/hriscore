namespace HRIS.Models.PDK
{
    public class FamilyMemberModel
    {
        public string Npk { get; set; }
        public string Hubungan { get; set; }
        public string Nama { get; set; }
        public string JenisKelamin { get; set; }
        public string TempatTanggalLahir { get; set; }
        public string Pekerjaan { get; set; }
        public string NoIdentitas { get; set; }
        public string NoGardaMedika { get; set; }
        public string NoBPJS { get; set; }
        public string StatusBPJS { get; set; }
        public string ProgressStatusBPJS { get; set; }
    }
}

