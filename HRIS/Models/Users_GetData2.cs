using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.Data.SqlClient;


namespace HRIS.Models
{
    public class Users_GetData2
    {
        [Key]
        public int? ID_user { get; set; }
        public string? username { get; set; }
        public string? password { get; set; }
        public int? npk { get; set; }
        public string? name { get; set; }
        public string? division_name { get; set; }
        public string? dept_name { get; set; }
        public string? level_name { get; set; }
        public string? photo { get; set; }
        public int? roleID { get; set; }
        public string? email { get; set; }
        public int? ID_klinik { get; set; }
        public string? nama_klinik { get; set; }
        public int? directorateID { get; set; }
        public int? ID_division { get; set; }
        public int? ID_dept { get; set; }
        public int? ID_golongan { get; set; }
        public int? ID_sub { get; set; }
        public int? ID_level { get; set; }
        public int? ID_lokasi { get; set; }
        public int? vendorID { get; set; }
        public string? locationNm { get; set; }
        public int? section_id { get; set; }
        public string? no_ktp { get; set; }
        public int? level_idx_no { get; set; }
        public string? dept_handle { get; set; }
        public string? location_handle { get; set; }
        public int? section_id_con { get; set; }
        public int? Uactive { get; set; }
        public int? Eactive { get; set; }
        public int? TotalRows { get; set; }
        public int? isAssessor { get; set; }
        public DateTime? tgl_upadate_pass { get; set; }
        public int? is_change_def_pass { get; set; }
        public int? is_expPass { get; set; }
        public int? is_mfa { get; set; }
        public string msg { get; set; }
        public int? mfa_registrasi { get; set; }
       
    }
}