using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRIS.Models.General
{
    public class DatatableToJson
    {
        public long recordsFiltered { get; set; }
        public long recordsTotal { get; set; }
        public object data { get; set; }
    }
}