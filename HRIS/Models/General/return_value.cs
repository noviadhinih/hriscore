using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRIS.Models.General
{
    public class return_value
    {
      
        public bool status { get; set; }
        public string remarks { get; set; }
        public string fleet_id { get; set; }
        public string old_fleet_mf { get; set; }
        public string new_fleet_mf { get; set; }
  
    }
}