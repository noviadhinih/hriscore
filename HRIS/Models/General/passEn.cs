using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace HRIS.Models.General
{
    public class passEn
    {
        private byte[] key;
        private byte[] IV;
        private byte[] GenerateRandomKey(int keySizeInBits)
        {
            using (AesManaged aes = new AesManaged())
            {
                aes.KeySize = keySizeInBits;
                aes.GenerateKey();
                return aes.Key;
            }
            
        }
        public byte[] Encrypt(string plainText)
        {
            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.Key = this.key;
                aesAlg.IV = this.IV;

                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(plainText);
                        }
                        return msEncrypt.ToArray();
                    }
                }
            }
        }

        public string Decrypt(byte[] cipherText)
        {
            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.Key = this.key;
                aesAlg.IV = this.IV;

                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            return srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
        }

        private byte[] GenerateRandomIV(int ivSizeInBytes)
        {
            using (AesManaged aes = new AesManaged())
            {
                aes.IV = new byte[ivSizeInBytes];
                using (var rng = new RNGCryptoServiceProvider())
                {
                    rng.GetBytes(aes.IV);
                }
                return aes.IV;
            }
        }
    }
}