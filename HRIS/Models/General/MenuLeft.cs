using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;

namespace HRIS.Models.General
{
    public class MenuLeft
    {
        private string str_menuResult = string.Empty;

        public string recursiveMenu(string id_user)
        {
            try
            {
                // var menuItems = sp_getMenu.GetMenu(id_user).OrderBy(s => s.Menuid).ThenBy(s => s.ParentMenuID).ThenBy(s => s.urutan).ToList();
                // var header = menuItems.Where(m => m.ParentMenuID == 0).ToList();
                // foreach (var menuItem in header)
                // {
                //     // Inisialisasi kembali str_menuResult sebelum memanggil BuildMenu
                //     //str_menuResult += BuildMenu(menuItems, menuItem);
                //     str_menuResult += BuildMenuNew(menuItems, menuItem);
                // }
                return str_menuResult;
            }
            catch (System.Exception ex)
            {
                return "Failed load menu" + ex.ToString();
            }
        }
        private string BuildMenuNew(List<sp_getMenu> menuItems, sp_getMenu menuItem)
        {
            str_menuResult += "<li class='nav-item'>";
            // if (menuItem.jmlink == 0)
            // {
            //     str_menuResult += $"<a class=\"nav-link\" href=\"{menuItem.link}\" data-target=\"#{menuItem.id}\"  aria-expanded=\"true\" aria-controls=\"collapsePages\">";
            //     str_menuResult += "<i class=\"fas fa-fw fa-folder\"></i>";
            //     str_menuResult += $"<span>{menuItem.menu}</span>";
            //     str_menuResult += "</a>";
            // }
            // else
            // {
            //     var submenuItems = menuItems.Where(m => m.ParentMenuID == menuItem.Menuid).ToList();
            //     if (submenuItems.Any())
            //     {
            //         str_menuResult += $"<a class=\"nav-link\" href=\"{menuItem.link}\" data-toggle=\"collapse\" data-target=\"#collapsePages{menuItem.id}\"  aria-expanded=\"true\" aria-controls=\"{menuItem.id}\">";
            //         str_menuResult += "<i class=\"fas fa-fw fa-folder\"></i>";
            //         str_menuResult += $"<span>{menuItem.menu}</span>";
            //         str_menuResult += "</a>";
            //         str_menuResult += $"<div id=\"collapsePages{menuItem.id}\" class=\"collapse\" aria-labelledby=\"headingPages\" data-parent=\"#accordionSidebar\">";
            //         str_menuResult += "<div class=\"bg-white py-2 collapse-inner rounded\">";
            //         foreach (var subMenuItem in submenuItems)
            //         {
            //             if (subMenuItem.jmlink == 0)
            //             {
            //                 str_menuResult += $"<a class=\"collapse-item\" href=\"{subMenuItem.link}\">{subMenuItem.menu}</a>";
            //             }
            //             else
            //             {
            //                 str_menuResult += $"<a class=\"collapse-item\" href=\"{subMenuItem.link}\" data-toggle=\"collapse\"  data-target=\"#nested{subMenuItem.id}\" aria-expanded=\"false\" aria-controls=\"#nested{subMenuItem.id}\"> {subMenuItem.menu}";
            //                 str_menuResult += " <i class=\"fas fa-chevron-right float-right collapse-icon\"></i>";
            //                 str_menuResult += " </a>";
            //                 str_menuResult += $"<div id=\"nested{subMenuItem.id}\" class=\"collapse\">";
            //                 str_menuResult += "<ul class=\"nested-list\">";
            //                 var ch = menuItems.Where(m => m.ParentMenuID == subMenuItem.Menuid).ToList();
            //                 foreach (var c in ch)
            //                 {
            //                     str_menuResult += $"<li><a class=\"nested-item\" href=\"{c.link}\">{c.menu}</a></li>";
            //                 }
            //                 str_menuResult += "</ul>";
            //                 str_menuResult += " </div>";
            //             }

            //         }
            //         str_menuResult += "</div>";
            //         str_menuResult += "</div>";
            //     }

            // }
            // str_menuResult += "</li>";
            return str_menuResult;
        }

        private string BuildMenu(List<sp_getMenu> menuItems, sp_getMenu menuItem)
        {


            str_menuResult += "<li class='nav-item'>";
            // if (menuItem.jmlink == 0)
            // {
            //     str_menuResult += $"<a class=\"nav-link\" href=\"{menuItem.link}\"><i class='icon-layout menu-icon'></i>{menuItem.menu}</a>";
            // }
            // else
            // {
            //     str_menuResult += $"<a class='nav-link' data-toggle='collapse' href='#submenu-{menuItem.id}' aria-expanded='false' aria-controls='submenu-{menuItem.id}'><i class='icon-layout menu-icon'></i> {menuItem.menu}</a><i class='menu-arrow'></i>";
            //     var submenuItems = menuItems.Where(m => m.ParentMenuID == menuItem.Menuid).ToList();
            //     if (submenuItems.Any())
            //     {
            //         str_menuResult += "<i class='menu-arrow'></i>";
            //         str_menuResult += $"<div class='collapse' id='submenu-{menuItem.id}'>";
            //         str_menuResult += "<ul class='nav flex-column sub-menu'>";
            //         foreach (var subMenuItem in submenuItems)
            //         {
            //             if (subMenuItem.jmlink == 0)
            //             {
            //                 str_menuResult += "<li class='nav-item'>";
            //                 str_menuResult += $"<a class='nav-link' href='{subMenuItem.link}'>{subMenuItem.menu}</a>";
            //                 str_menuResult += "</li>";
            //             }
            //             else
            //             {
            //                 str_menuResult += "<li class='nav-item' data-toggle='toggle' data-target='content" + subMenuItem.Menuid + "'>";
            //                 str_menuResult += $"<a class='nav-link'>{subMenuItem.menu}<i class='menu-arrow'></i></a>";
            //                 str_menuResult += "<div id='content" + subMenuItem.Menuid + "' class='toggle-content'>";
            //                 str_menuResult += "<ul class='nav flex-column sub-menu'>";
            //                 str_menuResult += "<li>";
            //                 str_menuResult += $"<a class='nav-link' href='{subMenuItem.link}'>{subMenuItem.menu}</a>";
            //                 str_menuResult += "</li>";
            //                 str_menuResult += "</ul>";
            //                 str_menuResult += "</div>";
            //                 str_menuResult += "</li>";
            //             }

            //         }
            //         str_menuResult += "</ul>";
            //         str_menuResult += "</div>";
            //     }
            // }

            // str_menuResult += "</li>";
            return str_menuResult;
        }


        // private static string BuildMenu(List<sp_getMenu> menuItems, sp_getMenu menuItem)
        // {
        //     string result = "<li class=\"nav-item\">";
        //     if (menuItem.jmlink == 0)
        //     {
        //         result +=$"<a class=\"nav-link\" href=\"{menuItem.link}\">{menuItem.menu}</a>";
        //     }
        //     else
        //     {
        //         if (menuItem.link != "#")
        //         {
        //             result += $"<a class=\"nav-link\" data-toggle=\"collapse\" href=\"{menuItem.link}\" aria-expanded=\"false\" aria-controls=\"submenu-{menuItem.id}\">";
        //         }
        //         else
        //         {
        //             result += $"<a class=\"nav-link\" data-toggle=\"collapse\" href=\"#submenu-{menuItem.id}\" aria-expanded=\"false\" aria-controls=\"submenu-{menuItem.id}\">";

        //         }
        //         result += $"<i class=\"{menuItem.icon}\"></i>";
        //         result += $"<span class=\"menu-title\">{menuItem.menu}</span>";
        //         if (menuItem.jmlink > 0)
        //         {
        //             result += "<i class=\"menu-arrow\"></i>";
        //         }
        //         result += "</a>";

        //         if (menuItems.Any(m => m.ParentMenuID == menuItem.id))
        //         {
        //             result += $"<div class=\"collapse\" id=\"submenu-{menuItem.id}\">";
        //             result += "<ul class=\"nav flex-column sub-menu\">";

        //             foreach (var subMenuItem in menuItems.Where(m => m.ParentMenuID == menuItem.id))
        //             {
        //                 result += BuildMenu(menuItems, subMenuItem);
        //             }

        //             result += "</ul>";
        //             result += "</div>";
        //         }
        //     }


        //     result += "</li>";

        //     return result;
        // }
    }
}