using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRIS.Models.General
{
    public class SKTA
    {
        public string jenisTms { get; set; }
        public string letter_id { get; set; }
        public string ID_type { get; set; }
        public string ID_user { get; set; }
        public string ID_lokasi { get; set; }
        public string ID_division { get; set; }
        public string ID_dept { get; set; }
        public string ID_golongan { get; set; }

        public string hasSent { get; set; }
        public string TotalData { get; set; }
        public string SeqNo { get; set; }
        public string title { get; set; }
        public string info { get; set; }
        public string npk { get; set; }
        public string emp_name { get; set; }
        //public string npk_apv { get; set; }
        public string npk_apv { get; set; }
        public string apv_spv { get; set; }
        public string apv_atasan { get; set; }
        //public string name_apv { get; set; }
        public string name_apv { get; set; }
        public string npk_dept { get; set; }
        public string name_dept { get; set; }

        public string emp_status { get; set; }
        public string stats { get; set; }
        public string statsNm { get; set; }
        public string reason { get; set; }
        public string type_name { get; set; }
        public string skta_dateText { get; set; }
        public string date_startText { get; set; }
        public string date_endText { get; set; }
        public string jam_masuk { get; set; }
        public string jam_keluar { get; set; }
        public string last_updateText { get; set; }
        public string div_name { get; set; }
        public string dept_name { get; set; }
        public string golongan_name { get; set; }
        public string status_atasan { get; set; }
        public string status_hrd { get; set; }
        public string npk_input { get; set; }
        public string npk_hrd { get; set; }
        public string date_input { get; set; }
        public string date_atasan { get; set; }
        public string date_hrd { get; set; }
        public string date_dept { get; set; }
        public string npk_create { get; set; }
        public string costcenter { get; set; }
        public string id_shift { get; set; }

        public string skta_date { get; set; }
        public string date_start { get; set; }
        public string date_end { get; set; }
        public string last_update { get; set; }

        public string pr { get; set; }
        public string yr { get; set; }

        public string SKTADateText { get; set; }
        public string overtimeDateMonth { get; set; }
        public string overtimeDateYear { get; set; }
        public string createdBy { get; set; }
        public string type { get; set; }
    }
}