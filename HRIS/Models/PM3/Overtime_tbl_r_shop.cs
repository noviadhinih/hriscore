﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
namespace HRIS.Models.PM3
{
    public class Overtime_tbl_r_shop
    {
        [Key]
        public long? pid_shop { get; set; }
        public string? shop { get; set; }
        public string? ShopCode { get; set; }
        public bool? is_pis { get; set; }
    }
}
