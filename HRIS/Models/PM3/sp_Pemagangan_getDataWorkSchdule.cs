﻿using System.ComponentModel.DataAnnotations;

namespace HRIS.Models.PM3
{
    public class sp_Pemagangan_getDataWorkSchdule
    {
        [Key]
        public long? id_sch_magang { get; set; }
        public string? npk_magang { get; set; }
        public string? name { get; set; }
        public string? division { get; set; }
        public string? department { get; set; }
        public string? calendar_date { get; set; }
        public string? DWS_Type { get; set; }
        public string? ID_Master_Shift_Header { get; set; }
        public string? Master_Shift_Header_Name { get; set; }
        public string? clock_in { get; set; }
        public string? clock_out { get; set; }
    }
}
