using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Models.PM3
{
    public class sp_CUTI_getDataReject
    {
        [Key]
        public long? leave_id { get; set; }
        public string? npk { get; set; }
        public string? name { get; set; }
        public string? divNm { get; set; }
        public string? deptNm { get; set; }
        public string? golongan_name { get; set; }
        public string? employee { get; set; }
        public string? days_leave { get; set; }
        public string? days_long_leave { get; set; }
        public string? status { get; set; }
        public string? statusText { get; set; }
        public DateTime? last_update { get; set; }
        public DateTime? approval_date { get; set; }
        public string? CTDate { get; set; }
        public string? CPDate { get; set; }
    }
}