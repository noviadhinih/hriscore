using System.ComponentModel.DataAnnotations;

namespace HRIS.Models.PM3
{
    public class SP_PDK_Family_GetDataEmployee
    {
        [Key]
        public string? npk { get; set; }
        public string? name { get; set; }
        public string? status_family { get; set; }
        public string? status_marriage { get; set; }
        public string? tanggal_nikah { get; set; }
    }
}