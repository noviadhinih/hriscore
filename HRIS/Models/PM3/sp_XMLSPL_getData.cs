using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Models.PM3
{
    public class sp_XMLSPL_getData
    {
        [Key]
        public long? id_xml { get; set; }
        public string? NoSurat { get; set; }
        public string? NPK { get; set; }
        public string? Nama { get; set; }
        public string? TgglAwal { get; set; }
        public string? type_name { get; set; }
        public string? SPLCode { get; set; }
        public string? status_xml { get; set; }
        public DateTime? created_date { get; set; }
        public string? msg_xml { get; set; }
    }
}