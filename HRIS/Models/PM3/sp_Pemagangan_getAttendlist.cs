﻿using System.ComponentModel.DataAnnotations;

namespace HRIS.Models.PM3
{
    public class sp_Pemagangan_getAttendlist
    {
        [Key]
        public string? pid { get; set; }
        public int? npk { get; set; }
        public string? empNm { get; set; }
        public string? divNm { get; set; }
        public string? deptNm { get; set; }
        public long? CICO_ID { get; set; }
        public string? shift { get; set; }
        public string? stats { get; set; }
        public DateTime? CI_date { get; set; }
        public DateTime? CO_date { get; set; }
        public DateTime? clock_in { get; set; }
        public DateTime? clock_out { get; set; }
        public DateTime? overtime_in { get; set; }
        public DateTime? overtime_out { get; set; }
        public string? tglKerja { get; set; }
        public string? dayName_ { get; set; }
        public string? tglDesc { get; set; }
    }
}
