using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Models.PM3
{
    public class sp_WCalendar_getdata
    {
        [Key]
        public string? WSR_ID { get; set; }
        public string? WSR_Name { get; set; }
        public string? Master_Shift_Header_Name { get; set; }
        public string? start_date { get; set; }
        public string? end_date { get; set; }
        public string? last_update { get; set; }
        public string? active_ { get; set; }
        public string? dibuat { get; set; }

    }
}