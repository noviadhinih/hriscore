using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.Data.SqlClient;

namespace HRIS.Models.PM3
{
    public class WSR_Template
    {
        [Key]
        public long? WSR_ID { get; set; }
        public string? WSR_Name { get; set; }
        public long? ID_Master_Shift_Header { get; set; }
        public DateTime? start_date { get; set; }
        public DateTime? end_date { get; set; }
        public DateTime? last_update { get; set; }
        public long? ID_user { get; set; }
        public short? active { get; set; }
    }
}