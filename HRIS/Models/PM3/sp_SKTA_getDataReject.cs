using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Models.PM3
{
    public class sp_SKTA_getDataReject
    {
        [Key]
        public long? letter_id { get; set; }
        public string? employee { get; set; }
        public string? npk { get; set; }
        public string? empNm { get; set; }
        public string? divNm { get; set; }
        public string? deptNm { get; set; }
        public string? golongan_name { get; set; }
        public long? ID_type { get; set; }
        public string? type_name { get; set; }
        public DateTime? skta_date { get; set; }
        public DateTime? date_start { get; set; }
        public DateTime? date_end { get; set; }
        public string? info { get; set; }
        public DateTime? last_update { get; set; }
        public DateTime? approval_date { get; set; }
    }
}