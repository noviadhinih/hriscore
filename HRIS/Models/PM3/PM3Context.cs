using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
﻿using Microsoft.EntityFrameworkCore;
using HRIS.Models.PM3;

namespace HRIS.Models.PM3
{
    public class PM3Context  : DbContext
    {
        public PM3Context(DbContextOptions<PM3Context> options) : base(options)
        {
        }
        /// <summary>
        /// TABEL
        /// </summary>
        public DbSet<location> location { get; set; }
        public DbSet<Z_tx_req_xml_pws> Z_tx_req_xml_pws { get; set; }
        public DbSet<Master_Shift_Header> Master_Shift_Header { get; set; }
        public DbSet<WSR_Template> WSR_Template { get; set; }
        public DbSet<WSR_Template_Detail> WSR_Template_Detail { get; set; }
        public DbSet<view_DWS> view_DWS { get; set; }
        public DbSet<dept> dept { get; set; }
        public DbSet<TrLogError> TrLogError { get; set; }
        public DbSet<viewEmployeeOrganizationAll> viewEmployeeOrganizationAll { get; set; }





        /// <summary>
        /// STORE PROCEDURE
        /// </summary>
        public DbSet<Users_GetData2> Users_GetData2 { get; set; }
        public DbSet<Trtokengoogle> Trtokengoogle { get; set; }
        public DbSet<sp_getMenu> sp_getMenu { get; set; }
        public DbSet<Sp_Attendance_GetViewAbsenceForEmployeeNew> Sp_Attendance_GetViewAbsenceForEmployeeNew { get; set; }
        public DbSet<sp_Overtime_getdataDetailAbesensi2> sp_Overtime_getdataDetailAbesensi2 { get; set; }
        public DbSet<sp_Attendance_control> sp_Attendance_control { get; set; }
        public DbSet<sp_Shift_getMasterData> sp_Shift_getMasterData { get; set; }
        public DbSet<sp_PWS_getrequestplant> sp_PWS_getrequestplant { get; set; }
        public DbSet<sp_PWS_getDetailRequest> sp_PWS_getDetailRequest { get; set; }
        public DbSet<sp_WCalendar_getdata> sp_WCalendar_getdata { get; set; }
        public DbSet<sp_WCalendar_generate> sp_WCalendar_generate { get; set; }
        public DbSet<sp_WCalendar_crud> sp_WCalendar_crud { get; set; }
        public DbSet<sp_WBS_getdata> sp_WBS_getdata { get; set; }
        public DbSet<sp_retrieveMealTime> sp_retrieveMealTime { get; set; }
        public DbSet<sp_retrieveMealTimeRpt> sp_retrieveMealTimeRpt { get; set; }
        public DbSet<sp_XMLCICO_getDataCount> sp_XMLCICO_getDataCount { get; set; }
        public DbSet<sp_XMLCICO_getData> sp_XMLCICO_getData { get; set; }
        public DbSet<sp_XMLSUPEM_getData> sp_XMLSUPEM_getData { get; set; }
        public DbSet<sp_XMLSPL_getData> sp_XMLSPL_getData { get; set; }
        
        public DbSet<Sp_PDK_Family_GetDataFamily> Sp_PDK_Family_GetDataFamily {get;set;}
        public DbSet<SP_PDK_Family_GetDataEmployee> SP_PDK_Family_GetDataEmployee {get;set;}
        public DbSet<sp_SUPEM_getDataReject> sp_SUPEM_getDataReject {get;set;}
        public DbSet<sp_SKTA_getDataReject> sp_SKTA_getDataReject {get;set;}
        public DbSet<sp_CUTI_getDataReject> sp_CUTI_getDataReject {get;set;}
        public DbSet<sp_Overtime_getDataReject> sp_Overtime_getDataReject {get;set;}
        public DbSet<sp_SKTA_getData> sp_SKTA_getData {get;set;}
        public DbSet<sp_SUPEM_getData> sp_SUPEM_getData {get;set;}
        public DbSet<SUPEM_Type_Get> SUPEM_Type_Get {get;set;}
        public DbSet<Employee_Working_Schedule_Get> Employee_Working_Schedule_Get {get;set;}
        public DbSet<SUPEM_GetBy> SUPEM_GetBy {get;set;}
        public DbSet<CICO_Stats_Get> CICO_Stats_Get {get;set;}
        public DbSet<CICO_GetPriority> CICO_GetPriority {get;set;}
        public DbSet<SUPEM_CheckLeave> SUPEM_CheckLeave {get;set;}
        public DbSet<Employee_ValidateSKTAByNPK> Employee_ValidateSKTAByNPK {get;set;}
        public DbSet<Sp_Employee_GetAllAtasan_ByDeptHead> Sp_Employee_GetAllAtasan_ByDeptHead {get;set;}
        public DbSet<CICO_Get> CICO_Get {get;set;}
        public DbSet<sp_SUPEM_getRekapExcel> sp_SUPEM_getRekapExcel {get;set;}
        public DbSet<sp_SKTA_getApproval> sp_SKTA_getApproval {get;set;}
        public DbSet<sp_SUPEM_getApproval> sp_SUPEM_getApproval {get;set;}
        public DbSet<sp_CUTI_getApproval> sp_CUTI_getApproval {get;set;}
        public DbSet<sp_SPL_getApproval> sp_SPL_getApproval {get;set;}
        public DbSet<sp_Pemagangan_getData> sp_Pemagangan_getData { get;set;}
        public DbSet<sp_Pemagangan_ValidasiUploadHiring> sp_Pemagangan_ValidasiUploadHiring { get;set;}
        public DbSet<sp_Pemagangan_getDataWorkSchdule> sp_Pemagangan_getDataWorkSchdule { get;set;}
        public DbSet<sp_Pemagangan_getAttendlist> sp_Pemagangan_getAttendlist { get;set;}
        public DbSet<sp_Overtime_getMsProd> sp_Overtime_getMsProd { get;set;}
        public DbSet<Overtime_tbl_m_production> Overtime_tbl_m_production { get;set;}
        public DbSet<Overtime_tbl_r_shop> Overtime_tbl_r_shop { get;set;}
        public DbSet<Overtime_tbl_r_line> Overtime_tbl_r_line { get;set;}
        



       protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(new BaseUrls().getPM3ConnectionString());
            }
        }
        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
           
            base.OnModelCreating(modelBuilder);
        }

    }
}