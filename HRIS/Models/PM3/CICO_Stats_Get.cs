using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Models.PM3
{
    public class CICO_Stats_Get
    {
        [Key]
        public long? statsID { get; set; }
        public string? stats_code { get; set; }
        public string? stats_name { get; set; }
        public DateTime? last_update { get; set; }
        public int? poin { get; set; }
        public string? is_specific { get; set; }
        public int? priority { get; set; }
        public long? typeID { get; set; }
    }
}