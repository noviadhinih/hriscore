using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Models.PM3
{
    public class sp_retrieveMealTimeRpt
    {
        [Key]
        public long? request_id { get; set; }
        public string? Master_Shift_Header_Name { get; set; }
        public string? deptNm { get; set; }
        public long? ID_Master_Shift_Header { get; set; }

        public DateTime? date_start { get; set; }
        public DateTime? date_end { get; set; }
        public string? employee { get; set; }
        public string? npk { get; set; }
        public string? empNm { get; set; }
        public string? email { get; set; }
        public long? ID_dept { get; set; }
        public long? ID_lokasi { get; set; }
        public string? info { get; set; }
        public long? meal_time_id { get; set; }
        public DateTime? meal_time { get; set; }
        public string? golongan_name { get; set; }
        public decimal? fee { get; set; }
        public string? typeName { get; set; }
        public string? stats { get; set; }
        public string? status { get; set; }
        public DateTime? last_update { get; set; }
        public DateTime? apvDate { get; set; }
        public string? dayNight { get; set; }
        public string? name { get; set; }
    }
}