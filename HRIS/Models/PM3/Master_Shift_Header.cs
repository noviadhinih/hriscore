using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
namespace HRIS.Models.PM3
{
    public class Master_Shift_Header
    {
        [Key]
        public long? ID_Master_Shift_Header { get; set; }
        public string? shift_code { get; set; }
        public string? Master_Shift_Header_Name { get; set; }
        public short? active { get; set; }
        public DateTime? last_update { get; set; }
        public long? ID_user { get; set; }
    }
}