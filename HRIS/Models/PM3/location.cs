using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Models.PM3
{
    public class location
    {
        [Key]
        public long? ID_lokasi { get; set; }
        public string? locationCode { get; set; }
        public string? locationNm { get; set; }
        public DateTime? last_update { get; set; }
        public string? sapCode { get; set; }
        public string? SAP_ID { get; set; }
    }
}