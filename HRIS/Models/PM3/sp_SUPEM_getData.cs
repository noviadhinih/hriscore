using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Models.PM3
{
    public class sp_SUPEM_getData
    {
        [Key]
        public long? permission_id { get; set; }
        public string? npk { get; set; }
        public string? emp_name { get; set; }
        public string? div_name { get; set; }
        public string? dept_name { get; set; }
        public string? permission_dateText { get; set; }
        public string? permit_code_name { get; set; }
        public string? info { get; set; }
        public string? statsNm { get; set; }
        public string? statsText { get; set; }
        public string? npk_input { get; set; }
        public string? date_input { get; set; }
        public string? npk_apv { get; set; }
        public string? date_atasan { get; set; }
        public string? npk_hrd { get; set; }
        public string? date_hrd { get; set; }
        public string? date_start { get; set; }
        public string? date_end { get; set; }
    }
}