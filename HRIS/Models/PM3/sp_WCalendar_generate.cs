using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Models.PM3
{
    public class sp_WCalendar_generate
    {
        [Key]
        public long? detail_ID { get; set; }
        public DateTime? calendar_date { get; set; }
        public string? DWS_ID { get; set; }
        public string? DWS_Name { get; set; }
        public string? DWS_Type { get; set; }
        public DateTime? clock_in { get; set; }
        public DateTime? clock_out { get; set; }
        public string? notes { get; set; }

    }
}