using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Models.PM3
{
    public class sp_PWS_getDetailRequest
    {
        [Key]
        public long? pid { get; set; }
        public string? NPK { get; set; }
        public string? Nama { get; set; }
        public string? status_ { get; set; }
        public string? file_name_ { get; set; }
        public string? file_name_out { get; set; }
        public string? status_xml { get; set; }
        public string? msg_xml { get; set; }

    }
}