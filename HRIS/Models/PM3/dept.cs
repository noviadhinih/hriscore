using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Models.PM3
{
    public class dept
    {
        [Key]
        public long? ID_dept { get; set; }
        public long? ID_div { get; set; }
        public string? dept_code { get; set; }
        public string? name { get; set; }
        public string? SAP_ID { get; set; }
        public DateTime? last_update { get; set; }
        public short? active { get; set; }
        public short? isReal { get; set; }
        public short? pds_pcd { get; set; }
        public string? cost_centre { get; set; }
        public byte? is_manufacture { get; set; }
        public string? Shop_Code { get; set; }
        public string? abbr { get; set; }

    }
}