using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Models.PM3
{
    public class sp_Shift_getMasterData
    {
        [Key]
        public string? ID_Master_Shift_Header { get; set; }
        public string? shift_code { get; set; }
        public string? ID_user { get; set; }
        public bool? active { get; set; }
        public string? last_update { get; set; }
        public string? Master_Shift_Header_Name { get; set; }
    }
}