using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Models.PM3
{
    public class viewEmployeeOrganizationAll
    {
        [Key]
        public string? npk { get; set; }
        public string? name { get; set; }
        public long? ID_dept { get; set; }
        public int? level_idx_no { get; set; }
        public long? directorateID { get; set; }
        public long? ID_division { get; set; }
    }
}