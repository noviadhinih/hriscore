using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.Data.SqlClient;
namespace HRIS.Models.PM3
{
    public class WSR_Template_Detail
    {
        [Key]
        public long? detail_ID { get; set; }
        public long? WSR_ID { get; set; }
        public DateTime? calendar_date { get; set; }
        public string? DWS_ID { get; set; }
        public string? DWS_Name { get; set; }
        public string? DWS_Type { get; set; }
        public DateTime? clock_in { get; set; }
        public DateTime? clock_out { get; set; }
        public string? notes { get; set; }
    }
}