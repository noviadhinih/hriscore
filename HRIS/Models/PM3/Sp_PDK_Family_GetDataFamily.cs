using System.ComponentModel.DataAnnotations;

namespace HRIS.Models.PM3
{
    public class Sp_PDK_Family_GetDataFamily
    {
        [Key]
        public long? id_fam { get; set; }
        public string? npk { get; set; }
        public long? relationID { get; set; }
        public string? relationship { get; set; }
        public string? name { get; set; }
        public string? gender { get; set; }
        public string? genderNm { get; set; }
        public string? tempat_lahir { get; set; }
        public string? tanggal_lahir { get; set; }
        public long? job { get; set; }
        public string? jobNm { get; set; }
        public string? no_ktp { get; set; }
        public string? no_garda_medika { get; set; }
        public string? no_bpjs { get; set; }
        public string? status { get; set; }
        public string? statusNm { get; set; }
        public int? status_medical { get; set; }
        public string? expired_medical { get; set; }
        public long? id_typeID { get; set; }
        public string? no_kk { get; set; }
        public long? id_faskes { get; set; }
        public string? faskesNm { get; set; }
        public string? status_kawin { get; set; }
        public int? bpjs_registered { get; set; }
        public int? bpjs_unregistered { get; set; }
        public string? status_bpjs { get; set; }
        public string? Progress_BPJS { get; set; }
        public string? Reject_Comment { get; set; }
        public string? status_kepesertaan { get; set; }
    }
}