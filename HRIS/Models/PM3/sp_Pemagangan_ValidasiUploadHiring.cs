﻿using System.ComponentModel.DataAnnotations;

namespace HRIS.Models.PM3
{
    public class sp_Pemagangan_ValidasiUploadHiring
    {
        [Key]
        public string? directorateID { get; set; }
        public string? ID_division { get; set; }
        public string? ID_dept { get; set; }
        public string? section_id { get; set; }
        public string? sub_section_id { get; set; }
        public string? id_lokasi { get; set; }
        public string? msg { get; set; }
    }
}
