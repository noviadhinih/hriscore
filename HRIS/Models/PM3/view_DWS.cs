using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Models.PM3
{
    public class view_DWS
    {
        [Key]
        public long? DWS_ID { get; set; }
        public string? DWS_Name { get; set; }
        public long? DWS_type_id { get; set; }
        public string? typeName { get; set; }
        public string? Info { get; set; }
        public DateTime? clock_in { get; set; }
        public DateTime? clock_out { get; set; }
        public string? WBS_ID { get; set; }
        public string? WBS_Text { get; set; }
        public DateTime? last_update { get; set; }
        public long? ID_user { get; set; }
        public string? notes { get; set; }
        public string? SAP_variant { get; set; }
    }
}