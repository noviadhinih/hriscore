using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
namespace HRIS.Models.PM3
{
    public class sp_Attendance_control
    {
        [Key]
        public string? CICO_ID { get; set; }
        public string? employee { get; set; }
        public string? npk { get; set; }
        public string? empNm { get; set; }
        public string? DWS_Name { get; set; }
        public string? CI_date { get; set; }
        public string? CO_date { get; set; }
        public string? DWS_ID { get; set; }
        public string? DWS_Type { get; set; }
        public string? status_ { get; set; }
        public string? stats { get; set; }
        public string? apv_stats { get; set; }
        public bool? isManual { get; set; }
        public string? last_update { get; set; }
    }
}