﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
namespace HRIS.Models.PM3
{
    public class Overtime_tbl_m_production
    {
        [Key]
        public long? pid { get; set; }
        public long? pid_shop { get; set; }
        public string? shop { get; set; }
        public long? pid_line { get; set; }
        public string? line { get; set; }
        public long? pid_pos { get; set; }
        public string? pos { get; set; }
        public int? durasi { get; set; }
        public string? tipe_durasi { get; set; }
        public int? jumlah_mp { get; set; }
        public string? created_by { get; set; }
        public DateTime? created_date { get; set; }
        public string? updated_by { get; set; }
        public DateTime? updated_date { get; set; }
        public string? pis { get; set; }
        public bool? active { get; set; }
        public string? apvCostControl { get; set; }
        public string? apvCostControlName { get; set; }
        public DateTime? apvCostControlDate { get; set; }
        public string? apvCostControlStats { get; set; }
        public string? apvPCDHead { get; set; }
        public string? apvPCDHeadName { get; set; }
        public DateTime? apvPCDHeadDate { get; set; }
        public string? apvPCDHeadStats { get; set; }
        public string? summaryStats { get; set; }
        public int? summaryApproval { get; set; }
        public string? tgl_valid { get; set; }
        public string? tgl_end { get; set; }
        public string? rejectComment { get; set; }
        public string? dot_hot { get; set; }
        public int? durasiPIS { get; set; }
        public string? day_night { get; set; }
    }
}
