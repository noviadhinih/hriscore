using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Models.PM3
{
    public class sp_SPL_getApproval
    {
        [Key]
        public long? overtime_id { get; set; }
        public string? npk { get; set; }
        public string? emp_name { get; set; }
        public string? div_name { get; set; }
        public string? dept_name { get; set; }
        public string? overtimeDateText { get; set; }
        public string? jam_mulai { get; set; }
        public string? jam_selesai { get; set; }
        public string? type_name { get; set; }
        public string? info { get; set; }
        public string? statsNm { get; set; }
        public string? npk_input { get; set; }
        public string? date_input { get; set; }
        public string? npk_apv { get; set; }
        public string? date_atasan { get; set; }
        public string? npk_hrd { get; set; }
        public string? date_hrd { get; set; }
        public string? overtimeDateMonth { get; set; }
        public string? overtimeDateYear { get; set; }
        public int? SeqNo { get; set; }
        public long? cico_id2 { get; set; }
        public string? npkOutstanding { get; set; }
        public string? ID_division { get; set; }
        public string? ID_dept { get; set; }
        public string? ID_golongan { get; set; }
        public string? createdBy { get; set; }
        public string? skta_date { get; set; }
        public string? Cost_Center_Id { get; set; }
        public string? ID_Master_Shift_Header { get; set; }
        public string? statsNmText { get; set; }
    }
}