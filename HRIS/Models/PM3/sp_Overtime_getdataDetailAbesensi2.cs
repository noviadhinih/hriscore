using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
namespace HRIS.Models.PM3
{
    public class sp_Overtime_getdataDetailAbesensi2
    {
        [Key]
        public long? overtime_id { get; set; }
        public string? npk { get; set; }
        public DateTime? overtime_date { get; set; }
        public DateTime? date_start { get; set; }
        public DateTime? date_end { get; set; }
        public string? overtime_date_desc { get; set; }
        public string? date_start_time { get; set; }
        public string? date_end_time { get; set; }
    }
}