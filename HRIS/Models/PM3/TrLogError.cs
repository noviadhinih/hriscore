using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.Data.SqlClient;
using HRIS.Models.General;

namespace HRIS.Models.PM3
{
    public class TrLogError
    {
        [Key]
        public long? id { get; set; }
        public string? controller { get; set; }
        public string? method { get; set; }
        public string? logError { get; set; }
        public DateTime? created_date { get; set; }
        public string? created_by { get; set; }
    }
}