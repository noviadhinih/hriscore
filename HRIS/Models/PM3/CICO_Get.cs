using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Models.PM3
{
    public class CICO_Get
    {
        [Key]
        public long? CICO_ID { get; set; }
        public string? npk { get; set; }
        public string? CheckInTimeText { get; set; }
        public string? CheckOutTimeText { get; set; }
        public DateTime? working_date { get; set; }
    }
}