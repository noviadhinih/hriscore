﻿using System.ComponentModel.DataAnnotations;

namespace HRIS.Models.PM3
{
    public class sp_Pemagangan_getData
    {
        [Key]
        public string? npk_magang { get; set; }
        public string? name { get; set; }
        public string? directorate { get; set; }
        public string? division { get; set; }
        public string? department { get; set; }
        public string? section { get; set; }
        public string? subSection { get; set; }
        public string? lokasiKerja { get; set; }
        public string? work_date { get; set; }
        public string? end_work_date { get; set; }
        public string? gender { get; set; }
        public string? email { get; set; }
        public string? created_date { get; set; }
        public string? created_by { get; set; }
    }
}
