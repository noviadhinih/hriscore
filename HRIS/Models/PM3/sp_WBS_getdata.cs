using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Models.PM3
{
    public class sp_WBS_getdata
    {
         [Key]
        public long? WBS_ID { get; set; }
        public string? WBS_Name { get; set; }
        public string? clock_start { get; set; }
        public string? clock_end { get; set; }
        public string? last_update { get; set; }
        public long? ID_user { get; set; }
        public string? info { get; set; }
    }
}