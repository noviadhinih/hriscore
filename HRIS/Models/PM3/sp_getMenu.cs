using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Models.PM3
{
    public class sp_getMenu
    {
        [Key]
        public int? id { get; set; }
        public string? id_user { get; set; }
        public string? username { get; set; }
        public string? menu { get; set; }
        public string? link { get; set; }
        public int? ParentMenuID { get; set; }
        public int? urutan { get; set; }
        public int? Menuid { get; set; }
        public string? icon { get; set; }
        public int? jmlink { get; set; }
        public int? isApv { get; set; }
        public string? JenisMenu { get; set; }
        public string? id_dept { get; set; }
        public string? departement { get; set; }
        public string? id_lokasi { get; set; }
        public string? lokasiKerja { get; set; }
    }
}