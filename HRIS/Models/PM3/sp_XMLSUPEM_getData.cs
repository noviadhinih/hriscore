using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Models.PM3
{
    public class sp_XMLSUPEM_getData
    {
        [Key]
        public string? permission_id { get; set; }
        public string? Nama { get; set; }
        public string? NPK { get; set; }
        public string? division { get; set; }
        public string? dept { get; set; }
        public string? golongan_name { get; set; }
        public string? permit_code { get; set; }
        public string? permit_name { get; set; }
        public string? permission_date { get; set; }
        public string? status_xml { get; set; }
        public short? pro_scs { get; set; }
        public DateTime? last_update { get; set; }
        public string? pro_msg { get; set; }
        public string? IDNo { get; set; }
    }
}