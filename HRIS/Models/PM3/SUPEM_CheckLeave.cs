using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Models.PM3
{
    public class SUPEM_CheckLeave
    {
        [Key]
        public string? npk { get; set; }
        public long? leave_id { get; set; }
        public DateTime? leave_date { get; set; }
        public string? stats { get; set; }
        public DateTime? last_update { get; set; }
    }
}