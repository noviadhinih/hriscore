using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
namespace HRIS.Models.PM3
{
    public class sp_XMLCICO_getDataCount
    {
        [Key]
        public string? status_xml { get; set; }
        public string? jmlh { get; set; }
    }
}