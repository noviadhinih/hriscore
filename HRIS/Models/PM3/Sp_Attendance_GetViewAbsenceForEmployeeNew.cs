using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Models.PM3
{
    public class Sp_Attendance_GetViewAbsenceForEmployeeNew
    {
        [Key]
        public long? schID { get; set; }
        public long? CICO_ID { get; set; }
        public string? employee { get; set; }
        public string? npk { get; set; }
        public string? empNm { get; set; }
        public string? divNm { get; set; }
        public string? deptNm { get; set; }
        public string? golongan_name { get; set; }
        public string? status_kerja { get; set; }
        public string? WSR_name { get; set; }
        public DateTime? calendar_date { get; set; }
        public DateTime? CI_date { get; set; }
        public DateTime? CO_date { get; set; }
        public long? DWS_ID { get; set; }
        public string? DWS_Type { get; set; }
        public string? overtime_in { get; set; }
        public string? overtime_out { get; set; }
        public string? stats { get; set; }
        public string? status { get; set; }
        public string? apv_stats { get; set; }
        public int? isManual { get; set; }
        public DateTime? last_update { get; set; }
        public int? selisihmenit { get; set; }
        public string? colorhrilibur { get; set; }
        public string? OtIN { get; set; }
        public string? OtOUT { get; set; }
        public string? telat { get; set; }
        public string? jamEmp_work { get; set; }
       
    }
}