﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
namespace HRIS.Models.PM3
{
    public class Overtime_tbl_r_line
    {
        [Key]
        public long? pid_line { get; set; }
        public string? line { get; set; }
    }
}
