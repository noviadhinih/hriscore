using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.Data.SqlClient;

namespace HRIS.Models.PM3
{
    public class Trtokengoogle
    {
        [Key]
        public long? id { get; set; }
        public string? token { get; set; }
        public string? username { get; set; }
        public DateTime? created_date { get; set; }
    }
}