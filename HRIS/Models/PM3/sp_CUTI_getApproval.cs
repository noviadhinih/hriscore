using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Models.PM3
{
    public class sp_CUTI_getApproval
    {
        [Key]
        public long? leave_id { get; set; }
        public string? npk { get; set; }
        public string? emp_name { get; set; }
        public string? div_name { get; set; }
        public string? dept_name { get; set; }
        public string? leave_dateText { get; set; }
        public string? leave_typeText { get; set; }
        public string? statsNm { get; set; }
        public string? statsText { get; set; }
        public string? npk_input { get; set; }
        public string? date_input { get; set; }
        public string? last_updateText { get; set; }
        public string? npk_apv { get; set; }
        public string? date_atasan { get; set; }
        public string? npk_hrd { get; set; }
        public string? date_hrd { get; set; }
        public int? SeqNo { get; set; }
        public string? stats { get; set; }
        public long? cico_id { get; set; }
        public string? reject_reason { get; set; }
        public string? npkOutstanding { get; set; }
        public string? ID_division { get; set; }
        public string? ID_dept { get; set; }
        public string? ID_golongan { get; set; }
        public string? createdBy { get; set; }
        public string? leave_date { get; set; }
        public string? Cost_Center_Id { get; set; }
        public string? ID_Master_Shift_Header { get; set; }
    }
}