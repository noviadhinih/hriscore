using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
namespace HRIS.Models.PM3
{
    public class Z_tx_req_xml_pws
    {
        [Key]
        public long? pid { get; set; }
        public string? plant { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public bool? status { get; set; }
        public string? request { get; set; }
        public string? created_by { get; set; }
        public DateTime? created_date { get; set; }
        public string? updated_by { get; set; }
        public DateTime? updated_date { get; set; }
        public string? msg_xml { get; set; }
    }
}