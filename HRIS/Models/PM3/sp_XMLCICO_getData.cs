using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Models.PM3
{
    public class sp_XMLCICO_getData
    {
        [Key]
        public string? CICO_ID { get; set; }
        public string? npk { get; set; }
        public string? empNm { get; set; }
        public string? divNm { get; set; }
        public string? deptNm { get; set; }
        public string? CI_date { get; set; }
        public string? CO_date { get; set; }
        public string? JamCI { get; set; }
        public string? JamC0 { get; set; }
        public string? assignDay { get; set; }
        public short? pro_scs { get; set; }
        public string? status_xml { get; set; }
        public string? pro_msg { get; set; }
        public string? IDNo { get; set; }
    }
}