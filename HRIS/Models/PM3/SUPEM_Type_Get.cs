using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Models.PM3;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HRIS.Models.PM3
{
    public class SUPEM_Type_Get
    {
          [Key]
          public long? permit_id { get; set; }
          public string? permit_code { get; set; }
          public string? permit_name { get; set; }
          public string? cico_code { get; set; }
          public short? visible { get; set; }
          public short? pass_date { get; set; }
          public DateTime? last_update { get; set; }
          public long? ID_user { get; set; }
          public int? max_days { get; set; }
          public short? need_abs { get; set; }
          public short? status_jam { get; set; }
          public short? status_lembur { get; set; }
          public short? status_libur { get; set; }
          public string? status_gender { get; set; }
          public short? exclude_SAP { get; set; }
    }
}