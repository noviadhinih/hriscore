using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.IO;
using Microsoft.Extensions.Configuration;


namespace HRIS.Models
{
    public class BaseUrls
    {
        static public IConfigurationRoot Configuration { get; set; }
        public BaseUrls()
        {
            var builder = new ConfigurationBuilder()
                         .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
        }
        public string getPM3ConnectionString()
        {
            return Configuration["PM3Connection"];
        }


    }
}