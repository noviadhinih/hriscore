using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRIS.Models
{
    public class WebBased
    {
        public string? UserId { get; set; }
        public string? Npk { get; set; }
        public long? DirectorateId { get; set; }
        public string Name { get; set; }
        public long? DivisionId { get; set; }
        public long? DeptId { get; set; }
        public long? LevelId { get; set; }
        public string LevelIdxNo { get; set; }
        public string LocationId { get; set; }
        public string LocationNm { get; set; }
        public string DeptHandle { get; set; }
        public string LocationHandle { get; set; }

        public WebBased(IHttpContextAccessor httpContextAccessor)
        {
            UserId = httpContextAccessor.HttpContext.Session.GetString("id");
            Npk=httpContextAccessor.HttpContext.Session.GetString("npk");
            DirectorateId = Convert.ToInt64(httpContextAccessor.HttpContext.Session.GetString("directorateID"));
            Name=httpContextAccessor.HttpContext.Session.GetString("name");
            DivisionId = Convert.ToInt64(httpContextAccessor.HttpContext.Session.GetString("ID_division"));
            DeptId = Convert.ToInt64(httpContextAccessor.HttpContext.Session.GetString("ID_dept"));
            LevelId = Convert.ToInt64(httpContextAccessor.HttpContext.Session.GetString("ID_level"));
            LevelIdxNo = httpContextAccessor.HttpContext.Session.GetString("level_idx_no");
            LocationId = httpContextAccessor.HttpContext.Session.GetString("ID_lokasi");
            LocationNm=httpContextAccessor.HttpContext.Session.GetString("lokasiNm");
            DeptHandle=httpContextAccessor.HttpContext.Session.GetString("dept_handle");
            LocationHandle=httpContextAccessor.HttpContext.Session.GetString("location_handle");
        }

    }
}