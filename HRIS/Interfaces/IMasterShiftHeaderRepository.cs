using System.Collections.Generic;
using System.Threading.Tasks;
using HRIS.Models.PM3;

namespace HRIS.Interfaces
{
    public interface IMasterShiftHeaderRepository
    {
        Task<List<Master_Shift_Header>> GetActiveShiftsAsync();
    }
}
