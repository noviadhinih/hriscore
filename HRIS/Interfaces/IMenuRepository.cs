using System.Collections.Generic;
using HRIS.Models.PM3;

namespace HRIS.Interfaces
{
    public interface IMenuRepository
    {
        List<sp_getMenu> GetMenu(string id_user);
    }
}
