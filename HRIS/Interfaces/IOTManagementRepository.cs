﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HRIS.Models.General;
using HRIS.Models.PM3;
using HRIS.Repositories;
namespace HRIS.Interfaces
{
    public interface IOTManagementRepository
    {
        Task<List<sp_Overtime_getMsProd>> GetDataMprod(clsOT f);
        Task<List<Overtime_tbl_r_shop>> getShop();
        Task<List<Overtime_tbl_r_line>> getLine();
        Task<return_value> InsertMprod(clsOT f);
    }
}
