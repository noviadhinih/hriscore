using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRIS.Controllers.TMS;
using HRIS.Models.General;
using HRIS.Models.PM3;

namespace HRIS.Interfaces.TMS
{
    public interface ITMSRepository
    {
        Task<List<sp_SUPEM_getDataReject>> RejectGetDataSupem(string deptHandle, string locHandle, string npk, string tgl_request_start, string tgl_request_end, string status, string tahun, string npk_input);
        Task<return_value> RejectSupem(string pid, string reason, string createdBy);
        Task<List<sp_SKTA_getDataReject>> RejectGetDataSKTA(string deptHandle, string locHandle, string npk, string tgl_request_start, string tgl_request_end, string status, string tahun, string npk_input);
        Task<return_value> RejectSKTA(string pid, string reason, string createdBy);
        Task<List<sp_CUTI_getDataReject>> RejectGetDataCuti(string deptHandle, string locHandle, string npk, string tgl_request_start, string tgl_request_end, string status, string tahun, string npk_input);
        Task<return_value> RejectCuti(string pid, string reason, string createdBy);
        Task<List<sp_Overtime_getDataReject>> RejectGetDataSPL(string deptHandle, string locHandle, string npk, string tgl_request_start, string tgl_request_end, string status, string tahun, string npk_input);
        Task<return_value> RejectSPL(string pid, string reason, string createdBy);

        #region ADD
        Task<List<sp_SUPEM_getData>> GetDataSupem(string deptHandle, string locHandle, string pr, string yr, string npk, string nama, string emp_status, string status, string divisi,
                                                  string dept, string golongan, string npk_input, string date_input, string costcenter, string shift, string type_spl, string UserId,
                                                  string npkLogin, string tgl_mulai, string tgl_selesai);

        Task<List<SUPEM_Type_Get>> GetPermissionTypeByID(string permit_id);
        Task<List<Employee_Working_Schedule_Get>> GetWorkingSchedule(string npk, DateTime clock_in, DateTime clock_out, string dws_type);
        Task<List<SUPEM_GetBy>> GetByID(string npk, DateTime date_start, DateTime date_end, string stats);
        Task<List<CICO_Stats_Get>> GetStats(string cico_code);
        Task<List<CICO_GetPriority>> GetCicoPriority(string npk, string start_date, string end_date);
        Task<List<SUPEM_CheckLeave>> CheckLeave(string npk, string start_date, string end_date);
        Task<return_value> InsertSupem(SUPEM c);
        Task<List<Employee_ValidateSKTAByNPK>> GetValidateSKTAByNPK(string npk, string UserId, string DeptId, string LocationId);
        Task<List<Sp_Employee_GetAllAtasan_ByDeptHead>> GetAtasanDH(string npk);
        Task<List<Sp_Employee_GetAllAtasan_ByDeptHead>> GetAtasanBasedOrganization(string npk);
        Task<List<CICO_Get>> GetCICO(string npk, string working_date);
        Task<List<SUPEM_Type_Get>> GetListSupemType(string npk);
        Task<List<sp_SUPEM_getRekapExcel>> GetdataRekapExcelSupem(string permission_id);
        
        #endregion
        #region APPROVAL
        Task<List<sp_SKTA_getApproval>> GetSKTAApproval(SKTA c,string UserId,string npkLogin);
        Task<List<sp_SUPEM_getApproval>> GetSUPEMApproval(SKTA c,string UserId,string npkLogin);
        Task<List<sp_CUTI_getApproval>> GetCUTIApproval(SKTA c, string UserId, string npkLogin);
        Task<List<sp_SPL_getApproval>> GetSPLApproval(SKTA c, string UserId, string npkLogin);
        Task<return_value> ApproveSKTA(string pid, string stats, string createdBy,string rejectComment);
        Task<return_value> ApproveSUPEM(string pid, string stats, string createdBy,string rejectComment);
        Task<return_value> ApproveCUTI(string pid, string stats, string createdBy,string rejectComment);
        Task<return_value> ApproveSPL(string pid, string stats, string createdBy,string rejectComment);
        #endregion


    }
}