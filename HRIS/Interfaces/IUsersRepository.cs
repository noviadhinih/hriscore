using System.Collections.Generic;
using System.Threading.Tasks;
using HRIS.Models;
using HRIS.Models.General;
using HRIS.Models.PM3;

namespace HRIS.Interfaces
{
    public interface IUsersRepository
    {
        Task<List<Users_GetData2>> GetUserDataAsync(string username, string password, string token);
        Task<return_value> RegisterMFA(string id_user, string token);
    }
}
