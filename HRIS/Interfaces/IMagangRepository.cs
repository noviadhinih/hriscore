﻿using HRIS.Controllers.Magang;
using HRIS.Models.General;
using HRIS.Models.PM3;
using HRIS.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace HRIS.Interfaces
{
    public interface IMagangRepository
    {
        Task<List<sp_Pemagangan_getData>> GetDataHiring(clsfil f);
        Task<List<sp_Pemagangan_ValidasiUploadHiring>> ValidasiUHiring(clsfil f);
        Task<return_value> UploadHiring(uHiring f);
        Task<List<sp_Pemagangan_getDataWorkSchdule>> GetDataWorkSch(clsWorkSch f);
        Task<return_value> UploadSch(uWorkSch f);
        Task<List<sp_Pemagangan_getAttendlist>> GetDataAttendance(clsfil f);
    }
}
