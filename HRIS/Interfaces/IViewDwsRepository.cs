using System.Collections.Generic;
using System.Threading.Tasks;
using HRIS.Models.PM3;

namespace HRIS.Interfaces
{
    public interface IViewDwsRepository
    {
        Task<List<view_DWS>> GetDWSAsync();
        Task<view_DWS> GetDWSAsync(long dwsId);
    }
}
