using System.Collections.Generic;
using System.Threading.Tasks;
using HRIS.Models.PDK;
using HRIS.Models.PM3;

namespace HRIS.Interfaces.PDK
{
    public interface IFamilyRepository
    {
        Task<FamilyModel> GetFamilyDataAsync(string npk);
    }
}
