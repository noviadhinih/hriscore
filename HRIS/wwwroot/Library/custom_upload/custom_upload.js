// Javascript Document
// Created by Albert Ricia
// Created on : 27 Juli 2014
// Javascript uploader

function Custom_upload(uploader,config)
{
	this.uploadID = uploader;
	this.upload = $("."+uploader);
	this.showProgressBar = (typeof config.showProgressBar != "undefined") ? config.showProgressBar : true; 
	this.uploadTriggerHandler = (typeof config.uploadTriggerHandler != "undefined") ? config.uploadTriggerHandler : null; 
	this.uploadChangeHandler = (typeof config.uploadChangeHandler != "undefined") ? config.uploadChangeHandler : null; 
	this.uploadHandler = (typeof config.uploadHandler != "undefined") ? config.uploadHandler : null; 
	this.uploadSuccessHandler = (typeof config.uploadSuccessHandler != "undefined") ? config.uploadSuccessHandler : null; 
	this.maxSize = (typeof config.maxSize == "undefined") ? 600 * 1024 : config.maxSize * 1024;
	this.fileType = (typeof config.fileType == "undefined") ? "*" : config.fileType;
	
	// Init Trigger
	if (this.triggerEvent == true) {
		this.initTrigger();
	}

	// Init Upload Dialog
	this.initUploadDialog();
}

Custom_upload.prototype.initTrigger = function()
{
	this.upload = $("."+this.uploadID);
	this.upload.attr("href","#"+this.uploadID);
	this.upload.attr("data-toggle","modal");

	// Always empty file when init dialog
	var cstUpload = this;
	this.upload.click(function(e)
	{
		var which = $(this);

		cstUpload.emptyFile();
		
		if (cstUpload.uploadTriggerHandler != null)
			cstUpload.uploadTriggerHandler(which);
	});
}

Custom_upload.prototype.angularFunc = function($event, data)
{
	$("#"+this.uploadID).modal("show");
	var which = new Object();
	which.data = data;
	this.emptyFile();
	
	if (this.uploadTriggerHandler != null)
		this.uploadTriggerHandler(which);
	
}

Custom_upload.prototype.setTitle = function(title)
{
	$("#cstUploadTitle").html(title);
}

Custom_upload.prototype.initUploadDialog = function()
{
	var html = "<div class='modal fade' id='"+this.uploadID+"'>" +
				  "<div class='modal-dialog'>" + 
   				 "<div class='modal-content'>" + 
				  "<div class='modal-header'>" +
				    "<button type='button' class='close' id='cstClose' data-dismiss='modal'>&times;</button>" +
				    	"<h4 class='modal-title'><span class='glyphicon glyphicon-paperclip'></span> &nbsp;<span id='cstUploadTitle'>Upload Arsip</span></h4>" +
				  	"</div>" + 
				  	"<div class='modal-body'>" + 
				   		"<div class='span4'>" +
				    		"<table class='small-mleft fluid'>" +
				      			"<tbody>" + 
				       				"<tr>" +    
				        				"<td width='100px'><b>Pilih Arsip</b></td>" + 
								        "<td colspan='3'>" + 
								        "<input type='file' class='span4' id='cstFile' name='cstFile' />" +
								        "</td>" +
								       "</tr>" +
								       "<tr>" +
								         "<td colspan='2' id='tdprogress' class='d_none'>"+
								         	"<div class='progress mtop'>" +
											  "<div class='progress-bar progress-bar-striped active' id='cstProgressBar'></div>" +
											"</div>" + 
								         "</td>" +
								       "</tr>" +
								       "<tr>" +
								         "<td colspan='2' id='tderror' class='file-error' style='padding-top:10px'>"+
								         "</td>" +
								       "</tr>" +
								   	"</tbody>" +
								 "</table>" +
								"</form>" +
								"</div>" +
								"</div>" +
								"<div class='modal-footer'>" +
								  "<a id='cstUpload' class='btn btn-success'><i class='glyphicon glyphicon-upload'></i> &nbsp;Upload</a>" +
								"</div>" +
								"</div>" +
								"</div>" + 
								"</div>";
	$("body").append(html);

	var cstUpload = this; 
	
	$("#cstUpload").click(function(e)
	{
		cstUpload.uploadFile();
	});

	$("#cstFile").change(function(e)
	{	
		var files = cstUpload.getFiles();

		if (cstUpload.uploadChangeHandler != null)
			cstUpload.uploadChangeHandler(files);
	});
}

Custom_upload.prototype.getFiles = function()
{
	var files = document.getElementById("cstFile").files;
	return files;
}

Custom_upload.prototype.getFilesExtension = function()
{
	var file = document.getElementById("cstFile").value;
	var exts = file.split(".");
	var ext = exts[exts.length-1];
	return ext;
}

Custom_upload.prototype.emptyFile = function()
{
	document.getElementById("cstFile").value = "";
}

Custom_upload.prototype.setFileType = function(typeList)
{
	this.fileType = typeList;
}

Custom_upload.prototype.uploadFile = function()
{
	var files = this.getFiles();
	var file = files[0];
	var ext = this.getFilesExtension();

	if (this.getFiles().length <= 0) // Validate if file attached or not
	{
		$(".file-error").html("There is no file attached.");
	}
	else if (file.size > this.maxSize) // Validate Size
	{
		$(".file-error").html("File size must not exceed " + (this.maxSize /1024) + " KB.");
	}
	else if (this.fileType != "*" && this.fileType.indexOf(ext) == -1)
	{
		$(".file-error").html("Please upload " + this.fileType.join(", ") + " files.");
	}
	else
	{
		if (this.uploadHandler != null)
			this.uploadHandler(files);
	}

	setTimeout(function() { $(".file-error").html(""); } , 4000);
}

Custom_upload.prototype.setProgressBarValue = function (percent)
{
	$("#cstProgressBar").css("width",percent+"%");
}

Custom_upload.prototype.closeUploadDialog = function()
{
	$("#cstClose").click();
}

Custom_upload.prototype.uploadFileToDest = function(urlDest, params)
{
	param = (typeof param == "undefined") ? "" : param;
	var file = this.getFiles()[0];
	var xhr = new XMLHttpRequest();
	var formdata;
	var cstUpload = this;

	if (window.FormData) {
	    formdata = new FormData();
	  }

	if ( window.FileReader ) {
  		reader = new FileReader();
	 	reader.onloadend = function (e) { 	

	  };
	  reader.readAsDataURL(file);
	}
	if (formdata) {
		for (var i = 0; i<params.length ; i++)
		{
			var p = params[i].split("=");
			var param = p[0];
			var value = p[p.length-1];
			formdata.append(param, value);
		}
	 	formdata.append("cstFile", file);
	}

	if (formdata) {

		if (cstUpload.showProgressBar == true)
		{
			// Show Progress bar
			$("#tdprogress").show();
		}

	    // xhr.file = file; // not necessary if you create scopes like this
	    xhr.addEventListener('progress', function(e) {
	        var done = e.position || e.loaded, total = e.totalSize || e.total;
	        console.log('xhr progress: ' + (Math.floor(done/total*1000)/10) + '%');
	    }, false);
	    if ( xhr.upload ) {
	        xhr.upload.onprogress = function(e) {
	            var done = e.position || e.loaded, total = e.totalSize || e.total;
	            var percent = (Math.floor(done/total*1000)/10);
	           
	            if (cstUpload.showProgressBar == true)
				{
	            	cstUpload.setProgressBarValue(percent);	
	        	}
	        	console.log('xhr.upload progress: ' + done + ' / ' + total + ' = ' +  + '%');
	        };
	    }
	    xhr.onreadystatechange = function(e) {
	        if ( this.readyState == 4 ) {
				var result = xhr.responseText;
				console.log(result);
	            console.log(['xhr upload complete', e]);
	            
	            
	            setTimeout(function()
	            {
	            	if (cstUpload.showProgressBar == true)
					{
		            	$("#tdprogress").hide();
			            cstUpload.setProgressBarValue(0);
			            cstUpload.closeUploadDialog();
		        	}

		        	if (typeof cstUpload.uploadSuccessHandler != "undefined")
	        			cstUpload.uploadSuccessHandler(result);
	        	}, 2500);
	        }
	    };
	    xhr.onerror = function(e) {
	    	errorFlag = true;
	    	cstUpload.emptyFile();
        	$(".progressbar").hide();
        	cstUpload.setProgressBarValue(0);
	    	alert("Error occurred while uploading to server, please retry again.");
	    };
	    xhr.open('post', urlDest, true);
	    xhr.send(formdata);
	}
}