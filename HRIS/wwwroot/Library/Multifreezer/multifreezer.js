$(document).ready(function () {
    /*
    jQuery MultiFreezer - scrollable tables with freezed thead and (n) first columns.
    (c) 2017 Jan Renner (http://janrenner.cz, jan.renner@gmail.com)
    */
    $('.table-freeze-multi').each(function () {

        var table = $(this),
        scrollbarWidth = freezerGetScrollbarWidth();

        //prepare
        table.css({
            margin: 0
        }).addClass('table-freeze-multi-original').find('tfoot').remove();

        //wrap
        table.wrap('<div class="freeze-multi-scroll-wrapper" />');
        var wrapper = table.closest('.freeze-multi-scroll-wrapper');
        table.wrap('<div class="freeze-multi-scroll-table" />');
        table.wrap('<div class="freeze-multi-scroll-table-body" />');
        var scroller = wrapper.find('.freeze-multi-scroll-table-body');

        //layout
        var headblock = $('<div class="freeze-multi-scroll-table-head-inner" />');
        scroller.before($('<div class="freeze-multi-scroll-table-head" />').append(headblock));
        var topblock = $('<div class="freeze-multi-scroll-left-head" />');
        var leftblock = $('<div class="freeze-multi-scroll-left-body-inner" />');
        wrapper.append(
        $('<div class="freeze-multi-scroll-left" />')
            .append(topblock)
            .append($('<div class="freeze-multi-scroll-left-body" />').append(leftblock))
    );

        //cloning
        var clone = table.clone(true);
        clone.addClass('table-freeze-multi-clone').removeClass('table-freeze-multi-original');
        var colsNumber = table.data('colsNumber') || table.find('tbody tr:first th').length;
        //head
        var cloneHead = clone.clone(true);
        cloneHead.find('tbody').remove();
        headblock.append(cloneHead);
        //top
        var cloneTop = cloneHead.clone(true);
        topblock.append(cloneTop);
        //left
        var cloneLeft = clone.clone(true);
        cloneLeft.find('thead').remove();
        leftblock.append(cloneLeft);

        //sizing
        var scrollHeight = table.data('scrollHeight') || wrapper.parent().closest('*').height();
        var headerHeight = table.find('thead').height();
        var bodyHeight = $(".freeze-multi-scroll-left-body-inner table").height();
        var leftWidth = (function () {
            var w = 0;
            table.find('tbody tr:first > *').slice(0, colsNumber).each(function () {
                w = w + $(this).outerWidth();
            });
            return w + 1;
        } ());
        wrapper.css('height', scrollHeight);
        scroller.css('max-height', scrollHeight - headblock.height());
        headblock.width(table.width()).css('padding-right', scrollbarWidth);
        // leftblock.add(leftblock.parent()).height(scrollHeight - scrollbarWidth - headerHeight);
        leftblock.add(leftblock.parent()).height(bodyHeight - 18);
        leftblock.width(leftWidth + scrollbarWidth);
        wrapper.find('.freeze-multi-scroll-left').width(leftWidth);

        //postprocess
        wrapper.find('.table-freeze-multi-original thead').hide();

        //scrolling
        scroller.on('scroll', function () {
            var s = $(this),
            left = s.scrollLeft(),
            top = s.scrollTop();
            headblock.css('transform', 'translate(' + (-1 * left) + 'px, 0)');
            leftblock.scrollTop(top);
        });
        leftblock.on('mousewheel', false);

    });
    ensureWidthData();
});

function ensureWidthData() {
    var heads = $(".freeze-multi-scroll-left-head thead th.column");
    var data = $(".freeze-multi-scroll-left .freeze-multi-scroll-left-body tbody tr:first td");
    var real = $(".freeze-multi-scroll-table .freeze-multi-scroll-table-body tbody tr:first td");
    var wHeight = $('.freeze-multi-scroll-left-body').height();
    var mHeight = $('.freeze-multi-scroll-wrapper').height();
    var hHeight = $(".freeze-multi-scroll-table-head").height();
    // var scrollHeight = 16;
    var scrollHeight = 0;
    var width;
    for (var i = 0; i < heads.length;i++) {
        width = $(heads[i]).outerWidth();
        $(data[i]).css("width", width);
        $(real[i]).css("width", width);
    }

    var beginCol = 6;
    for (var i = 1; i <= data.length - 12; i++) {
        if (i % 5 == 0) {
            width = $(".colgate").outerWidth();
        } else {
            width = $(".colfree").outerWidth();
        }
        $(data[beginCol + i]).css("width", width);
        $(real[beginCol + i]).css("width", width);
       
    }

    $('.freeze-multi-scroll-left-body').css("height", wHeight - scrollHeight);
    $('.freeze-multi-scroll-left-body').css("max-height", mHeight - hHeight - scrollHeight);
    $('.freeze-multi-scroll-left-body-inner').css("max-height", mHeight - hHeight - scrollHeight);
}

function ensureWidthData2() {
    var heads = $(".freeze-multi-scroll-left-head thead th.column");
    var data = $(".freeze-multi-scroll-left .freeze-multi-scroll-left-body tbody tr:first td");
    var real = $(".freeze-multi-scroll-table .freeze-multi-scroll-table-body tbody tr:first td");
    var wHeight = $('.freeze-multi-scroll-left-body').height();
    var mHeight = $('.freeze-multi-scroll-wrapper').height();
    var hHeight = $(".freeze-multi-scroll-table-head").height();
    var scrollHeight = 16;
    var width, width2, selisih;
    for (var i = 0; i < heads.length; i++) {
        width = $(heads[i]).outerWidth();
        width2 = $(data[i]).css("width");
        selisih = parseInt(width2) - width;
        $(data[i]).css("width", width - selisih);
        $(real[i]).css("width", width - selisih);
    }

    var beginCol = 6;
    var gateIdx = 0, freeIdx = 0;
    var gates = $(".colgate");
    var frees = $(".colfree");
    for (var i = 1; i <= data.length - 8; i++) {  
        if (i % 5 == 0) {
            width = $(gates[gateIdx]).outerWidth();
            width2 = $(real[beginCol + i]).css("width");
            selisih = parseInt(width2) - width;
            gateIdx++;
        } else {
            width = $(frees[freeIdx]).outerWidth();
            width2 = $(real[beginCol + i]).css("width");
            selisih = parseInt(width2) - width;
            freeIdx++;
        }
        $(data[beginCol + i]).css("width", width);
        $(real[beginCol + i]).css("width", width);
        console.log((beginCol + i) + " " + width2 + "|" + selisih + " " + width + " " + $(real[beginCol + i]).css("width"));

    }

    $('.freeze-multi-scroll-left-body').css("height", wHeight - scrollHeight);
    $('.freeze-multi-scroll-left-body').css("max-height", mHeight - hHeight - scrollHeight);
    $('.freeze-multi-scroll-left-body-inner').css("max-height", mHeight - hHeight - scrollHeight);
}

// @see https://davidwalsh.name/detect-scrollbar-width
function freezerGetScrollbarWidth () {
    // Create the measurement node
    var scrollDiv = document.createElement("div");
    scrollDiv.className = "freezer-scrollbar-measure";
    document.body.appendChild(scrollDiv);

    // Get the scrollbar width
    var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
    //console.warn(scrollbarWidth); // Mac: 15, Win: 17

    // Delete the DIV 
    document.body.removeChild(scrollDiv);

    return scrollbarWidth;
}
