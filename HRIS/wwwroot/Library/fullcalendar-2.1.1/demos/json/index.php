<?php 
	$addr = explode("/",$_SERVER['SCRIPT_NAME']);
	$base_url = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on" ) ? "https" : "http" );
	$base_url .= "://".$_SERVER['HTTP_HOST'];
	$base_url .= "/".$addr[1];
	header("location:".$base_url."/public/error/e403");
?>