
function treeViewExpand(showButton, hideButton, childrenItems, globalIndex) 
{
    UpdateExpandCollapseState(globalIndex, 'e');

    document.getElementById(showButton).style.display = 'none';
    document.getElementById(hideButton).style.display = 'block';
    document.getElementById(childrenItems).style.display = 'block';
}

function treeViewCollapse(showButton, hideButton, childrenItems, globalIndex) 
{
    UpdateExpandCollapseState(globalIndex, 'c');
 
    document.getElementById(showButton).style.display = 'block';
    document.getElementById(hideButton).style.display = 'none';
    document.getElementById(childrenItems).style.display = 'none';
}

function UpdateExpandCollapseState(globalIndex, expandCollapseCharacter) {
    
    // expandCollapseHiddenFieldId is a javascript variable holding the client id of the 
    // ExpandCollapseStatesHiddenField control. The variable is added to the TreeView.aspx page
    // as a ClientScriptBlock on the Page_Load of TreeView.aspx.cs
    var hiddenControl = document.getElementById(expandCollapseHiddenFieldId);
    
    var currentString = hiddenControl.value;
    
    var start = currentString.substring(0, globalIndex);
    var after = currentString.substring(globalIndex + 1);
    
    var updatedString = start + expandCollapseCharacter + after;
    
    hiddenControl.value = updatedString;
}